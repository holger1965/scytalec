﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * This class encapsulates the Inmarsat-C decoder.
 * The entire decoding chain is contained within and events are raised as needed.
 * The events are synchronous, and it is the user responsability not to block them.
 * 
 * Currently the code implements a queue to be used by the UI. I am doing this
 * because the events are synchronous and any delay in processing within event
 * handlers will block the decoder. I am using a timer in adifferent thread to
 * consume the queue filled by the decoder.
 * Would love to have someone show me a way to properly do this using Tasks or what
 * not.
 * 
 */

using ScytaleC.PacketDecoders;
using ScytaleC.Interfaces;
using ScytaleC.Properties;
using System.Collections.Concurrent;

namespace ScytaleC
{
    public class InmarsatCDecoder
    {
        private IDemodulator demodulator;
        private IUWFinder uwFinder;
        private IDepermuter depermuter;
        private IDeinterleaver deinterleaver;
        private IViterbiDecoder viterbiDecoder;
        private IDescrambler descrambler;
        private IPacketDetector packetDetector;

        private ConcurrentQueue<object> eQue;
        private ConcurrentQueue<SignalAttributesArgs> signalAttributesArgsQue;
        private const int MaxQueStorage = 1000;

        //public event EventHandler<DemodulatedSymbolsArgs> OnDemodulatedSymbols;
        //public event EventHandler<SignalAttributesArgs> OnDemodulatedSignalAttributes;
        //public event EventHandler<UWFinderFrameArgs> OnUWFinderFrame;
        //public event EventHandler<DepermutedFrameArgs> OnDepermutedFrame;
        //public event EventHandler<DeinterleavedFrameArgs> OnDeinterleavedFrame;
        //public event EventHandler<ViterbiFrameArgs> OnViterbiFrame;
        //public event EventHandler<DescrambledFrameArgs> OnDescrambledFrame;
        //public event EventHandler<PacketArgs> OnPacket;
        //public event Eventhandler<MessageArgs> OnMessage;

        public InmarsatCDecoder(ConcurrentQueue<object> eQue, ConcurrentQueue<SignalAttributesArgs> sQue)
        {
            this.eQue = eQue;
            signalAttributesArgsQue = sQue;

            demodulator = null;
            
            uwFinder = new UWFinder();
            uwFinder.OnUWFinderFrame += UwFinder_OnUWFinderFrame;

            depermuter = new Depermuter();
            depermuter.OnDepermutedFrame += Depermuter_OnDepermutedFrame;

            deinterleaver = new Deinterleaver();
            deinterleaver.OnDeinterleavedFrame += Deinterleaver_OnDeinterleavedFrame;

            viterbiDecoder = null;

            descrambler = new Descrambler();
            descrambler.OnDescrambledFrame += Descrambler_OnDescrambledFrame;

            packetDetector = new PacketDetector();
            packetDetector.OnPacket += PacketDetector_OnPacket;
        }

        public void SetDemodulator(IDemodulator demodulator)
        {
            unsetDemodulator(demodulator);

            if (this.demodulator != demodulator)
            {
                this.demodulator = demodulator;
                this.demodulator.SetCenterFrequency(DataConsts.InmarsatCCenterFrequency);
                this.demodulator.CmaEnabled = Settings.Default.cbCMA;
                this.demodulator.AgcEnabled = Settings.Default.cbAGC;
                uwFinder?.SetTolerance((int)Settings.Default.nTolerance);
            }
            this.demodulator.OnDemodulatedSymbols += Demodulator_OnDemodulatedSymbols;
            this.demodulator.OnSignalAttributes += Demodulator_OnSignalAttributes;
        }

        private void unsetDemodulator(IDemodulator demodulator)
        {
            if (this.demodulator == null)
            {
                return;
            }

            if (this.demodulator == demodulator)
            {
                this.demodulator.OnDemodulatedSymbols -= Demodulator_OnDemodulatedSymbols;
                this.demodulator.OnSignalAttributes -= Demodulator_OnSignalAttributes;
            }
        }

        public void SetTolerance(int tolerance)
        {
            uwFinder?.SetTolerance(tolerance);
        }

        public void SetCenterFrequency(double centerFrequency)
        {
            demodulator?.SetCenterFrequency(centerFrequency);
        }

        public void SetCMAEnabled(bool cmaEnabled)
        {
            if (demodulator != null)
            {
                demodulator.CmaEnabled = cmaEnabled;
            }
        }

        public void SetAGCEnabled(bool agcEnabled)
        {
            if (demodulator != null)
            {
                demodulator.AgcEnabled = agcEnabled;
            }
        }

        public void SetViterbiDecoder(IViterbiDecoder viterbiDecoder)
        {
            unsetViterbiDecoder(viterbiDecoder);

            if (this.viterbiDecoder != viterbiDecoder)
            {
                this.viterbiDecoder = viterbiDecoder;
            }
            this.viterbiDecoder.OnViterbiFrame += ViterbiDecoder_OnViterbiFrame;
        }

        private void unsetViterbiDecoder(IViterbiDecoder viterbiDecoder)
        {
            if (this.viterbiDecoder == null)
            {
                return;
            }

            if (this.viterbiDecoder == viterbiDecoder)
            {
                this.viterbiDecoder.OnViterbiFrame -= ViterbiDecoder_OnViterbiFrame;
            }
        }

        private void PacketDetector_OnPacket(object sender, PacketArgs e)
        {
            //OnPacket?.Invoke(this, e);

            // If no one is listening, do not give out any data
            //if (eQue.Count > MaxQueStorage)
            //{
            //    return;
            //}

            //eQue.Enqueue(e);
        }

        private void Descrambler_OnDescrambledFrame(object sender, DescrambledFrameArgs e)
        {
            //OnDescrambledFrame?.Invoke(this, e);

            // If no one is listening, do not give out any data
            if (eQue.Count > MaxQueStorage)
            {
                return;
            }

            eQue.Enqueue(e);
            //packetDetector.Process(e);
        }

        private void ViterbiDecoder_OnViterbiFrame(object sender, ViterbiFrameArgs e)
        {
            //OnViterbiFrame?.Invoke(this, e);

            //// If no one is listening, do not give out any data
            //if (eQue.Count > MaxQueStorage)
            //{
            //    return;
            //}

            //eQue.Enqueue(e);
            descrambler.Process(e);
        }

        private void Deinterleaver_OnDeinterleavedFrame(object sender, DeinterleavedFrameArgs e)
        {
            //OnDeinterleavedFrame?.Invoke(this, e);

            //// If no one is listening, do not give out any data
            //if (eQue.Count > MaxQueStorage)
            //{
            //    return;
            //}

            //eQue.Enqueue(e);
            viterbiDecoder.Process(e);
        }

        private void Depermuter_OnDepermutedFrame(object sender, DepermutedFrameArgs e)
        {
            //OnDepermutedFrame?.Invoke(this, e);

            //// If no one is listening, do not give out any data
            //if (eQue.Count > MaxQueStorage)
            //{
            //    return;
            //}

            //eQue.Enqueue(e);
            deinterleaver.Process(e);
        }

        private void UwFinder_OnUWFinderFrame(object sender, UWFinderFrameArgs e)
        {
            //OnUWFinderFrame?.Invoke(this, e);
            
            // If no one is listening, do not give out any data
            if (eQue.Count > MaxQueStorage)
            {
                return;
            }

            eQue.Enqueue(e);
            depermuter.Process(e);
        }

        private void Demodulator_OnDemodulatedSymbols(object sender, DemodulatedSymbolsArgs e)
        {
            //OnDemodulatedSymbols?.Invoke(this, e);

            // If no one is listening, do not give out any data
            if (eQue.Count > MaxQueStorage)
            {
                return;
            }

            eQue.Enqueue(e);
            uwFinder.Process(e);
        }

        private void Demodulator_OnSignalAttributes(object sender, SignalAttributesArgs e)
        {
            // If the source is a file, this will get call ridiculously fast with no
            // actual resalt other than rendering the application unusable
            if (signalAttributesArgsQue.Count < 10)
            {
                //OnDemodulatedSignalAttributes?.Invoke(this, e);
                signalAttributesArgsQue.Enqueue(e);
            }
        }

        public void Process(AudioSampleArgs e)
        {
            if (demodulator == null || viterbiDecoder == null)
            {
                return;
            }
            demodulator.Process(e);
        }

        internal void SetLoFreq(int loFreq)
        {
            if (demodulator != null)
            {
                demodulator.LoFreq = loFreq;
            }
        }

        internal void SetHiFreq(int hiFreq)
        {
            if (demodulator != null)
            {
                demodulator.HiFreq = hiFreq;
            }
        }
    }
}
