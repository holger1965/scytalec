﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *   Bibliography:
 * 
 *   Calcutt, D. M., & Tetley, L. (2004). Satellite communications: principles and applications. Oxford: Elsevier.
 *   https://www.amazon.com/Satellite-Communications-Applications-David-Calcutt/dp/034061448X
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 * Data is transmitted in blocks using 640 bytes. The convolutional encoder used by the Inmarsat C consists of the 7 bits shift register
 * (constraint length-7) and two modulo-2 adderes (exclusive OR). Data is fed into a 1/2 rate convolutional encoder (CE) using a left to
 * right register. The convolutional encoder produces 640 x 8 x 2 symbols (10240 symbols). The symbols are passed to an interleaver
 * matrix. The first 2 columns are identical and permanently contain two unique word patterns. The unique word pattern of the
 * transmitter/receiver is given in a cuple of UW table that can be inferred one from the other. The remaining 160 columns contain the
 * encoder 10240 symbols output. From the output of the CE the symbols are passed in by column (chunks of 64) from top to bottom into
 * the matrix. The interleave block is transmitted on a row by row basis with the symbols in each row sent in ascending column order,
 * UW first. The transmission at symbol level occurs left to right. The rows i(0..64) are transmitted in a permutted sequence from
 * row j = 0 to row j = 63 where i and j are correlated by the formulas: i = (j x 39) modulo 64, j = (i x 23) modulo 64.
 * 
 * Each frame lasts 8.64 seconds, 1200 Symbols/s.
 * The time of day can be computed from this information. The information is volatile, and as such, assume current date. 
 * Frame start time reference is always the leading edge of first UW for i=j=0
 *   
 * For the implementation below we handle the data in the order it comes out of the demodulator.
 * First symbol at the highest index in the symbolRegister array.
 * 
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * current frame           | last_data_symbol ..... last_UW last_UW | ..... | .... second_data_symbol first_data_symbol first_UW first_UW |
 * content when detecting  |       0                  160    161                       10364              10365         10366     10367
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * previous frame          |
 * this is an incomplete   | last_data_symbol
 * or bad frame as we      | of prev frame
 * could not identify the  |
 * correct UWs             | ....................                                             -->                -->              20735    |
 * -----------------------------------------------------------------------------------------------------------------------------------------
 * 
 * The next symbol coming from demodulator will rotate the entire symbolRegister to the right.
 * We will then start filling the beginning of the symbolRegister with the next frame.
 * The next frame is expected to arrive after just about another 10367 symbols.
 *  
 * TODO:
 * Add notes on uncertainty and mid-frame polarity changes.
 * 
 */


using ScytaleC.Interfaces;
using System;

namespace ScytaleC
{
    /// <summary>
    /// The UW finder works with a matrix of interleaved symbols as described above.
    /// The BPSK demodulator has phase ambiguity, and also there is the possibility
    /// that mid-stream the polarity of the symbols will change if the demodulator
    /// steps over a symbol.
    /// 
    /// This also represents the frame synchronization.
    /// </summary>
    class UWFinder : IUWFinder
    {
        /// This is the UW pattern at the receiver, 64 symbols for Inmarsat-C, see table in specs.
        private byte[] nrmPolUwPattern =
        {
            0, 0, 0, 0,    0, 1, 1, 1,   1, 1, 1, 0,   1, 0, 1, 0,
            1, 1, 0, 0,    1, 1, 0, 1,   1, 1, 0, 1,   1, 0, 1, 0,
            0, 1, 0, 0,    1, 1, 1, 0,   0, 0, 1, 0,   1, 1, 1, 1,
            0, 0, 1, 0,    1, 0, 0, 0,   1, 1, 0, 0,   0, 0, 1, 0
        };

        /// We must use a reverse polarity search pattern as well due to phase ambiguity
        private byte[] revPolUwPattern =
        {
            1, 1, 1, 1,    1, 0, 0, 0,   0, 0, 0, 1,   0, 1, 0, 1,
            0, 0, 1, 1,    0, 0, 1, 0,   0, 0, 1, 0,   0, 1, 0, 1,
            1, 0, 1, 1,    0, 0, 0, 1,   1, 1, 0, 1,   0, 0, 0, 0,
            1, 1, 0, 1,    0, 1, 1, 1,   0, 0, 1, 1,   1, 1, 0, 1
        };

        private byte[] symbolRegister = new byte[DataConsts.UWFinderFrameLength * 2];
        private byte[] symbolRegisterUncertain = new byte[DataConsts.UWFinderFrameLength];

        // There is no reason why this should not start at zero.
        // however if the input is a file, by setting the counter higher we force the evaluation of
        // less than full frames as well.
        // Of course, to make this really nice, we should also on close flush our register, just in
        // case there is one almost complete frame not getting pushed through fully.
        private int symbolCount = 10239;
        private int tolerance = DataConsts.MaxTolerance;

        public event EventHandler<UWFinderFrameArgs> OnUWFinderFrame;

        /// <summary>
        /// One full data frame has 10240 symbols.
        /// Add to this 2 x 64 UW symbols and we get 10368 symbols per received frame.
        /// We use a buffer of 2 full frames to be able to store the symbols that come before a frame is detected.
        /// Beyound 2 frames, what we miss is discarded.
        /// 
        /// We store the symbols into the symbol buffer, one symbol at a time.
        /// The first symbol received is always at the highest index in the symbols array parameter.
        /// As new symbols come, the buffer content is rotated to the right to make space for the newcomer.
        /// As the newcomer arrives, we run the unique word finding sequence.
        /// When the current frame is detected, we check the number of symbols pushed beyound the frame size and if a good
        /// amount, say 80% of a frame is in there, we attempt to decode that as well.
        /// If we manage to recover that frame, we send it down the processing chain before the current frame.
        /// </summary>
        /// <param name="symbols">The symbols coming straight from the demodulator, in the order they are demodulated.</param>
        /// <param name="length">The length of the symbols array.</param>
        public void Process(DemodulatedSymbolsArgs args)
        {
            for (int pos = args.Length - 1; pos >= 0; pos--)
            {
                //shift register to right with one symbol to make space for the next incoming symbol
                Array.Copy(symbolRegister, 0, symbolRegister, 1, symbolRegister.Length - 1);

                //store
                symbolRegister[0] = args.Symbols[pos];
                symbolCount++;

                //detect with a few symbols leeway
                //needs testing
                if (symbolCount >= DataConsts.UWFinderFrameLength - 1000)
                {
                    int nUW;
                    int rUW;
                    bool isReversedPolarity;
                    bool isMidStreamReversePolarity;
                    bool isReversedFirst_;
                    if (isFrameDetected(true, out nUW, out rUW, out isReversedPolarity, out isMidStreamReversePolarity, out isReversedFirst_))
                    {
                        /// We check symbolRegister length
                        /// If the length is larger than 1.6 the length of a frame, we can assume
                        /// there is another at least 0.6 of a frame before the current detected one
                        /// We attempt to correct it and redetect it.
                        /// If after correction it passes the tolerence we send it further before the
                        /// packet we detected.
                        if (symbolCount > DataConsts.UWFinderFrameLength * 1.6)
                        {
                            int nUW_;
                            int rUW_;
                            bool isReversedPolarity_;
                            bool isMidStreamReversePolarity_;

                            /// There is no point in checking if the frame has been detected, however we need the
                            /// output values
                            isFrameDetected(false, out nUW_, out rUW_, out isReversedPolarity_, out isMidStreamReversePolarity_, out isReversedFirst_);

                            /// We reverse all symbols either for reverse polarity or for normal
                            /// however we need to know which are which
                            int i_ = 0;
                            if (isReversedFirst_)
                            {
                                /// The highest index in the uncertain frame contain reversed polarity symbols
                                i_ = 2 * DataConsts.UWFinderFrameLength - 81 * rUW_;
                            }
                            else
                            {
                                i_ = 2 * DataConsts.UWFinderFrameLength - 81 * nUW_;
                            }

                            for (; i_ > DataConsts.UWFinderFrameLength; i_--)
                            {
                                symbolRegister[i_] = symbolRegister[i_] == 0 ? (byte)1 : (byte)0;
                            }

                            /// now we reassess the result
                            if (isFrameDetected(false, out nUW_, out rUW_, out isReversedPolarity_, out isMidStreamReversePolarity_, out isReversedFirst_))
                            {
                                UWFinderFrameArgs uwfba_ = new UWFinderFrameArgs();
                                byte[] frame_ = new byte[DataConsts.UWFinderFrameLength];
                                Array.Copy(symbolRegister, DataConsts.UWFinderFrameLength, frame_, 0, DataConsts.UWFinderFrameLength);
                                uwfba_.Length = DataConsts.UWFinderFrameLength;
                                uwfba_.IsReversedPolarity = isReversedPolarity_;

                                //reverse reversed (for hard decision)
                                //this will neede rewriting when the demodulator will output soft symbols.
                                if (isReversedPolarity_)
                                {
                                    for (int i = 0; i < DataConsts.UWFinderFrameLength; i++)
                                    {
                                        frame_[i] = frame_[i] == 0 ? (byte)1 : (byte)0;
                                    }
                                }

                                uwfba_.UWFrame = frame_;
                                uwfba_.SymbolCount = DataConsts.UWFinderFrameLength;
                                uwfba_.IsMidStreamReversePolarity = isMidStreamReversePolarity_;
                                uwfba_.BER = Math.Min(nUW_, rUW_);
                                uwfba_.IsUncertain = true;
                                uwfba_.IsHardDecision = args.IsHardDecision;

                                OnUWFinderFrame?.Invoke(this, uwfba_);
                            }
                        }

                        UWFinderFrameArgs uwfba = new UWFinderFrameArgs();
                        byte[] frame = new byte[DataConsts.UWFinderFrameLength];
                        Array.Copy(symbolRegister, frame, DataConsts.UWFinderFrameLength);
                        uwfba.Length = DataConsts.UWFinderFrameLength;
                        uwfba.IsReversedPolarity = isReversedPolarity;

                        //reverse reversed (for hard decision)
                        //this will neede rewriting when the demodulator will output soft symbols.
                        if (isReversedPolarity)
                        {
                            for (int i = 0; i < DataConsts.UWFinderFrameLength; i++)
                            {
                                frame[i] = frame[i] == 0 ? (byte)1 : (byte)0;
                            }
                        }

                        uwfba.UWFrame = frame;
                        uwfba.SymbolCount = symbolCount;
                        uwfba.IsMidStreamReversePolarity = isMidStreamReversePolarity;
                        uwfba.BER = Math.Min(nUW, rUW);
                        uwfba.IsUncertain = false;
                        uwfba.IsHardDecision = args.IsHardDecision;

                        OnUWFinderFrame?.Invoke(this, uwfba);

                        /// There is no point in clearing up the symbolRegister
                        /// However also there is no point in detecting until we fill in the register again
                        symbolCount = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Frame detector method. It computes the distribution of unique words inside a packet of known length.
        /// If we perform this operation every time a new symbol comes out of the demodulator, there will be a
        /// point where the distribution will match the encoded stream and in that moment we have detected the frame.
        /// The array of symbols we are using has 2 * Consts.UWFinderFrameLength, that is 2 * 10368 symbols.
        /// We are parting the symbol array in two parts, | lower | higher | where the last arrived symbol sits at the
        /// leftmost position in the lower part.
        /// We always compute 10368 symbols, either the lower or the higher part only.
        /// </summary>
        /// <param name="lowestFrame">If true, compute the lower part, otherwise the higher part.</param>
        /// <param name="nUW">Returns the number of times the nornal UW was found.</param>
        /// <param name="rUW">returns the number of times the reverse polarity UW was found.</param>
        /// <param name="isReversedPolarity">Indicates reversed polarity. To decode this frame we will have to reverse all the symbols later on.</param>
        /// <param name="isMidStreamReversePolarity">This indicates a frame that has symbols that were one polarity for a while and then another.
        /// This could happen id the demodulator loses sync, or the audio input is losing samples, or the source is of low quality, or...</param>
        /// <param name="isReversedFirst">It indicates the polarity of the first computed symbols.</param>
        /// <returns></returns>
        private bool isFrameDetected(bool lowestFrame, out int nUW, out int rUW, out bool isReversedPolarity, out bool isMidStreamReversePolarity, out bool isReversedFirst)
        {
            bool isFirstSymbolDetermined = false;
            isReversedFirst = false;
            nUW = 0;
            rUW = 0;
            isReversedPolarity = false;
            isMidStreamReversePolarity = false;

            int patternPos = 0;

            /// the symbolRegister array stores data as it comes out of the demodulator, 1st symbol at the highest index
            /// in the symbolRegister
            /// there are 160 + 2 columns per row
            int symbolPos = DataConsts.UWFinderFrameLength - 1;
            int minPos = 0;
            if (!lowestFrame)
            {
                symbolPos = 2 * DataConsts.UWFinderFrameLength - 1;
                minPos = DataConsts.UWFinderFrameLength;
            }

            for (; symbolPos >= minPos; symbolPos -= 162)
            {
                //compute normal polarity
                nUW += nrmPolUwPattern[patternPos] ^ symbolRegister[symbolPos];
                nUW += nrmPolUwPattern[patternPos] ^ symbolRegister[symbolPos - 1];

                //compute reverse polarity
                rUW += revPolUwPattern[patternPos] ^ symbolRegister[symbolPos];
                rUW += revPolUwPattern[patternPos] ^ symbolRegister[symbolPos - 1];

                if ((!isFirstSymbolDetermined) && (rUW != nUW))
                { 
                    isFirstSymbolDetermined = true;
                    isReversedFirst = rUW > nUW;
                }

                patternPos++;

                //detect polarity change midstream
                if (!isMidStreamReversePolarity)
                {
                    isMidStreamReversePolarity = nUW % 2 != 0;
                }
            }

            isReversedPolarity = rUW <= tolerance;

            return (nUW <= tolerance || rUW <= tolerance);
        }

        /// <summary>
        /// We set the highest allowed number of incorrect UW words in a frame.
        /// If a frame has more incorrect UW than the tolerance, we skip the frame.
        /// </summary>
        /// <param name="tolerance">The highest allowed number of incorrect UW words in a frame.</param>
        public void SetTolerance(int tolerance)
        {
            if (tolerance < 0)
            {
                tolerance = 0;
            }
            else if (tolerance > DataConsts.MaxTolerance)
            {
                tolerance = DataConsts.MaxTolerance;
            }

            this.tolerance = tolerance;
        }
    }
}
