﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *   Bibliography:
 * 
 *   Calcutt, D. M., & Tetley, L. (2004). Satellite communications: principles and applications. Oxford: Elsevier.
 *   https://www.amazon.com/Satellite-Communications-Applications-David-Calcutt/dp/034061448X
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 *   
 *   Nera. (2015) Nera Saturn C Technical Manual. Billingstad: Nera ASA.
 *   ftp://161.87.213.193.static.cust.telenor.com/Manuals/Saturn%20C/SatC_Marine_Tech_Manual_A.pdf
 *   
 *
 * Scrambling prevents 0 or 1 from continuing excessively; if 0 or 1 continues, clock
 * recovery would be reduced at the BPSK modulator. For scrambling, the output is
 * gained by inputting to the output from the scramble generator and the modulo-2 adder.
 * 
 * Descrambling is the reverse of scrambling.
 * The 640 bytes frame is split into 160 groups, each of 4 consecutive bytes.
 * Each group either has all its bytes inverted, or is left as is depending of the
 * 0 or 1 output of the descramble generator, G = X3 + X4 + X5 + X7
 * 
 */

using ScytaleC.Interfaces;
using System;

namespace ScytaleC
{
    /// <summary>
    /// Instead of recalculating on the fly, we populate a descrambler array with the output of the 
    /// descramble generator.
    /// 
    /// </summary>
    class Descrambler : IDescrambler
    {
        private byte[] descramblerArray;
        private const int groupCount = 160;

        /// <summary>
        /// Populate the descrambler array with pseudo-random values.
        /// </summary>
        public Descrambler()
        {
            descramblerArray = new byte[groupCount];

            byte x7;
            byte x5;
            byte x4;
            byte x3;
            byte newByte;

            /// Initial state; the documentation found is incorrect as it indicates 0x40, however by
            /// corroboration with more documents and experimenting, the 0x80 generates the correct
            /// scrambling/descrambling array.
            byte register = 0x80;

            for (int i = 0; i < groupCount; i++)
            {
                x7 = (byte)(register & 0x01);
                descramblerArray[i] = x7;

                x5 = (byte)((register & 0x04) >> 2);
                x4 = (byte)((register & 0x08) >> 3);
                x3 = (byte)((register & 0x10) >> 4);

                newByte = (byte)(x7 ^ x5 ^ x4 ^ x3);

                register >>= 1;
                register = (byte)(register | (byte)(newByte << 7));
            }
        }

        public event EventHandler<DescrambledFrameArgs> OnDescrambledFrame;

        public void Process(ViterbiFrameArgs e)
        {
            byte[] destination = new byte[DataConsts.DescrambledFrameLength];

            /// Invert all
            for (int i = 0; i < DataConsts.DescrambledFrameLength; i++)
            {
                destination[i] = invertBits(e.ViterbiFrame[i]);
            }

            /// Apply the bitwise complement only for the "1" states of the descrambler array.
            int j = 0;
            for (int i = 0; i < groupCount; i++)
            {
                if (descramblerArray[i] == 1)
                {
                    destination[j] = (byte)~(destination[j]);
                    destination[j + 1] = (byte)~(destination[j + 1]);
                    destination[j + 2] = (byte)~(destination[j + 2]);
                    destination[j + 3] = (byte)~(destination[j + 3]);
                }
                j += 4;
            }

            DescrambledFrameArgs dpa = new DescrambledFrameArgs();
            dpa.Length = DataConsts.DescrambledFrameLength;
            dpa.DescrambledFrame = destination;

            // this member is defaulted here but it will be filled in properly
            // when detecting the bb packet
            //it gets used by the statistics
            dpa.IsBadBulletinBoard = true;

            OnDescrambledFrame?.Invoke(this, dpa);
        }

        /// <summary>
        /// bit 7 = bit 0
        /// bit 6 = bit 1
        /// bit 5 = bit 2
        /// bit 4 = bit 3
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        byte invertBits(byte input)
        {
            byte result;
            result = (byte)((input & 0x01));
            result <<= 1;
            result |= (byte)((input & 0x02) >> 1);
            result <<= 1;
            result |= (byte)((input & 0x04) >> 2);
            result <<= 1;
            result |= (byte)((input & 0x08) >> 3);
            result <<= 1;
            result |= (byte)((input & 0x16) >> 4);
            result <<= 1;
            result |= (byte)((input & 0x32) >> 5);
            result <<= 1;
            result |= (byte)((input & 0x64) >> 6);
            result <<= 1;
            result |= (byte)((input & 0x80) >> 7);

            return result;
        }
    }
}