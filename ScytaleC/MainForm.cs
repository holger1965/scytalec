﻿/*
 * microp11 2017
 * 
 * Sources: file or stream.
 * The decoding chain supports with minimum changes real IQ input and that would be the next step.
 * 
 * The data is stored into the sourceQue. It is consumed by the decoder and the fft display.
 * The decoder data output is stored into the decoderOutputQue. The decoder also has the capability of raising
 * events. This are synchronous events Windows style. If you want to use them, make sure your code does not
 * slow the decoding down. I would suggest less then 50ms per handler.
 * The decoder signal output goes into its separate queueu as it has a different speed than the data output.
 * The decoder data and signal outputs are consumed by the UI for display purposes.
 * 
 * TODO:
 *  Write a proper producer/consumer using Tasks and async and get rid of timers.
 *  Check the dynamic loading of the assemblies...
 *  Unify the sources under one common interface.
 *  
 *  https://stackoverflow.com/questions/15238714/net-local-assembly-load-failed-with-cas-policy
 *  https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml?&page=121
 *  https://stackoverflow.com/questions/2920696/how-generate-unique-integers-based-on-guids
 *  http://www.java2s.com/Code/CSharp/Network/TcpClientSample.htm
 *  
 */

using NAudio.Wave;
using ScytaleC.PacketDecoders;
using ScytaleC.Interfaces;
using ScytaleC.Properties;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;

namespace ScytaleC
{
    public partial class MainForm : Form
    {
        private WaveIn waveIn;
        private NetworkStream tcpIpStream;
        private WaveFormat tcpIpWaveFormat;
        private TcpClient tcpIpClient;
        private byte[] tcpIpData;
        private StreamQueue sourceQue = new StreamQueue();
        private WaveFileReader wavFile;
        private byte[] wavData;
        private bool continueToReadFile;
        private bool continueToReadTcpIp;
        private bool isFileInput;
        private bool isTcpIpInput;
        private WavReaderState readStateObj = null;
        private string exePath;
        private static Exception InterfaceNotFoundException;
        private int framesReceived = 0;
        private uint symbols = 0;

        int testCounter = 0;
        private bool consumingAllowed;
        private InmarsatCDecoder decoder;
        private Stopwatch sw;
        private ConcurrentQueue<object> decoderOutputQue;
        private ConcurrentQueue<SignalAttributesArgs> decoderSignalAttributesQue;

        private int udpFrameCount = 0;
        private UdpRepeater udpr;

        private Statistics stats;
        private long StreamId;

        public MainForm()
        {
            InitializeComponent();
            rtbInfo.Text = string.Format("Scytalec-C, version {0}{1}{1}", Assembly.GetExecutingAssembly().GetName().Version.ToString(), Environment.NewLine);
            InterfaceNotFoundException = new Exception("Interface not found!");

            exePath = AppDomain.CurrentDomain.BaseDirectory;
            FileOutput.SelectedPath = exePath;
            rSource_CheckedChanged(null, null);

            InitializePlaybackDevices();
            InitializeNetworkInterfaces();
            InitializeDemodulators();
            InitializeViterbiDecoders();
            InitializeRemainingVisualElements();

            decoderOutputQue = new ConcurrentQueue<object>();
            decoderSignalAttributesQue = new ConcurrentQueue<SignalAttributesArgs>();
            decoder = new InmarsatCDecoder(decoderOutputQue, decoderSignalAttributesQue);

            sw = new Stopwatch();
            stats = new Statistics();
            stats.OnStatsUpdate += Stats_OnStatsUpdate;

            //Set a new stream id every time we restart the decoder.
            DateTime now = DateTime.UtcNow;
            StreamId = (now.Ticks / 10000);
            string sl = StreamId.ToString();
            lblStreamId.Text = string.Format("SId: *{0,5}", sl.Substring(sl.Length - 5));

            //tcpip input
            tcpIpStream = null;
            tcpIpClient = null;
        }

        private void InitializeRemainingVisualElements()
        {
            rAudioDevice.Checked = Settings.Default.rAudioDevice;
            rFile.Checked = Settings.Default.rFile;
            rTcpIp.Checked = Settings.Default.rTcpIp;

            int selectedIndex = Settings.Default.comboPlaybackDevices;
            if (comboPlaybackDevices.Items.Count > selectedIndex)
            {
                comboPlaybackDevices.SelectedIndex = selectedIndex;
            }

            txtWaveFileInput.Text = Settings.Default.txtWaveFileInput;
            openWavFileInput.FileName = Settings.Default.openWavFileInput;

            selectedIndex = Settings.Default.comboNetworkInterfaces;
            if (comboNetworkInterfaces.Items.Count > selectedIndex)
            {
                comboNetworkInterfaces.SelectedIndex = selectedIndex;
            }

            txtIPAddress.Text = Settings.Default.txtIPAddress;

            selectedIndex = Settings.Default.comboDemodulators;
            if (comboDemodulators.Items.Count > selectedIndex)
            {
                comboDemodulators.SelectedIndex = selectedIndex;
            }

            cbCMA.Checked = Settings.Default.cbCMA;
            cbAGC.Checked = Settings.Default.cbAGC;
            nLoFreq.Value = Settings.Default.nLoFreq;
            nLoFreq_ValueChanged(this, null);
            nHiFreq.Value = Settings.Default.nHiFreq;
            nHiFreq_ValueChanged(this, null);

            nTolerance.Value = Settings.Default.nTolerance;

            selectedIndex = Settings.Default.comboViterbiDecoders;
            if (comboViterbiDecoders.Items.Count > selectedIndex)
            {
                comboViterbiDecoders.SelectedIndex = selectedIndex;
            }

            txtRawFrameUDPPort.Text = Settings.Default.txtRawFrameUDPPort;
            cbRawFrameUDP.Checked = Settings.Default.cbRawFrameUDP;

            cbSuspendSleepMode.Checked = Settings.Default.cbSuspendSleepMode;

            if (rAudioDevice.Checked)
            {
                ActiveControl = rAudioDevice;
            }
            else if (rFile.Checked)
            {
                ActiveControl = rFile;
            }
            else
            {
                ActiveControl = rTcpIp;
            }

            nMaxDisplayedFrames.Value = Settings.Default.nMaxDisplayedFrames;
        }

        private void Stats_OnStatsUpdate(object sender, StatsArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                lblRxFR.Text = string.Format("Rx FR: {0}", e.RxFRCount);
                lblLostFR.Text = string.Format("Lost FR: {0}", e.LostFRCount);
                lblBBER.Text = string.Format("BBER: {0}%", (int)e.BBERPercent);
            }));
        }

        private void PresentFrame(DescrambledFrameArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                try
                {
                    string packetHex = Utils.BytesToHexString(e.DescrambledFrame, 0, e.Length);
                    int frameNumber = e.DescrambledFrame[2] << 8 | e.DescrambledFrame[3];

                    stats.AddRxFR(frameNumber, false);

                    if (frameNumber < 0 || frameNumber > 9999)
                    {
                        //bad frame
                        rtbFrames.SelectionBackColor = Color.PaleVioletRed;
                        rtbFrames.SelectionColor = Color.WhiteSmoke;
                        rtbFrames.AppendText(string.Format(" NOK {0:0.00} {1}", sw.ElapsedMilliseconds / 1000.0, packetHex));

                        rtbFrames.SelectionBackColor = Color.White;
                        rtbFrames.SelectionColor = Color.Black;
                    }
                    else
                    {
                        //possibly good frame
                        rtbFrames.SelectionBackColor = Color.PaleGreen;
                        rtbFrames.SelectionColor = Color.DarkBlue;
                        rtbFrames.AppendText(string.Format("{0}", frameNumber));

                        rtbFrames.SelectionBackColor = Color.White;
                        rtbFrames.SelectionColor = Color.Black;
                        rtbFrames.AppendText(string.Format(" {0:0.00} {1}", sw.ElapsedMilliseconds / 1000.0, packetHex));
                    }
                    sw.Restart();
                }
                catch (Exception ex)
                {
                    Utils.Log.Error(ex);
                }

                #region RestrictContentSizeAndScrolling

                int maxLinesOnDisplay = (int)nMaxDisplayedFrames.Value;
                if (rtbFrames.Lines.Length > maxLinesOnDisplay)
                {
                    rtbFrames.SelectionStart = rtbFrames.GetFirstCharIndexFromLine(0);
                    rtbFrames.SelectionLength = rtbFrames.Lines[rtbFrames.Lines.Length - maxLinesOnDisplay].Length + 1;
                    rtbFrames.SelectedText = String.Empty;
                }

                if (ActiveControl == rtbFrames)
                {
                    SendKeys.Send("+{TAB}");
                }

                rtbFrames.AppendText(Environment.NewLine);

                //scroll to last
                if (btnFramesAutoScroll.Checked)
                {
                    rtbFrames.ScrollToCaret();
                }

                #endregion
            }));
        }

        private void OnDescrambledFrame(object sender, DescrambledFrameArgs e)
        {
            if (cbRawFrameUDP.Checked)
            {
                try
                {
                    //Append the StreamId to the frame
                    byte[] datagram = new byte[e.Length + 8];
                    Array.Copy(e.DescrambledFrame, datagram, e.Length);
                    byte[] sid = BitConverter.GetBytes(StreamId);
                    Array.Copy(sid, 0, datagram, e.Length, 8);

                    udpr?.Send(txtIPAddress.Text, Convert.ToInt32(txtRawFrameUDPPort.Text), Utils.BytesToHexString(datagram, 0, datagram.Length));
                    udpFrameCount++;
                    BeginInvoke(new Action(() =>
                    {
                        lblTxUDP.Text = string.Format("Tx UDP: {0}", udpFrameCount);
                    }));
                }
                catch (Exception ex)
                {
                    Utils.Log.Error(ex);
                }
            }

            PresentFrame(e);
        }

        /// <summary>
        /// https://stackoverflow.com/questions/22054414/update-ui-from-an-async-method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnUWFinderFrame(object sender, UWFinderFrameArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                framesReceived++;

                string s = (e.IsReversedPolarity) ? "I" : "N";
                s += (e.IsUncertain) ? "U" : "-";
                s += (e.IsMidStreamReversePolarity) ? "M" : "-";

                string s1 = string.Format("{0:00000} {1} {2:#0 }", e.SymbolCount, s, e.BER);
                rtbFrames.AppendText(s1);
            }));
        }

        private void OnDemodulatedSymbols(object sender, DemodulatedSymbolsArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                symbols += (uint)e.Length;

                lblRxSYM.Text = string.Format("Rx SYM: {0:0.00}M", symbols / 1.0e6);

                if (cbSuspendSleepMode.Checked)
                {
                    NativeMethods.SetThreadExecutionState(EXECUTION_STATE.ES_DISPLAY_REQUIRED);
                }
            }));
        }

        private void OnDemodulatedSignalAttributes(object sender, SignalAttributesArgs e)
        {
            if ((int)e.Frequency == (int)nLoFreq.Value)
            {
                decoder?.SetCenterFrequency((double)nHiFreq.Value);
                return;
            }

            BeginInvoke(new Action(() =>
            {
                fftD.SetSignalAttributes(e);
                lblSyncERR.Text = string.Format("Sync ERR: {0}", e.SyncCounter);
            }));
        }

        private void StopConsuming()
        {
            consumingAllowed = false;
        }

        private async Task<int> StartConsuming()
        {
            consumingAllowed = true;
            while (consumingAllowed)
            {
                await Task.Delay(10);

                // for debugging
                if (sourceQue.Count > 100 && !isFileInput)
                {
                    Debug.WriteLine("que.Count = {0}", sourceQue.Count);
                }

                AudioSampleArgs asa = new AudioSampleArgs();
                float[] samples;
                bool success = sourceQue.TryTake(out samples);
                if (!success)
                {
                    continue;
                }

                asa.Samples = samples;
                int length = asa.Samples.Length;
                asa.Length = length;

                for (int index = 0; index < length; index++)
                {
                    fftD.sampleAggregator.Add(asa.Samples[index]);
                }
                decoder?.Process(asa);
            }
            return 0;
        }

        #region ViterbiDecoders
        private void InitializeViterbiDecoders()
        {
            comboViterbiDecoders.Items.Clear();
            try
            {
                string[] dlls = Directory.GetFiles(exePath, "*ViterbiDecoder.dll", SearchOption.TopDirectoryOnly);
                foreach (string dll in dlls)
                {
                    try
                    {
                        IViterbiDecoder viterbiDecoder = LoadViterbiDecoder(dll);
                        ComboBoxItem cbi = new ComboBoxItem(viterbiDecoder.Name, viterbiDecoder);
                        comboViterbiDecoders.Items.Add(cbi);
                        rtbInfo.AppendText(string.Format("Loaded viterbi decoder \"{0}\" at location {1}.", viterbiDecoder.Name, dll));
                        rtbInfo.AppendText(Environment.NewLine);
                    }
                    catch (Exception ex1)
                    {
                        rtbInfo.AppendText(string.Format("Loading viterbi decoder in exception {0}", ex1.Message));
                        rtbInfo.AppendText(Environment.NewLine);
                    }
                }
            }
            catch (Exception ex)
            {
                rtbInfo.AppendText(string.Format("Loading viterbi decoder exception {0}", ex.Message));
                rtbInfo.AppendText(Environment.NewLine);
                Utils.Log.Error(ex);
            }
        }

        private static IViterbiDecoder LoadViterbiDecoder(string assemblyPath)
        {
            Assembly assembly = Assembly.LoadFile(assemblyPath);
            foreach (Type item in assembly.GetTypes())
            {
                if (!item.IsClass)
                {
                    continue;
                }

                if (item.GetInterfaces().Contains(typeof(IViterbiDecoder)))
                {
                    return (IViterbiDecoder)Activator.CreateInstance(item);
                }
            }

            throw InterfaceNotFoundException;
        }

        private void SetViterbiDecoder()
        {
            IViterbiDecoder viterbiDecoder = (IViterbiDecoder)(((ComboBoxItem)comboViterbiDecoders.SelectedItem).Value);
            decoder.SetViterbiDecoder(viterbiDecoder);
            decoder.SetTolerance((int)nTolerance.Value);
        }

        #endregion

        #region Demodulators
        private void InitializeDemodulators()
        {
            comboDemodulators.Items.Clear();
            try
            {
                string[] dlls = Directory.GetFiles(exePath, "*Demodulator.dll", SearchOption.TopDirectoryOnly);
                foreach (string dll in dlls)
                {
                    try
                    {
                        IDemodulator demodulator = LoadDemodulator(dll);
                        ComboBoxItem cbi = new ComboBoxItem(demodulator.Name, demodulator);
                        comboDemodulators.Items.Add(cbi);
                        rtbInfo.AppendText(string.Format("Loaded demodulator \"{0}\" at location {1}.", demodulator.Name, dll));
                        rtbInfo.AppendText(Environment.NewLine);
                    }
                    catch (Exception ex1)
                    {
                        rtbInfo.AppendText(string.Format("Loading demodulator in exception {0}", ex1.Message));
                        rtbInfo.AppendText(Environment.NewLine);
                    }
                }
            }
            catch (Exception ex)
            {
                rtbInfo.AppendText(string.Format("Loading demodulator exception {0}", ex.Message));
                rtbInfo.AppendText(Environment.NewLine);
            }
        }

        private static IDemodulator LoadDemodulator(string assemblyPath)
        {
            Assembly assembly = Assembly.LoadFile(assemblyPath);
            foreach (Type item in assembly.GetTypes())
            {
                if (!item.IsClass)
                {
                    continue;
                }

                if (item.GetInterfaces().Contains(typeof(IDemodulator)))
                {
                    return (IDemodulator)Activator.CreateInstance(item);
                }
            }

            throw InterfaceNotFoundException;
        }

        private void SetDemodulator()
        {
            IDemodulator demodulator = (IDemodulator)(((ComboBoxItem)comboDemodulators.SelectedItem).Value);
            decoder.SetDemodulator(demodulator);
            decoder.SetCMAEnabled(cbCMA.Checked);
            decoder.SetAGCEnabled(cbAGC.Checked);
            decoder.SetLoFreq((int)nLoFreq.Value);
            decoder.SetHiFreq((int)nHiFreq.Value);

            /// Let fftD know about demodulator as it needs to suggest cf based on user interaction
            /// At least for the time being as this can be made an event triggered by fftD and
            /// handled by the main form.
            fftD.Demodulator = demodulator;
        }

        #endregion

        private void InitializeNetworkInterfaces()
        {
            try
            {
                Dictionary<NetworkInterface, string> selection = new Dictionary<NetworkInterface, string>();

                foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (ni.OperationalStatus == OperationalStatus.Up && ni.SupportsMulticast && ni.GetIPProperties().GetIPv4Properties() != null)
                    {
                        int id = ni.GetIPProperties().GetIPv4Properties().Index;
                        selection.Add(ni, ni.Name);
                    }
                }

                comboNetworkInterfaces.DataSource = new BindingSource(selection, null);
                comboNetworkInterfaces.ValueMember = "Key";
                comboNetworkInterfaces.DisplayMember = "Value";
            }
            catch (Exception ex)
            {
                rtbInfo.AppendText(string.Format("No network interface detected. {0}", ex.Message));
                rtbInfo.AppendText(Environment.NewLine);
                Utils.Log.Error(ex);
            }
        }

        private void InitializePlaybackDevices()
        {
            try
            {
                waveIn = new WaveIn();
                waveIn.RecordingStopped += WaveIn_RecordingStopped;

                Dictionary<int, string> selection = new Dictionary<int, string>();

                int waveInDevices = WaveIn.DeviceCount;
                for (int waveInDevice = 0; waveInDevice < waveInDevices; waveInDevice++)
                {
                    WaveInCapabilities deviceInfo = WaveIn.GetCapabilities(waveInDevice);
                    selection.Add(waveInDevice, deviceInfo.ProductName);
                }

                comboPlaybackDevices.DataSource = new BindingSource(selection, null);
                comboPlaybackDevices.ValueMember = "Key";
                comboPlaybackDevices.DisplayMember = "Value";
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
                MessageBox.Show("No sound system detected.");
            }
        }

        private void WaveIn_RecordingStopped(object sender, StoppedEventArgs e)
        {
            if (waveIn != null)
            {
                waveIn.DataAvailable -= waveIn_DataAvailable;
                waveIn.RecordingStopped -= waveIn_RecordingStopped;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                if ((string)btnStart.Tag == "start")
                {
                    Start();
                }
                else
                {
                    Stop();
                    rSource_CheckedChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
            }
        }

        private void SetButtons(int action)
        {
            switch (action)
            {
                case 0:
                    btnStart.Image = Resources.button_blue_stop;
                    EnableDisableControls(false);
                    btnStart.Tag = "stop";
                    break;

                case 1:
                    btnStart.Image = Resources.button_blue_play;
                    EnableDisableControls(true);
                    btnStart.Tag = "start";
                    break;

                case 2:
                    btnMenu.Image = Resources.exchange;
                    btnMenu.Tag = "menu-open";
                    break;

                case 3:
                    btnMenu.Image = Resources.exchange_back;
                    btnMenu.Tag = "menu-close";
                    break;
            }
        }

        private void EnableDisableControls(bool enabled)
        {
            rAudioDevice.Enabled = enabled;
            comboPlaybackDevices.Enabled = enabled;

            rFile.Enabled = enabled;
            txtWaveFileInput.Enabled = enabled;
            btnSelectWav.Enabled = enabled;

            rTcpIp.Enabled = enabled;
            txtTcpIpAddress.Enabled = enabled;
            txtTcpIpPort.Enabled = enabled;

            comboDemodulators.Enabled = enabled;
            comboViterbiDecoders.Enabled = enabled;
            comboNetworkInterfaces.Enabled = enabled;
        }

        private void Stop()
        {
            SetButtons(1);
            stats.Stop();

            if (isFileInput)
            {
                continueToReadFile = false;
            }
            else if (isTcpIpInput)
            {
                continueToReadTcpIp = false;
            }
            else
            {
                if (readStateObj != null)
                {
                    readStateObj.ManualEvent.Set();
                    readStateObj = null;
                }
                waveIn.StopRecording();

                framesReceived = 0;
            }
            StopConsuming();
        }

        private async void Start()
        {
            // clear queue
            while (sourceQue.Count > 0)
            {
                sourceQue.TryTake(out float[] samplesToClear);
            }

            try
            {
                SetDemodulator();
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
                MessageBox.Show("No demodulator detected.");
                return;
            }

            try
            {
                SetViterbiDecoder();
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
                MessageBox.Show("No Viterbi decoder detected.");
                return;
            }

            SetButtons(0);
            stats.Restart();

            //Set network interface used
            udpr = new UdpRepeater(((KeyValuePair<NetworkInterface, string>)comboNetworkInterfaces.SelectedItem).Key);

            isFileInput = rFile.Checked;
            isTcpIpInput = rTcpIp.Checked;
            try
            {
                if (isFileInput)
                {
                    continueToReadFile = true;

                    if (wavFile != null)
                    {
                        wavFile.Dispose();
                    }
                    wavFile = new WaveFileReader(txtWaveFileInput.Text);
                    wavData = new byte[DataConsts.SamplesPerRead];

                    // clear queue
                    while (sourceQue.Count > 0)
                    {
                        sourceQue.TryTake(out float[] samplesToClear);
                    }
                    sourceQue.SetInputDataType(wavFile.WaveFormat);
                    wavFile.BeginRead(wavData, 0, DataConsts.SamplesPerRead, OnReadAsyncComplete, null);

                    await Task.Run(() => StartConsuming());
                }
                else if (isTcpIpInput)
                {
                    if (tcpIpStream != null)
                    {
                        tcpIpStream.Dispose();
                        tcpIpStream = null;
                    }

                    if (tcpIpClient != null)
                    {
                        tcpIpClient.Close();
                        tcpIpClient = null;
                    }

                    tcpIpWaveFormat = new WaveFormat(DataConsts.AudioSampleRate, DataConsts.AudioBitsPerSample, DataConsts.AudioChannels);
                    tcpIpData = new byte[DataConsts.SamplesPerRead];

                    tcpIpClient = new TcpClient(txtTcpIpAddress.Text, Convert.ToInt32(txtTcpIpPort.Text));
                    continueToReadTcpIp = true;

                    // clear queue
                    while (sourceQue.Count > 0)
                    {
                        sourceQue.TryTake(out float[] samplesToClear);
                    }

                    sourceQue.SetInputDataType(tcpIpWaveFormat);
                    tcpIpStream = tcpIpClient.GetStream();
                    tcpIpStream.BeginRead(tcpIpData, 0, DataConsts.SamplesPerRead, OnTcpIpReadAsyncComplete, null);

                    await Task.Run(() => StartConsuming());
                }
                else
                {
                    if (waveIn != null)
                    {
                        waveIn.DataAvailable -= waveIn_DataAvailable;
                        waveIn.RecordingStopped -= waveIn_RecordingStopped;
                        waveIn.Dispose();
                        waveIn = new WaveIn();
                    }

                    waveIn.DeviceNumber = ((KeyValuePair<int, string>)comboPlaybackDevices.SelectedItem).Key;
                    waveIn.DataAvailable += waveIn_DataAvailable;
                    waveIn.RecordingStopped += waveIn_RecordingStopped;

                    waveIn.WaveFormat = new WaveFormat(DataConsts.AudioSampleRate, DataConsts.AudioBitsPerSample, DataConsts.AudioChannels);
                    // clear queue
                    while (sourceQue.Count > 0)
                    {
                        sourceQue.TryTake(out float[] samplesToClear);
                    }
                    sourceQue.SetInputDataType(waveIn.WaveFormat);
                    waveIn.StartRecording();

                    await Task.Run(() => StartConsuming());
                }
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
                Stop();
            }
        }

        private void OnTcpIpReadAsyncComplete(IAsyncResult ar)
        {
            int bytesRead;
            try
            {
                bytesRead = tcpIpStream.EndRead(ar);
                byte[] data = new byte[bytesRead];
                Buffer.BlockCopy(tcpIpData, 0, data, 0, bytesRead);
                sourceQue.Store(data, bytesRead, testCounter++, false);
            }
            catch
            {
                bytesRead = 0;
            }

            if (!continueToReadTcpIp || bytesRead == 0)
            {
                if (bytesRead == 0)
                {
                    //eof reached
                    SpinWait.SpinUntil(() => sourceQue.Count == 0 || !continueToReadTcpIp);

                    if (continueToReadTcpIp)
                    {
                        Invoke((MethodInvoker)delegate
                        {
                            btnStart_Click(null, null);
                        });
                    }
                }
                else
                {
                    //the user has pressed the stop button
                    // clear queue
                    while (sourceQue.Count > 0)
                    {
                        sourceQue.TryTake(out float[] samplesToClear);
                    }
                }
            }
            else
            {
                tcpIpData = new byte[DataConsts.SamplesPerRead];
                tcpIpStream.BeginRead(tcpIpData, 0, DataConsts.SamplesPerRead, OnTcpIpReadAsyncComplete, null);
            }
        }

        private void OnReadAsyncComplete(IAsyncResult ar)
        {
            int bytesRead;
            try
            {
                bytesRead = wavFile.EndRead(ar);
                byte[] data = new byte[bytesRead];
                Buffer.BlockCopy(wavData, 0, data, 0, bytesRead);
                sourceQue.Store(data, bytesRead, testCounter++, false);
            }
            catch
            {
                bytesRead = 0;
            }

            if (!continueToReadFile || bytesRead == 0)
            {
                if (wavFile != null)
                {
                    wavFile.Dispose();
                }
                if (bytesRead == 0)
                {
                    //eof reached
                    SpinWait.SpinUntil(() => sourceQue.Count == 0 || !continueToReadFile);

                    if (continueToReadFile)
                    {
                        Invoke((MethodInvoker)delegate
                        {
                            btnStart_Click(null, null);
                        });
                    }
                }
                else
                {
                    //the user has pressed the stop button
                    // clear queue
                    while (sourceQue.Count > 0)
                    {
                        sourceQue.TryTake(out float[] samplesToClear);
                    }
                }
            }
            else
            {
                wavData = new byte[DataConsts.SamplesPerRead];
                wavFile.BeginRead(wavData, 0, DataConsts.SamplesPerRead, OnReadAsyncComplete, null);
            }
        }

        private void waveIn_RecordingStopped(object sender, StoppedEventArgs e)
        {
            StopConsuming();

            if (waveIn != null)
            {
                waveIn.DataAvailable -= waveIn_DataAvailable;
                waveIn.RecordingStopped -= waveIn_RecordingStopped;
            }
        }

        private void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            int bytesRead = e.Buffer.Length;
            byte[] data = new byte[bytesRead];
            Buffer.BlockCopy(e.Buffer, 0, data, 0, bytesRead);
            sourceQue.Store(data, bytesRead, testCounter++, true);
        }

        private void ReadUISettings()
        {
            isFileInput = rFile.Checked;
            isTcpIpInput = rTcpIp.Checked;
        }

        private void btnSelectWav_Click(object sender, EventArgs e)
        {
            if (openWavFileInput.ShowDialog() == DialogResult.OK)
            {
                txtWaveFileInput.Text = openWavFileInput.FileName;
            }
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            if ((string)btnMenu.Tag == "menu-close")
            {
                SetButtons(2);
            }
            else
            {
                SetButtons(3);
            }

            panelMenu.Visible = !panelMenu.Visible;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopConsuming();
            continueToReadFile = false;
            continueToReadTcpIp = false;
            Application.DoEvents();
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            About frm = new About();
            frm.Show();
        }

        private void cbCMA_CheckedChanged(object sender, EventArgs e)
        {
            decoder?.SetCMAEnabled(cbCMA.Checked);
        }

        private void ConsumerDecoderOutput_Tick(object sender, EventArgs e)
        {
            object r;
            if (decoderOutputQue.TryDequeue(out r))
            {
                Type t = r.GetType();
                if (t.Equals(typeof(DemodulatedSymbolsArgs)))
                {
                    OnDemodulatedSymbols(sender, (DemodulatedSymbolsArgs)r);
                }
                else if (t.Equals(typeof(UWFinderFrameArgs)))
                {
                    OnUWFinderFrame(sender, (UWFinderFrameArgs)r);
                }
                else if (t.Equals(typeof(DescrambledFrameArgs)))
                {
                    OnDescrambledFrame(sender, (DescrambledFrameArgs)r);
                }
                else
                {
                    Utils.Log.Debug("The decoderOutputQue has {0} unhandled objects.", decoderOutputQue.Count);
                }
            }
        }

        private void ConsumerSignalAttributes_Tick(object sender, EventArgs e)
        {
            SignalAttributesArgs r;
            if (decoderSignalAttributesQue.TryDequeue(out r))
            {
                OnDemodulatedSignalAttributes(this, (SignalAttributesArgs)r);
            }
        }

        private void comboDemodulators_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                IDemodulator demodulator = (IDemodulator)(((ComboBoxItem)comboDemodulators.SelectedItem).Value);
                cbCMA.Checked = demodulator.IsCMASupported();
                cbCMA.Enabled = demodulator.IsCMASupported();
                cbAGC.Checked = demodulator.IsAGCSupported();
                cbAGC.Enabled = demodulator.IsAGCSupported();
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
            }
        }

        private void cbAGC_CheckedChanged(object sender, EventArgs e)
        {
            decoder?.SetAGCEnabled(cbAGC.Checked);
        }

        private void nLoFreq_ValueChanged(object sender, EventArgs e)
        {
            //set lo frequency range of the demodulator
            decoder?.SetLoFreq((int)nLoFreq.Value);
            fftD?.SetLoFreq((int)nLoFreq.Value);
        }

        private void nHiFreq_ValueChanged(object sender, EventArgs e)
        {
            //set hi frequency range of the demodulator
            decoder?.SetHiFreq((int)nHiFreq.Value);
            fftD?.SetHiFreq((int)nHiFreq.Value);
        }

        /// <summary>
        /// Everywhere when we have combo boxes, we should go by the text, not by the index, as the device numbers 
        /// not always point to the same device. This is easy for now.
        /// TODO
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Settings.Default.rAudioDevice = rAudioDevice.Checked;
            Settings.Default.comboPlaybackDevices = comboPlaybackDevices.SelectedIndex;
            Settings.Default.rFile = rFile.Checked;
            Settings.Default.txtWaveFileInput = txtWaveFileInput.Text;
            Settings.Default.openWavFileInput = openWavFileInput.FileName;
            Settings.Default.rTcpIp = rTcpIp.Checked;
            Settings.Default.txtTcpIpAddress = txtTcpIpAddress.Text;
            Settings.Default.txtTcpIpPort = txtTcpIpPort.Text;
            Settings.Default.comboNetworkInterfaces = comboNetworkInterfaces.SelectedIndex;
            Settings.Default.txtIPAddress = txtIPAddress.Text;
            Settings.Default.comboDemodulators = comboDemodulators.SelectedIndex;
            Settings.Default.cbCMA = cbCMA.Checked;
            Settings.Default.cbAGC = cbAGC.Checked;
            Settings.Default.nLoFreq = nLoFreq.Value;
            Settings.Default.nHiFreq = nHiFreq.Value;
            Settings.Default.nTolerance = nTolerance.Value;
            Settings.Default.comboViterbiDecoders = comboViterbiDecoders.SelectedIndex;
            Settings.Default.txtRawFrameUDPPort = txtRawFrameUDPPort.Text;
            Settings.Default.cbRawFrameUDP = cbRawFrameUDP.Checked;
            Settings.Default.cbSuspendSleepMode = cbSuspendSleepMode.Checked;
            Settings.Default.nMaxDisplayedFrames = nMaxDisplayedFrames.Value;

            Settings.Default.Save();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            lblVersion.Text = string.Format("v.{0} BETA", FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly‌​().Location).Product‌​Version);
            try
            {
                rtbNotes.LoadFile(AppDomain.CurrentDomain.BaseDirectory + "notes.rtf", RichTextBoxStreamType.PlainText);
            }
            catch (Exception ex)
            {
                rtbNotes.Text = ex.Message;
            }
        }

        private void lostPacketTimer_Tick(object sender, EventArgs e)
        {
            //evaluate lost packets
            DateTime time = DateTime.UtcNow;
            if (time.CompareTo(stats.GetMarkTime()) == -1)
            {
                //the evaluation for lost packets is currently suspended
                return;
            }
            else
            {
                stats.EvaluateLostPackets(time, rFile.Checked);
            }
        }

        private void btnPacketsAutoScroll_Click(object sender, EventArgs e)
        {
            (sender as ToolStripButton).Checked = !(sender as ToolStripButton).Checked;
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            foreach (string line in rtbFrames.Lines)
                sb.AppendLine(line);

            if (sb.Length > 0)
            {
                Clipboard.SetText(sb.ToString());
            }
        }

        private void btnPacketsClear_Click(object sender, EventArgs e)
        {
            rtbFrames.Clear();
        }

        private void rSource_CheckedChanged(object sender, EventArgs e)
        {
            if (rAudioDevice.Checked)
            {
                comboPlaybackDevices.Enabled = true;
                txtWaveFileInput.Enabled = false;
                btnSelectWav.Enabled = false;
                txtTcpIpAddress.Enabled = false;
                txtTcpIpPort.Enabled = false;
            }
            else if (rFile.Checked)
            {
                comboPlaybackDevices.Enabled = false;
                txtWaveFileInput.Enabled = true;
                btnSelectWav.Enabled = true;
                txtTcpIpAddress.Enabled = false;
                txtTcpIpPort.Enabled = false;
            }
            else //rTcpIp
            {
                comboPlaybackDevices.Enabled = false;
                txtWaveFileInput.Enabled = false;
                btnSelectWav.Enabled = false;
                txtTcpIpAddress.Enabled = true;
                txtTcpIpPort.Enabled = true;
            }
        }
    }

    internal class WavReaderState
    {
        WaveFileReader wfr = null;
        byte[] readArray = null;
        ManualResetEvent manualEvent = null;

        internal WaveFileReader FStream
        {
            get
            {
                return wfr;
            }
        }

        public byte[] ReadArray
        {
            get
            {
                return readArray;
            }
        }

        public ManualResetEvent ManualEvent
        {
            get
            {
                return manualEvent;
            }
        }
    }

    /// <summary>
    /// https://stackoverflow.com/questions/3063320/combobox-adding-text-and-value-to-an-item-no-binding-source, Adam Markowitz
    /// </summary>
    internal class ComboBoxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public ComboBoxItem (string text, object value)
        {
            Text = text;
            Value = value;
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
