﻿namespace ScytaleC
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                waveIn?.Dispose();
                udpr?.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panelMenu = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nMaxDisplayedFrames = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.cbSuspendSleepMode = new System.Windows.Forms.CheckBox();
            this.gbFrames = new System.Windows.Forms.GroupBox();
            this.cbRawFrameUDP = new System.Windows.Forms.CheckBox();
            this.txtRawFrameUDPPort = new System.Windows.Forms.TextBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboViterbiDecoders = new System.Windows.Forms.ComboBox();
            this.gbUWFinder = new System.Windows.Forms.GroupBox();
            this.nTolerance = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.gbDemodulator = new System.Windows.Forms.GroupBox();
            this.nHiFreq = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.nLoFreq = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.cbAGC = new System.Windows.Forms.CheckBox();
            this.cbCMA = new System.Windows.Forms.CheckBox();
            this.comboDemodulators = new System.Windows.Forms.ComboBox();
            this.gbRepeater = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboNetworkInterfaces = new System.Windows.Forms.ComboBox();
            this.txtIPAddress = new System.Windows.Forms.TextBox();
            this.gbSignalInput = new System.Windows.Forms.GroupBox();
            this.txtTcpIpAddress = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTcpIpPort = new System.Windows.Forms.TextBox();
            this.rTcpIp = new System.Windows.Forms.RadioButton();
            this.rFile = new System.Windows.Forms.RadioButton();
            this.rAudioDevice = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.comboPlaybackDevices = new System.Windows.Forms.ComboBox();
            this.btnSelectWav = new System.Windows.Forms.Button();
            this.txtWaveFileInput = new System.Windows.Forms.TextBox();
            this.lblFile = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblRxSYM = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel6 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblBBER = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblRxFR = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel7 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel10 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblLostFR = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblTxUDP = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblSyncERR = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStreamId = new System.Windows.Forms.ToolStripStatusLabel();
            this.openWavFileInput = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCredits = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnMenu = new System.Windows.Forms.Button();
            this.tabFrames = new System.Windows.Forms.TabPage();
            this.rtbFrames = new System.Windows.Forms.RichTextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnPacketsClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.btnFramesAutoScroll = new System.Windows.Forms.ToolStripButton();
            this.ContentControl = new System.Windows.Forms.TabControl();
            this.tabInfo = new System.Windows.Forms.TabPage();
            this.rtbNotes = new System.Windows.Forms.RichTextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rtbInfo = new System.Windows.Forms.RichTextBox();
            this.ConsumerDecoderOutput = new System.Windows.Forms.Timer(this.components);
            this.ConsumerSignalAttributes = new System.Windows.Forms.Timer(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.lostPacketTimer = new System.Windows.Forms.Timer(this.components);
            this.lblVersion = new System.Windows.Forms.Label();
            this.fftD = new ScytaleC.FftDisplay();
            this.panelMenu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxDisplayedFrames)).BeginInit();
            this.gbFrames.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbUWFinder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nTolerance)).BeginInit();
            this.gbDemodulator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nHiFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nLoFreq)).BeginInit();
            this.gbRepeater.SuspendLayout();
            this.gbSignalInput.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabFrames.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.ContentControl.SuspendLayout();
            this.tabInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMenu
            // 
            this.panelMenu.AutoScroll = true;
            this.panelMenu.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panelMenu.Controls.Add(this.groupBox1);
            this.panelMenu.Controls.Add(this.gbFrames);
            this.panelMenu.Controls.Add(this.groupBox2);
            this.panelMenu.Controls.Add(this.gbUWFinder);
            this.panelMenu.Controls.Add(this.gbDemodulator);
            this.panelMenu.Controls.Add(this.gbRepeater);
            this.panelMenu.Controls.Add(this.gbSignalInput);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 77);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Padding = new System.Windows.Forms.Padding(2);
            this.panelMenu.Size = new System.Drawing.Size(216, 586);
            this.panelMenu.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nMaxDisplayedFrames);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.cbSuspendSleepMode);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(2, 541);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(195, 73);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // nMaxDisplayedFrames
            // 
            this.nMaxDisplayedFrames.Increment = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nMaxDisplayedFrames.Location = new System.Drawing.Point(91, 42);
            this.nMaxDisplayedFrames.Maximum = new decimal(new int[] {
            15000,
            0,
            0,
            0});
            this.nMaxDisplayedFrames.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nMaxDisplayedFrames.Name = "nMaxDisplayedFrames";
            this.nMaxDisplayedFrames.Size = new System.Drawing.Size(99, 20);
            this.nMaxDisplayedFrames.TabIndex = 92;
            this.nMaxDisplayedFrames.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 13);
            this.label9.TabIndex = 91;
            this.label9.Text = "Display Lines:";
            // 
            // cbSuspendSleepMode
            // 
            this.cbSuspendSleepMode.AutoSize = true;
            this.cbSuspendSleepMode.Location = new System.Drawing.Point(22, 19);
            this.cbSuspendSleepMode.Name = "cbSuspendSleepMode";
            this.cbSuspendSleepMode.Size = new System.Drawing.Size(125, 17);
            this.cbSuspendSleepMode.TabIndex = 90;
            this.cbSuspendSleepMode.Text = "Suspend sleep mode";
            this.cbSuspendSleepMode.UseVisualStyleBackColor = true;
            // 
            // gbFrames
            // 
            this.gbFrames.Controls.Add(this.cbRawFrameUDP);
            this.gbFrames.Controls.Add(this.txtRawFrameUDPPort);
            this.gbFrames.Controls.Add(this.checkBox11);
            this.gbFrames.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbFrames.Location = new System.Drawing.Point(2, 493);
            this.gbFrames.Name = "gbFrames";
            this.gbFrames.Size = new System.Drawing.Size(195, 48);
            this.gbFrames.TabIndex = 15;
            this.gbFrames.TabStop = false;
            this.gbFrames.Text = "Frame Detection and CRC";
            // 
            // cbRawFrameUDP
            // 
            this.cbRawFrameUDP.AutoSize = true;
            this.cbRawFrameUDP.Location = new System.Drawing.Point(140, 19);
            this.cbRawFrameUDP.Name = "cbRawFrameUDP";
            this.cbRawFrameUDP.Size = new System.Drawing.Size(49, 17);
            this.cbRawFrameUDP.TabIndex = 92;
            this.cbRawFrameUDP.Text = "UDP";
            this.cbRawFrameUDP.UseVisualStyleBackColor = true;
            // 
            // txtRawFrameUDPPort
            // 
            this.txtRawFrameUDPPort.Location = new System.Drawing.Point(91, 17);
            this.txtRawFrameUDPPort.Name = "txtRawFrameUDPPort";
            this.txtRawFrameUDPPort.Size = new System.Drawing.Size(43, 20);
            this.txtRawFrameUDPPort.TabIndex = 91;
            this.txtRawFrameUDPPort.Text = "15003";
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Checked = true;
            this.checkBox11.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox11.Enabled = false;
            this.checkBox11.Location = new System.Drawing.Point(22, 19);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(48, 17);
            this.checkBox11.TabIndex = 90;
            this.checkBox11.Text = "Raw";
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboViterbiDecoders);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(2, 444);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(195, 49);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Viterbi Decoder";
            // 
            // comboViterbiDecoders
            // 
            this.comboViterbiDecoders.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboViterbiDecoders.FormattingEnabled = true;
            this.comboViterbiDecoders.Items.AddRange(new object[] {
            "Default"});
            this.comboViterbiDecoders.Location = new System.Drawing.Point(21, 21);
            this.comboViterbiDecoders.Name = "comboViterbiDecoders";
            this.comboViterbiDecoders.Size = new System.Drawing.Size(169, 21);
            this.comboViterbiDecoders.TabIndex = 4;
            // 
            // gbUWFinder
            // 
            this.gbUWFinder.Controls.Add(this.nTolerance);
            this.gbUWFinder.Controls.Add(this.label4);
            this.gbUWFinder.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbUWFinder.Location = new System.Drawing.Point(2, 398);
            this.gbUWFinder.Name = "gbUWFinder";
            this.gbUWFinder.Size = new System.Drawing.Size(195, 46);
            this.gbUWFinder.TabIndex = 9;
            this.gbUWFinder.TabStop = false;
            this.gbUWFinder.Text = "Unique Words Finder";
            // 
            // nTolerance
            // 
            this.nTolerance.Location = new System.Drawing.Point(91, 17);
            this.nTolerance.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nTolerance.Name = "nTolerance";
            this.nTolerance.Size = new System.Drawing.Size(98, 20);
            this.nTolerance.TabIndex = 82;
            this.nTolerance.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 81;
            this.label4.Text = "Tolerance:";
            // 
            // gbDemodulator
            // 
            this.gbDemodulator.Controls.Add(this.nHiFreq);
            this.gbDemodulator.Controls.Add(this.label7);
            this.gbDemodulator.Controls.Add(this.nLoFreq);
            this.gbDemodulator.Controls.Add(this.label6);
            this.gbDemodulator.Controls.Add(this.cbAGC);
            this.gbDemodulator.Controls.Add(this.cbCMA);
            this.gbDemodulator.Controls.Add(this.comboDemodulators);
            this.gbDemodulator.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbDemodulator.Location = new System.Drawing.Point(2, 277);
            this.gbDemodulator.Name = "gbDemodulator";
            this.gbDemodulator.Size = new System.Drawing.Size(195, 121);
            this.gbDemodulator.TabIndex = 2;
            this.gbDemodulator.TabStop = false;
            this.gbDemodulator.Text = "Demodulator";
            // 
            // nHiFreq
            // 
            this.nHiFreq.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nHiFreq.Location = new System.Drawing.Point(91, 92);
            this.nHiFreq.Maximum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            this.nHiFreq.Minimum = new decimal(new int[] {
            2050,
            0,
            0,
            0});
            this.nHiFreq.Name = "nHiFreq";
            this.nHiFreq.Size = new System.Drawing.Size(98, 20);
            this.nHiFreq.TabIndex = 96;
            this.nHiFreq.Value = new decimal(new int[] {
            2500,
            0,
            0,
            0});
            this.nHiFreq.ValueChanged += new System.EventHandler(this.nHiFreq_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(21, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 95;
            this.label7.Text = "Hi Freq:";
            // 
            // nLoFreq
            // 
            this.nLoFreq.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nLoFreq.Location = new System.Drawing.Point(91, 69);
            this.nLoFreq.Maximum = new decimal(new int[] {
            1950,
            0,
            0,
            0});
            this.nLoFreq.Name = "nLoFreq";
            this.nLoFreq.Size = new System.Drawing.Size(98, 20);
            this.nLoFreq.TabIndex = 94;
            this.nLoFreq.Value = new decimal(new int[] {
            900,
            0,
            0,
            0});
            this.nLoFreq.ValueChanged += new System.EventHandler(this.nLoFreq_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(21, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 93;
            this.label6.Text = "Lo Freq:";
            // 
            // cbAGC
            // 
            this.cbAGC.AutoSize = true;
            this.cbAGC.Checked = true;
            this.cbAGC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAGC.Location = new System.Drawing.Point(77, 48);
            this.cbAGC.Name = "cbAGC";
            this.cbAGC.Size = new System.Drawing.Size(48, 17);
            this.cbAGC.TabIndex = 92;
            this.cbAGC.Text = "AGC";
            this.cbAGC.UseVisualStyleBackColor = true;
            this.cbAGC.CheckedChanged += new System.EventHandler(this.cbAGC_CheckedChanged);
            // 
            // cbCMA
            // 
            this.cbCMA.AutoSize = true;
            this.cbCMA.Checked = true;
            this.cbCMA.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCMA.Location = new System.Drawing.Point(22, 48);
            this.cbCMA.Name = "cbCMA";
            this.cbCMA.Size = new System.Drawing.Size(49, 17);
            this.cbCMA.TabIndex = 91;
            this.cbCMA.Text = "CMA";
            this.cbCMA.UseVisualStyleBackColor = true;
            this.cbCMA.CheckedChanged += new System.EventHandler(this.cbCMA_CheckedChanged);
            // 
            // comboDemodulators
            // 
            this.comboDemodulators.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDemodulators.FormattingEnabled = true;
            this.comboDemodulators.Items.AddRange(new object[] {
            "Default"});
            this.comboDemodulators.Location = new System.Drawing.Point(21, 21);
            this.comboDemodulators.Name = "comboDemodulators";
            this.comboDemodulators.Size = new System.Drawing.Size(169, 21);
            this.comboDemodulators.TabIndex = 0;
            this.comboDemodulators.SelectedIndexChanged += new System.EventHandler(this.comboDemodulators_SelectedIndexChanged);
            // 
            // gbRepeater
            // 
            this.gbRepeater.Controls.Add(this.label3);
            this.gbRepeater.Controls.Add(this.label2);
            this.gbRepeater.Controls.Add(this.comboNetworkInterfaces);
            this.gbRepeater.Controls.Add(this.txtIPAddress);
            this.gbRepeater.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbRepeater.Location = new System.Drawing.Point(2, 172);
            this.gbRepeater.Name = "gbRepeater";
            this.gbRepeater.Size = new System.Drawing.Size(195, 105);
            this.gbRepeater.TabIndex = 1;
            this.gbRepeater.TabStop = false;
            this.gbRepeater.Text = "Repeater";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 79;
            this.label3.Text = "IP Address:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 78;
            this.label2.Text = "Network Interfaces:";
            // 
            // comboNetworkInterfaces
            // 
            this.comboNetworkInterfaces.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboNetworkInterfaces.FormattingEnabled = true;
            this.comboNetworkInterfaces.Location = new System.Drawing.Point(21, 34);
            this.comboNetworkInterfaces.Name = "comboNetworkInterfaces";
            this.comboNetworkInterfaces.Size = new System.Drawing.Size(169, 21);
            this.comboNetworkInterfaces.TabIndex = 0;
            // 
            // txtIPAddress
            // 
            this.txtIPAddress.Location = new System.Drawing.Point(21, 77);
            this.txtIPAddress.Name = "txtIPAddress";
            this.txtIPAddress.Size = new System.Drawing.Size(168, 20);
            this.txtIPAddress.TabIndex = 1;
            this.txtIPAddress.Text = "255.255.255.255";
            // 
            // gbSignalInput
            // 
            this.gbSignalInput.Controls.Add(this.txtTcpIpAddress);
            this.gbSignalInput.Controls.Add(this.label10);
            this.gbSignalInput.Controls.Add(this.txtTcpIpPort);
            this.gbSignalInput.Controls.Add(this.rTcpIp);
            this.gbSignalInput.Controls.Add(this.rFile);
            this.gbSignalInput.Controls.Add(this.rAudioDevice);
            this.gbSignalInput.Controls.Add(this.label1);
            this.gbSignalInput.Controls.Add(this.comboPlaybackDevices);
            this.gbSignalInput.Controls.Add(this.btnSelectWav);
            this.gbSignalInput.Controls.Add(this.txtWaveFileInput);
            this.gbSignalInput.Controls.Add(this.lblFile);
            this.gbSignalInput.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbSignalInput.Location = new System.Drawing.Point(2, 2);
            this.gbSignalInput.Name = "gbSignalInput";
            this.gbSignalInput.Size = new System.Drawing.Size(195, 170);
            this.gbSignalInput.TabIndex = 0;
            this.gbSignalInput.TabStop = false;
            this.gbSignalInput.Text = "Source";
            // 
            // txtTcpIpAddress
            // 
            this.txtTcpIpAddress.Location = new System.Drawing.Point(20, 144);
            this.txtTcpIpAddress.Name = "txtTcpIpAddress";
            this.txtTcpIpAddress.Size = new System.Drawing.Size(113, 20);
            this.txtTcpIpAddress.TabIndex = 96;
            this.txtTcpIpAddress.Text = "127.0.0.1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 128);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 95;
            this.label10.Text = "TCP/IP Server:";
            // 
            // txtTcpIpPort
            // 
            this.txtTcpIpPort.Location = new System.Drawing.Point(140, 144);
            this.txtTcpIpPort.Name = "txtTcpIpPort";
            this.txtTcpIpPort.Size = new System.Drawing.Size(50, 20);
            this.txtTcpIpPort.TabIndex = 93;
            this.txtTcpIpPort.Text = "20022";
            // 
            // rTcpIp
            // 
            this.rTcpIp.AutoSize = true;
            this.rTcpIp.Location = new System.Drawing.Point(129, 19);
            this.rTcpIp.Name = "rTcpIp";
            this.rTcpIp.Size = new System.Drawing.Size(61, 17);
            this.rTcpIp.TabIndex = 78;
            this.rTcpIp.Text = "TCP/IP";
            this.rTcpIp.UseVisualStyleBackColor = true;
            this.rTcpIp.CheckedChanged += new System.EventHandler(this.rSource_CheckedChanged);
            // 
            // rFile
            // 
            this.rFile.AutoSize = true;
            this.rFile.Location = new System.Drawing.Point(81, 19);
            this.rFile.Name = "rFile";
            this.rFile.Size = new System.Drawing.Size(41, 17);
            this.rFile.TabIndex = 77;
            this.rFile.Text = "File";
            this.rFile.UseVisualStyleBackColor = true;
            this.rFile.CheckedChanged += new System.EventHandler(this.rSource_CheckedChanged);
            // 
            // rAudioDevice
            // 
            this.rAudioDevice.AutoSize = true;
            this.rAudioDevice.Checked = true;
            this.rAudioDevice.Location = new System.Drawing.Point(21, 19);
            this.rAudioDevice.Name = "rAudioDevice";
            this.rAudioDevice.Size = new System.Drawing.Size(52, 17);
            this.rAudioDevice.TabIndex = 76;
            this.rAudioDevice.TabStop = true;
            this.rAudioDevice.Text = "Audio";
            this.rAudioDevice.UseVisualStyleBackColor = true;
            this.rAudioDevice.CheckedChanged += new System.EventHandler(this.rSource_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 75;
            this.label1.Text = "Playback Devices:";
            // 
            // comboPlaybackDevices
            // 
            this.comboPlaybackDevices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPlaybackDevices.FormattingEnabled = true;
            this.comboPlaybackDevices.Location = new System.Drawing.Point(20, 60);
            this.comboPlaybackDevices.Name = "comboPlaybackDevices";
            this.comboPlaybackDevices.Size = new System.Drawing.Size(169, 21);
            this.comboPlaybackDevices.TabIndex = 0;
            // 
            // btnSelectWav
            // 
            this.btnSelectWav.Location = new System.Drawing.Point(163, 101);
            this.btnSelectWav.Name = "btnSelectWav";
            this.btnSelectWav.Size = new System.Drawing.Size(27, 22);
            this.btnSelectWav.TabIndex = 2;
            this.btnSelectWav.Text = "...";
            this.btnSelectWav.UseVisualStyleBackColor = true;
            this.btnSelectWav.Click += new System.EventHandler(this.btnSelectWav_Click);
            // 
            // txtWaveFileInput
            // 
            this.txtWaveFileInput.Location = new System.Drawing.Point(20, 102);
            this.txtWaveFileInput.Name = "txtWaveFileInput";
            this.txtWaveFileInput.Size = new System.Drawing.Size(136, 20);
            this.txtWaveFileInput.TabIndex = 1;
            // 
            // lblFile
            // 
            this.lblFile.AutoSize = true;
            this.lblFile.Location = new System.Drawing.Point(17, 87);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(85, 13);
            this.lblFile.TabIndex = 69;
            this.lblFile.Text = "Wave File Input:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblRxSYM,
            this.toolStripStatusLabel6,
            this.lblBBER,
            this.toolStripStatusLabel3,
            this.lblRxFR,
            this.toolStripStatusLabel7,
            this.toolStripStatusLabel10,
            this.lblLostFR,
            this.toolStripStatusLabel5,
            this.lblTxUDP,
            this.toolStripStatusLabel1,
            this.lblSyncERR,
            this.toolStripStatusLabel4,
            this.lblStreamId});
            this.statusStrip1.Location = new System.Drawing.Point(0, 663);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(950, 24);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "lblUDP";
            // 
            // lblRxSYM
            // 
            this.lblRxSYM.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblRxSYM.Name = "lblRxSYM";
            this.lblRxSYM.Size = new System.Drawing.Size(62, 19);
            this.lblRxSYM.Text = "Rx SYM: 0";
            // 
            // toolStripStatusLabel6
            // 
            this.toolStripStatusLabel6.Name = "toolStripStatusLabel6";
            this.toolStripStatusLabel6.Size = new System.Drawing.Size(10, 19);
            this.toolStripStatusLabel6.Text = " ";
            // 
            // lblBBER
            // 
            this.lblBBER.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblBBER.Name = "lblBBER";
            this.lblBBER.Size = new System.Drawing.Size(63, 19);
            this.lblBBER.Text = "BBER: ...%";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(10, 19);
            this.toolStripStatusLabel3.Text = " ";
            // 
            // lblRxFR
            // 
            this.lblRxFR.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblRxFR.Name = "lblRxFR";
            this.lblRxFR.Size = new System.Drawing.Size(51, 19);
            this.lblRxFR.Text = "Rx FR: 0";
            // 
            // toolStripStatusLabel7
            // 
            this.toolStripStatusLabel7.Name = "toolStripStatusLabel7";
            this.toolStripStatusLabel7.Size = new System.Drawing.Size(0, 19);
            // 
            // toolStripStatusLabel10
            // 
            this.toolStripStatusLabel10.Name = "toolStripStatusLabel10";
            this.toolStripStatusLabel10.Size = new System.Drawing.Size(10, 19);
            this.toolStripStatusLabel10.Text = " ";
            // 
            // lblLostFR
            // 
            this.lblLostFR.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblLostFR.Name = "lblLostFR";
            this.lblLostFR.Size = new System.Drawing.Size(61, 19);
            this.lblLostFR.Text = "Lost FR: 0";
            // 
            // toolStripStatusLabel5
            // 
            this.toolStripStatusLabel5.Name = "toolStripStatusLabel5";
            this.toolStripStatusLabel5.Size = new System.Drawing.Size(10, 19);
            this.toolStripStatusLabel5.Text = " ";
            // 
            // lblTxUDP
            // 
            this.lblTxUDP.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblTxUDP.Name = "lblTxUDP";
            this.lblTxUDP.Size = new System.Drawing.Size(60, 19);
            this.lblTxUDP.Text = "Tx UDP: 0";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(10, 19);
            this.toolStripStatusLabel1.Text = " ";
            // 
            // lblSyncERR
            // 
            this.lblSyncERR.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblSyncERR.Name = "lblSyncERR";
            this.lblSyncERR.Size = new System.Drawing.Size(71, 19);
            this.lblSyncERR.Text = "Sync ERR: 0";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(10, 19);
            this.toolStripStatusLabel4.Text = " ";
            // 
            // lblStreamId
            // 
            this.lblStreamId.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblStreamId.Name = "lblStreamId";
            this.lblStreamId.Size = new System.Drawing.Size(84, 19);
            this.lblStreamId.Text = "SId: XXXXXX...";
            // 
            // openWavFileInput
            // 
            this.openWavFileInput.Filter = "Wave audio files|*.wav";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel1.Controls.Add(this.btnCredits);
            this.panel1.Controls.Add(this.btnStart);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btnMenu);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(950, 77);
            this.panel1.TabIndex = 3;
            // 
            // btnCredits
            // 
            this.btnCredits.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCredits.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCredits.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCredits.FlatAppearance.BorderSize = 0;
            this.btnCredits.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCredits.Image = global::ScytaleC.Properties.Resources.if_keyhole_1930259_;
            this.btnCredits.Location = new System.Drawing.Point(877, 12);
            this.btnCredits.Name = "btnCredits";
            this.btnCredits.Size = new System.Drawing.Size(61, 50);
            this.btnCredits.TabIndex = 11;
            this.btnCredits.TabStop = false;
            this.btnCredits.Tag = "";
            this.btnCredits.UseVisualStyleBackColor = false;
            this.btnCredits.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnStart.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.btnStart.FlatAppearance.BorderSize = 0;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Image = global::ScytaleC.Properties.Resources.button_blue_play;
            this.btnStart.Location = new System.Drawing.Point(68, 12);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(61, 50);
            this.btnStart.TabIndex = 10;
            this.btnStart.TabStop = false;
            this.btnStart.Tag = "start";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.fftD);
            this.panel2.Location = new System.Drawing.Point(143, 2);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(1);
            this.panel2.Size = new System.Drawing.Size(574, 71);
            this.panel2.TabIndex = 9;
            // 
            // btnMenu
            // 
            this.btnMenu.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnMenu.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.btnMenu.FlatAppearance.BorderSize = 0;
            this.btnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu.Image = global::ScytaleC.Properties.Resources.exchange_back;
            this.btnMenu.Location = new System.Drawing.Point(9, 12);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(61, 50);
            this.btnMenu.TabIndex = 0;
            this.btnMenu.TabStop = false;
            this.btnMenu.Tag = "menu-close";
            this.btnMenu.UseVisualStyleBackColor = false;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // tabFrames
            // 
            this.tabFrames.Controls.Add(this.rtbFrames);
            this.tabFrames.Controls.Add(this.toolStrip1);
            this.tabFrames.Location = new System.Drawing.Point(4, 22);
            this.tabFrames.Name = "tabFrames";
            this.tabFrames.Padding = new System.Windows.Forms.Padding(3);
            this.tabFrames.Size = new System.Drawing.Size(726, 560);
            this.tabFrames.TabIndex = 1;
            this.tabFrames.Text = "Frames";
            this.tabFrames.UseVisualStyleBackColor = true;
            // 
            // rtbFrames
            // 
            this.rtbFrames.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbFrames.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbFrames.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbFrames.Location = new System.Drawing.Point(3, 28);
            this.rtbFrames.Name = "rtbFrames";
            this.rtbFrames.Size = new System.Drawing.Size(720, 529);
            this.rtbFrames.TabIndex = 0;
            this.rtbFrames.Text = "";
            this.rtbFrames.WordWrap = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPacketsClear,
            this.toolStripSeparator14,
            this.toolStripButton4,
            this.toolStripSeparator18,
            this.btnFramesAutoScroll});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(3, 3);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolStrip1.Size = new System.Drawing.Size(720, 25);
            this.toolStrip1.TabIndex = 78;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnPacketsClear
            // 
            this.btnPacketsClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnPacketsClear.Image = ((System.Drawing.Image)(resources.GetObject("btnPacketsClear.Image")));
            this.btnPacketsClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPacketsClear.Name = "btnPacketsClear";
            this.btnPacketsClear.Size = new System.Drawing.Size(30, 22);
            this.btnPacketsClear.Text = "Clear";
            this.btnPacketsClear.Click += new System.EventHandler(this.btnPacketsClear_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(29, 22);
            this.toolStripButton4.Text = "Copy";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(6, 25);
            // 
            // btnFramesAutoScroll
            // 
            this.btnFramesAutoScroll.Checked = true;
            this.btnFramesAutoScroll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnFramesAutoScroll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnFramesAutoScroll.Image = ((System.Drawing.Image)(resources.GetObject("btnFramesAutoScroll.Image")));
            this.btnFramesAutoScroll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFramesAutoScroll.Name = "btnFramesAutoScroll";
            this.btnFramesAutoScroll.Size = new System.Drawing.Size(51, 22);
            this.btnFramesAutoScroll.Text = "Auto Scroll";
            this.btnFramesAutoScroll.Click += new System.EventHandler(this.btnPacketsAutoScroll_Click);
            // 
            // ContentControl
            // 
            this.ContentControl.Controls.Add(this.tabFrames);
            this.ContentControl.Controls.Add(this.tabInfo);
            this.ContentControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContentControl.Location = new System.Drawing.Point(216, 77);
            this.ContentControl.Name = "ContentControl";
            this.ContentControl.SelectedIndex = 0;
            this.ContentControl.Size = new System.Drawing.Size(734, 586);
            this.ContentControl.TabIndex = 5;
            // 
            // tabInfo
            // 
            this.tabInfo.Controls.Add(this.rtbNotes);
            this.tabInfo.Controls.Add(this.panel3);
            this.tabInfo.Controls.Add(this.rtbInfo);
            this.tabInfo.Location = new System.Drawing.Point(4, 22);
            this.tabInfo.Name = "tabInfo";
            this.tabInfo.Size = new System.Drawing.Size(726, 560);
            this.tabInfo.TabIndex = 2;
            this.tabInfo.Text = "Info";
            this.tabInfo.UseVisualStyleBackColor = true;
            // 
            // rtbNotes
            // 
            this.rtbNotes.BackColor = System.Drawing.SystemColors.Info;
            this.rtbNotes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbNotes.Location = new System.Drawing.Point(0, 87);
            this.rtbNotes.Name = "rtbNotes";
            this.rtbNotes.ReadOnly = true;
            this.rtbNotes.Size = new System.Drawing.Size(726, 473);
            this.rtbNotes.TabIndex = 3;
            this.rtbNotes.Text = "Will be loaded at run-time from the notes.rtf file.";
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 84);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(726, 3);
            this.panel3.TabIndex = 4;
            // 
            // rtbInfo
            // 
            this.rtbInfo.BackColor = System.Drawing.SystemColors.Info;
            this.rtbInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.rtbInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbInfo.Location = new System.Drawing.Point(0, 0);
            this.rtbInfo.Name = "rtbInfo";
            this.rtbInfo.ReadOnly = true;
            this.rtbInfo.Size = new System.Drawing.Size(726, 84);
            this.rtbInfo.TabIndex = 2;
            this.rtbInfo.Text = "Scytale-C\n";
            // 
            // ConsumerDecoderOutput
            // 
            this.ConsumerDecoderOutput.Enabled = true;
            this.ConsumerDecoderOutput.Interval = 85;
            this.ConsumerDecoderOutput.Tick += new System.EventHandler(this.ConsumerDecoderOutput_Tick);
            // 
            // ConsumerSignalAttributes
            // 
            this.ConsumerSignalAttributes.Enabled = true;
            this.ConsumerSignalAttributes.Interval = 50;
            this.ConsumerSignalAttributes.Tick += new System.EventHandler(this.ConsumerSignalAttributes_Tick);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label5.Location = new System.Drawing.Point(769, 668);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(163, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Open source Inmarsat-C decoder";
            // 
            // lostPacketTimer
            // 
            this.lostPacketTimer.Enabled = true;
            this.lostPacketTimer.Interval = 8640;
            this.lostPacketTimer.Tick += new System.EventHandler(this.lostPacketTimer_Tick);
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lblVersion.ForeColor = System.Drawing.Color.DeepPink;
            this.lblVersion.Location = new System.Drawing.Point(663, 668);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(99, 13);
            this.lblVersion.TabIndex = 71;
            this.lblVersion.Text = "BETA";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // fftD
            // 
            this.fftD.BackColor = System.Drawing.Color.LightSkyBlue;
            this.fftD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fftD.Location = new System.Drawing.Point(1, 1);
            this.fftD.Name = "fftD";
            this.fftD.Padding = new System.Windows.Forms.Padding(1);
            this.fftD.Size = new System.Drawing.Size(572, 69);
            this.fftD.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(950, 687);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ContentControl);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Scytale-C";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panelMenu.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxDisplayedFrames)).EndInit();
            this.gbFrames.ResumeLayout(false);
            this.gbFrames.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.gbUWFinder.ResumeLayout(false);
            this.gbUWFinder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nTolerance)).EndInit();
            this.gbDemodulator.ResumeLayout(false);
            this.gbDemodulator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nHiFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nLoFreq)).EndInit();
            this.gbRepeater.ResumeLayout(false);
            this.gbRepeater.PerformLayout();
            this.gbSignalInput.ResumeLayout(false);
            this.gbSignalInput.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabFrames.ResumeLayout(false);
            this.tabFrames.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ContentControl.ResumeLayout(false);
            this.tabInfo.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.GroupBox gbSignalInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboPlaybackDevices;
        private System.Windows.Forms.Button btnSelectWav;
        private System.Windows.Forms.TextBox txtWaveFileInput;
        private System.Windows.Forms.Label lblFile;
        private System.Windows.Forms.GroupBox gbRepeater;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboNetworkInterfaces;
        private System.Windows.Forms.TextBox txtIPAddress;
        private System.Windows.Forms.GroupBox gbDemodulator;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ComboBox comboDemodulators;
        private System.Windows.Forms.OpenFileDialog openWavFileInput;
        private System.Windows.Forms.GroupBox gbUWFinder;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnMenu;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel5;
        private System.Windows.Forms.ToolStripStatusLabel lblRxSYM;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel6;
        private System.Windows.Forms.ToolStripStatusLabel lblLostFR;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel7;
        private System.Windows.Forms.ToolStripStatusLabel lblBBER;
        private System.Windows.Forms.ToolStripStatusLabel lblRxFR;
        private FftDisplay fftD;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel10;
        private System.Windows.Forms.ToolStripStatusLabel lblSyncERR;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblTxUDP;
        private System.Windows.Forms.RadioButton rFile;
        private System.Windows.Forms.RadioButton rAudioDevice;
        private System.Windows.Forms.Button btnCredits;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown nTolerance;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox gbFrames;
        private System.Windows.Forms.ComboBox comboViterbiDecoders;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox cbRawFrameUDP;
        private System.Windows.Forms.TextBox txtRawFrameUDPPort;
        private System.Windows.Forms.TabPage tabFrames;
        private System.Windows.Forms.RichTextBox rtbFrames;
        private System.Windows.Forms.TabControl ContentControl;
        private System.Windows.Forms.CheckBox cbCMA;
        private System.Windows.Forms.Timer ConsumerDecoderOutput;
        private System.Windows.Forms.Timer ConsumerSignalAttributes;
        private System.Windows.Forms.TabPage tabInfo;
        private System.Windows.Forms.RichTextBox rtbInfo;
        private System.Windows.Forms.CheckBox cbAGC;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbSuspendSleepMode;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nHiFreq;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown nLoFreq;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox rtbNotes;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Timer lostPacketTimer;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel lblStreamId;
        internal System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnPacketsClear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripButton btnFramesAutoScroll;
        private System.Windows.Forms.NumericUpDown nMaxDisplayedFrames;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTcpIpAddress;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtTcpIpPort;
        private System.Windows.Forms.RadioButton rTcpIp;
    }
}

