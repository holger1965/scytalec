/*
 * microp11 2017
 * 
 * https://stackoverflow.com/questions/461742/how-to-convert-an-ipv4-address-into-a-integer-in-c
 * 
 * Sends a UDP packet over the selected network card.
 * 
 */

using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using ScytaleC.Interfaces;

namespace ScytaleC
{
    public sealed class UdpRepeater : IDisposable
    {
        public UdpClient udpc;

        public UdpRepeater(NetworkInterface key)
        {
            int id = key.GetIPProperties().GetIPv4Properties().Index;
            if (NetworkInterface.LoopbackInterfaceIndex != id)
            {
                foreach (UnicastIPAddressInformation uip in key.GetIPProperties().UnicastAddresses)
                {
                    if (uip.Address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        byte[] addressBytes = uip.Address.GetAddressBytes();
                        //Array.Reverse(addressBytes);
                        //long address64bit = (addressBytes[0] << 24) | (addressBytes[1] << 16) | (addressBytes[2] << 8) | (addressBytes[3]);
                        IPEndPoint local = new IPEndPoint(uip.Address, 0);
                        udpc = new UdpClient(local);
                        udpc.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
                        udpc.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontRoute, 1);
                    }
                }
            }
        }

        public void Send(string ip, int port, string message)
        {
            byte[] data = Encoding.ASCII.GetBytes(message);
            IPEndPoint target = new IPEndPoint(IPAddress.Parse(ip), port);
            udpc.Send(data, data.Length, target);
        }

        /// <summary>
        /// Takes an array of bits, each bit is represented by a full byte (e.g. bit 0 = 0x00, bit 1 = 0x01) 
        /// It reverses the order of the array.
        /// Converts the array to a hex string.
        /// Saves each array in one line, then adds a Windows CRLF.
        /// 
        /// The resulted text represents the demodulator output, oldest bit at the beginning of the text file.
        /// </summary>
        public void Send(string ip, int port, DemodulatedSymbolsArgs e)
        {
            Array.Reverse(e.Symbols);
            string message = BitConverter.ToString(e.Symbols).Replace("-", string.Empty);
            Send(ip, port, message);
        }

        public void Dispose()
        {
            udpc?.Close();
        }
    }
}
