﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Provides the calculations for received and lost frames as well as BBER.
 * BBER is the percentage of received vs. lost in the most recent 100 frames.
 * 
 * Disregards elapsed time when using a file as source.
 * 
 */

using ScytaleC.Interfaces;
using System;
using System.Diagnostics;

namespace ScytaleC
{
    internal class Statistics
    {
        private DateTime markTime;
        private const int frameDuration = 24 * 3600 * 1000 / 10000;
        private const int BBEROverFrames = 100;
        private const int NoFrameNumber = -2;
        private int rxFRCount;
        private int lostFRCount;
        private double assumedLostDueToTimeElapsedSinceLastRx;
        
        private int[,] bber;

        private int passedZeroHour;
        private int prevFrameNumber;

        public int GetRxFRCount()
        {
            return rxFRCount;
        }

        public DateTime GetMarkTime()
        {
            return markTime;
        }

        public event EventHandler<StatsArgs> OnStatsUpdate;

        public Statistics()
        {
            //set the time in the future so the lost Packet counter will not work
            markTime = DateTime.UtcNow.AddHours(10);
            prevFrameNumber = NoFrameNumber;
            bber = new int[BBEROverFrames, 2];
        }

        public void Restart()
        {
            //set the time in the future so the lost Packet counter will not work
            markTime = DateTime.UtcNow.AddHours(10);
            prevFrameNumber = NoFrameNumber;

            rxFRCount = 0;
            lostFRCount = 0;

            for (int i = 0; i < BBEROverFrames; i++)
            {
                bber[i, 0] = 0;
                bber[i, 1] = 0;
            }

            updateUI();
        }

        private void updateUI()
        {
            StatsArgs e = new ScytaleC.StatsArgs();
            e.RxFRCount = rxFRCount;
            e.LostFRCount = (int)Math.Max(0, lostFRCount + assumedLostDueToTimeElapsedSinceLastRx);
            double percent = getBBER(e.RxFRCount, e.LostFRCount);
            percent = percent < 0 ? 0 : percent;
            e.BBERPercent = percent;

            //update UI
            OnStatsUpdate?.Invoke(this, e);
        }

        /// <summary>
        /// Calculate BBER over the last 100 frames received.
        /// </summary>
        /// <param name="rxFRCount"></param>
        /// <param name="lostFRCount"></param>
        /// <returns></returns>
        private double getBBER(int rxFRCount, int lostFRCount)
        {
            double percentage = 0.0;
            
            //rotate left
            for (int i = 0; i < BBEROverFrames - 1; i++)
            {
                bber[i, 0] = bber[i + 1, 0];
                bber[i, 1] = bber[i + 1, 1];
            }

            //insert on last position
            bber[BBEROverFrames - 1, 0] = rxFRCount;
            bber[BBEROverFrames - 1, 1] = lostFRCount;

            //calculate diff last to first
            int rx = bber[BBEROverFrames - 1, 0] - bber[0, 0];
            int lost = Math.Max(0, bber[BBEROverFrames - 1, 1] - bber[0, 1]);
            int divisor = rx + lost;

            if (divisor != 0)
            {
                percentage = lost * 100 / divisor;
            }

            return percentage;
        }

        /// <summary>
        /// This gets called on every packet received, or recursively when the counter resets due to
        /// passing over the zero hour.
        /// </summary>
        public void AddRxFR(int bbFrameNumber, bool isBadBB)
        {
            markTime = DateTime.UtcNow;
            assumedLostDueToTimeElapsedSinceLastRx = 0;

            bool afterRecursionDontUpdateResults = false;
            int frameNumber = bbFrameNumber + 10000 * passedZeroHour;
            int diff = 1;

            if (isBadBB)
            {
                // this frame has a bad bb, but we might recover its payload packets later on
                // we consider this packet as received
                prevFrameNumber++;
                frameNumber = prevFrameNumber;
            }
            else
            {
                //first received frame
                if (prevFrameNumber < 0)
                {
                    diff = 1;
                }
                else
                {
                    diff = frameNumber - prevFrameNumber;
                }

                if (diff < 0)
                {
                    //we have passed the zero hour and the packets have reset their number
                    passedZeroHour++;
                    //we trigger the recursion and we stop the stacked code to update results
                    afterRecursionDontUpdateResults = true;
                    AddRxFR(bbFrameNumber, isBadBB);
                }
            }

            if (afterRecursionDontUpdateResults)
            {
                return;
            }

            lostFRCount += diff - 1;
            prevFrameNumber = frameNumber;
            rxFRCount++;

            updateUI();
        }

        internal void Stop()
        {
            //set the time in the future so the lost Packet counter will not work
            markTime = DateTime.UtcNow.AddHours(10);
        }

        internal void EvaluateLostPackets(DateTime time, bool sourceIsFile)
        {
            if (sourceIsFile)
            {
                return;
            }

            // the clock arrives at one frame intervals
            // if we did not receive anything, we leavea fudge
            TimeSpan ts = time.Subtract(markTime);
            if (ts.TotalMilliseconds / frameDuration > 1.5)
            {
                assumedLostDueToTimeElapsedSinceLastRx = ts.TotalMilliseconds / frameDuration;
                updateUI();
            }
        }
    }

    public class StatsArgs : EventArgs
    {
        public int RxFRCount { get; set; }
        public int LostFRCount { get; set; }
        public double BBERPercent { get; set; }
    }
}
