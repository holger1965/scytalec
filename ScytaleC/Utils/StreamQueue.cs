﻿/*
 * microp11 2017
 * 
 *  
 * This class (naively written) provides a consistent output, to be used as input for a demodulator, fft,...
 * It is a smart queue. Everything equeued is guaranteed to be 16bit, LE, double, mono, PCM.
 * The enqueuer must be provided with the caracteristics of the incoming samples.
 * Will not store beyound a certain capacity.
 * 
 * The output data will be always 8192 samples, 16-bit float, 1 channel
 * 
 * NAudio provides out of the box a solution for this.
 * Perhaps that solution should be leveraged here and this abomination removed.
 * 
 */

using NAudio.Wave;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace ScytaleC
{
    static class StreamQueueConsts
    {
        public const int MaxSampleCount = 5000; 
        public const int OutputSampleCount = 4096;
    }

    class StreamQueue: BlockingCollection<float[]>
    {
        private WaveFormat iwf = null;
        private int maxSampleCount = StreamQueueConsts.MaxSampleCount;
        private float[] output = new float[StreamQueueConsts.OutputSampleCount];
        private int outputPos = 0;
        private int diff = StreamQueueConsts.OutputSampleCount;

        public void SetInputDataType(WaveFormat wf)
        {
            iwf = wf;
        }

        public void Store(byte[] buffer, int length, int counter, bool IsWave)
        {
            System.Threading.SpinWait.SpinUntil(() => base.Count < maxSampleCount);

            if (iwf == null)
            {
                throw new Exception("Must specify InputStreamType.");
            }

            //format incoming

            //The number of bytes for one sample including all channels.
            int truelen = 0;
            float[] source = null;
            switch (iwf.BlockAlign)
            {
                case 2:
                    if (iwf.Channels == 1)
                    {
                        truelen = length / 2;
                        source = new float[truelen];
                        for (int i = 0; i < length; i += 2)
                        {
                            source[i / 2] = ((sbyte)buffer[i + 1] << 8) + (sbyte)buffer[i];
                        }
                    }
                    else
                    {
                        throw new Exception("Unsupported input wave format.");
                    }

                    break;

                case 4:
                    if (iwf.Channels == 1)
                    {
                        throw new Exception("Unsupported input wave format.");
                    }
                    else 
                    {
                        truelen = length / 4;
                        source = new float[truelen];
                        for (int i = 0; i < length; i += 4)
                        {
                            //use only left channel
                            source[i / 4] = ((sbyte)buffer[i + 1] << 8) + (sbyte)buffer[i];
                        }
                    }

                    break;
                default:
                    throw new Exception("Unsupported input wave format.");
            }

            Store(source, 0, source.Length);
        }

        private void Store(float[] source, int sourcePos, int length)
        {
            if (length == diff)
            {
                Array.Copy(source, sourcePos, output, outputPos, diff);
                base.Add(output);

                output = new float[StreamQueueConsts.OutputSampleCount];
                sourcePos = 0;
                outputPos = 0;
                diff = StreamQueueConsts.OutputSampleCount;
            }
            else if (length > diff)
            {
                Array.Copy(source, sourcePos, output, outputPos, diff);
                base.Add(output);

                output = new float[StreamQueueConsts.OutputSampleCount];
                sourcePos += diff;
                outputPos = 0;
                int remaining = length - diff;
                diff = StreamQueueConsts.OutputSampleCount;

                Store(source, sourcePos, remaining);
            }
            else //source.Length < diff
            {
                Array.Copy(source, sourcePos, output, outputPos, length);

                outputPos += length;
                sourcePos = 0;
                diff = output.Length - outputPos;
            }
        }
    }

    class InputStreamType
    {
        public int SampleRate;
        public int BitDepth;
        public int Channels;
        public bool LittleEndian;

        public InputStreamType(int samplerate, int bitdepth, int channels, bool littleendian)
        {
            SampleRate = samplerate;
            BitDepth = bitdepth;
            Channels = channels;
            LittleEndian = littleendian;
        }
    }
}
