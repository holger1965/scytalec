# README #

To get your decoder up and running, download the zip file, extract it in a folder of your choosing and run the executable
ScytaleC.exe. To start viewing messages, run the acompanying UI executable, ScytaleC.QuickUI.exe. The UI communicates with
the decoder through UDP. If your PC does not have a network card, please install and configure a virtual network interface.

### What is this repository for? ###

* This is a PC-based open source decoder for [Inmarsat-C](https://www.inmarsat.com/services/safety/inmarsat-c/) written in C#.

### Note ###

This work is a learning experience. Resources and CPU consumption were second to code understandability. Feel free to use
this code as a base for your own development. If you have a question, if you find a bug, please contact me or preferably 
use the issue tracker associated with this repository. The bibliography is extensive and I am sure there is a lot more to
be added. If you find something interesting, please let me know.

### Issue tracker ###

https://bitbucket.org/scytalec/scytalec/issues/

### Who do I talk to? ###

* Repo owner or admin: microp11 at aelogic dot com
* Other community or team contact.

### I would like to help ###

Excellent initiative! Here are a few areas that could always use an extra hand:

* Wiki - Perhaps add a tutorial of how to use the application, and then update it when necessary.
* Packet decoding - There are always new packets to be decoded. There are decoded packets that have unknown parts. The database support implemented since 1.2 allows for easy collection and correlation of data. The ScytaleC.Tester utility that has been provided with the zip file from day one can be used to determine which portions of a packet contain text. Using this utility I was able for example to decode the BD and BE packets.
* Ideas - Bring another point of view, root for a new feature, or even implement one yourself.
* Coding - Write your code and ask for a pull request.

### Donations ###

Scytale-C is an exercise and comes with an intrinsic reward. There is no expectation of monetary gain. We understand however that a donation is also a way to contribute, and if you wish to buy us a beer, you're more than welcome! Thank you.  
See [PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=RFCSY3EWYXUPU).

### Licensing ###

GNU General Public License 3, microp11 2017
 
Scytale-C is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Scytale-C is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

See http://www.gnu.org/licenses/.
