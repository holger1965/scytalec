﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *   Bibliography:
 *   
 *   Digital Communications Playlists
 *   Bernd Porr, UofG
 *   https://www.youtube.com/channel/UCeFwNKTf19bJL1K5yYTpdqg/playlists
 *
 *   bpsk encoder/decoder using java audio 
 *   f4grx/jbpsk 
 *   https://github.com/f4grx/jbpsk
 *
 *   Design and Implementation of High Performance BPSK Demodulator for Satellite Communications
 *   by Tofigh, E.
 *   MS Thesis, TUDelft
 *   https://repository.tudelft.nl/islandora/object/uuid%3Ac102b61b-5bad-4550-9dbb-33ca2e191bd9
 *
 *   Practical Costas loop design - Designing a simple and inexpensive BPSK Costas loop carrier recovery circuit
 *   Feigin, J. 
 *   in RF DESIGN; 25; 20-37; RF DESIGN
 *   by CARDIFF PUBLISHING COMPANY INC; 2002
 * 
 *   JDSKA, DSCA demodulator and decoder written in C++ Qt
 *   jontio/JDSCA 
 *   http://jontio.zapto.org/hda1/jdsca.html 
 *   https://github.com/jontio/JDSCA
 *   
 *   Telecommunication Breakdown: Concepts of Communication Transmitted via Software-Defined Radio
 *   by C. R. Johnson, Jr. and W. A. Sethares
 *   http://sethares.engr.wisc.edu/telebreak.html
 *   
 *   Software Defined Radio Using MATLAB & Simulink and the RTL-SDR
 *   by K. Barlee, University of Strathclyde, D. Atkinson, University of Strathclyde, R.W. Stewart, University of Strathclyde
 *   L. Crockett, University of Strathclyde
 *   ISBN: 978-0-9929787-1-6
 *   http://www.desktopsdr.com/
 *   
 *   Delfi-C3 Telemetry Reception, Rascal & Warbler
 *   http://www.delfispace.nl/operations/delfi-c3-telemetry-reception
 *      
 *   Communication for Beginners in Matlab: BPSK Modulation Demodulation With AWGN Channel
 *   rupam rupam
 *   https://www.youtube.com/watch?v=B4TFTT1rnYc
 *   
 *   Gaussianwaves.com - Signal Processing Simplified
 *   http://www.gaussianwaves.com/2013/11/symbol-timing-recovery-for-qpsk-digital-modulations/
 *  
 *   WinPSK
 *   http://www.moetronix.com/ae4jy/winpsk.htm
 *  
 *   David Dorran Playlists and Matlab
 *   https://www.youtube.com/user/ddorran/playlists
 *   https://dadorran.wordpress.com/
 *  
 *   MATLAB code for the MDemodulator used by Scytale-C (source code and Wiki)
 *   https://bitbucket.org/scytalec/scytalec-matlab
 *   https://bitbucket.org/scytalec/scytalec-matlab/wiki/Home
 *   
 */

using ScytaleC.Interfaces;
using System;
using System.Numerics;

namespace ScytaleC
{
    public sealed class MDemodulator : IDemodulator
    {
        private double frequency = DataConsts.InmarsatCCenterFrequency;
        double alpha = DemodulatorConsts.Alpha;
        double beta;
        private double omega;
        private double phase = 0.0;
        private long sampleCount;
        private int noSyncCounter = 0;
        private double error;
        private bool isInSync = false;
        private int flagcounter;
        private Complex scatterPoint;

        // LPF1, LPF2
        // The literature suggestes that the square-shaped LPF filter should have a
        // bandwidth of no less than half the symbol rate. This will remove the most
        // noise possible without reducing the amplitude of the desired signal at
        // its sampling instants.
        //
        // I tried both, with the same results, chose FIR as easier to implement.
        // [b, a] = butter(1, 0.5, 'low');
        // or
        // [b] = fir1(1, 0.9, 'low');
        private double[] b = { 0.5, 0.5 };
        private FIR lpf1;
        private FIR lpf2;

        // The literature indicates that the loop filter should have a
        // response far outside the LPF1/LPF2.The fastest achievable settle
        // time is one in which the VCO has a gain 8x that of the LPF1/LPF2 pole
        // frequency.For LPF3, a factor of six times K or 12 times the LPF1/LPF2
        // pole is a better choice
        // Currently my loop filter is the same as the lpfs
        private FIR lpf3;

        private RRC rrcRe;
        private RRC rrcIm;
        private RRC rrcRe2;
        private RRC rrcIm2;

#if SaveToFile
        private WriteDoubleToFile wdtf1;
        private WriteDoubleToFile wdtf2;
        private WriteDoubleToFile gRe;
        private WriteDoubleToFile gIm;
        private WriteDoubleToFile cRe;
        private WriteDoubleToFile cIm;
#endif
        private Gardner gardner;
        private CMA cma;
        private bool cmaEnabled = false;
        private Complex cmaOutputSample;

        private byte[] octetbuffer;
        private AGC agc;
        private bool agcEnabled = false;

        private int loFreq;
        private int hiFreq;

        public MDemodulator()
        {
            flagcounter = 0;
            octetbuffer = new byte[DemodulatorConsts.BitsPerChunk];

            // see literature
            beta = alpha * alpha / 4;

            // carrier
            omega = 2 * Math.PI * frequency / DemodulatorConsts.SampleRate;

            // I and Q LPFs
            lpf1 = new FIR(b);
            lpf2 = new FIR(b);

            // loop LPF
            lpf3 = new FIR(b);

            rrcRe = new RRC(1, 17, DemodulatorConsts.SampleRate, DemodulatorConsts.SymbolRate);
            rrcIm = new RRC(1, 17, DemodulatorConsts.SampleRate, DemodulatorConsts.SymbolRate);
            rrcRe2 = new RRC(1, 11, DemodulatorConsts.SampleRate, DemodulatorConsts.SymbolRate);
            rrcIm2 = new RRC(1, 11, DemodulatorConsts.SampleRate, DemodulatorConsts.SymbolRate);

            //rrcRe = new RRC(1, 55, DemodulatorConsts.SampleRate, DemodulatorConsts.SymbolRate);
            //rrcIm = new RRC(1, 55, DemodulatorConsts.SampleRate, DemodulatorConsts.SymbolRate);

#if SaveToFile            
            wdtf1 = new WriteDoubleToFile(@"d:\zz\s\I.bin");
            wdtf2 = new WriteDoubleToFile(@"d:\zz\s\Irrc.bin");
            gRe = new WriteDoubleToFile(@"d:\zz\s\gRe.bin");
            gIm = new WriteDoubleToFile(@"d:\zz\s\gIm.bin");
            cRe = new WriteDoubleToFile(@"d:\zz\s\cRe.bin");
            cIm = new WriteDoubleToFile(@"d:\zz\s\cIm.bin");
#endif
            gardner = new Gardner(DemodulatorConsts.SampleRate, DemodulatorConsts.SymbolRate);

            cma = new CMA();
            cmaEnabled = true;

            agc = new AGC();
            agcEnabled = true;

            loFreq = DataConsts.loFrequency;
            hiFreq = DataConsts.hiFrequency;
        }

        public string Name
        {
            get
            {
                return "BPSK 1.0 (hard symbols)";
            }
        }

        bool IDemodulator.CmaEnabled
        {
            get
            {
                return cmaEnabled;
            }
        
            set
            {
                cmaEnabled = value;
            }
        }

        public bool AgcEnabled
        {
            get
            {
                return agcEnabled;
            }

            set
            {
                agcEnabled = value;
            }
        }

        public int LoFreq
        {
            get
            {
                return loFreq;
            }

            set
            {
                loFreq = value;
            }
        }

        public int HiFreq
        {
            get
            {
                return hiFreq;
            }

            set
            {
                hiFreq = value;
            }
        }

        public event EventHandler<DemodulatedSymbolsArgs> OnDemodulatedSymbols;
        public event EventHandler<SignalAttributesArgs> OnSignalAttributes;

        public void Process(AudioSampleArgs args)
        {
            double vI;
            double vQ;
            double I;
            double Q;
            double magnitude = 0.0;
            double meanMagnitude;
            float[] samples = args.Samples;
            int length = args.Length;
            double maxMagnitudeOfsamples = 0.0;
            double Irrc;
            double Qrrc;
            double Irrc2;
            double Qrrc2;

            // sample normalization
            for (int i = 0; i < length; i++)
            {
                magnitude += Math.Abs(samples[i]);
            }
            meanMagnitude = magnitude / length;

            for (int i = 0; i < length; i++)
            {
                samples[i] = samples[i] / (float)meanMagnitude;
                maxMagnitudeOfsamples = Math.Max(maxMagnitudeOfsamples, Math.Abs(samples[i]));
            }

            // There are about 100 sets of samples for each packet.
            // There are 4096 samples per set.
            // We decimate the calls to this event for everyone's sake.
            // A factor of 2 will give us 50 calls per packet, that is 50 calls per 8.4s,
            // which is 5.9 calls per second. Should be more than enough for the human eye.
            if (sampleCount % DemodulatorConsts.SignalAttributesDecimationFactor == 0)
            {
                SignalAttributesArgs sa = new SignalAttributesArgs
                {
                    Frequency = frequency,
                    MeanMagnitude = (int)meanMagnitude,
                    IsInSync = isInSync,
                    SyncCounter = noSyncCounter,
                    ScatterPoint = scatterPoint,
                    IsCMAEnabled = cmaEnabled,
                    IsAGCEnabled = agcEnabled
                };
                OnSignalAttributes?.Invoke(this, sa);
            }

            //process current args set of samples
            double err_summed = 0;
            for (int i = 0; i < length; i++)
            {
                sampleCount++;

                // Costas classic carrier recovery, ignore/eliminate the phase info)
                // Use both sin and cos from the VCO and create the voltage by combining them
                frequency = (omega * DemodulatorConsts.SampleRate) / (2 * Math.PI);

                // phase
                phase = phase + omega + alpha * error;

                // carrier
                omega = omega + beta * error;

                // keep frequency in range
                if (frequency < loFreq)
                {
                    frequency = loFreq;
                    omega = 2 * Math.PI * frequency / DemodulatorConsts.SampleRate;
                }

                if (frequency > hiFreq)
                {
                    frequency = hiFreq;
                    omega = 2 * Math.PI * frequency / DemodulatorConsts.SampleRate;
                }

                // keep phase in range
                if (phase > 2 * Math.PI)
                {
                    phase = phase - 2 * Math.PI;
                }

                // VCO
                vI = Math.Cos(phase);
                vQ = -Math.Sin(phase);

                // mixer
                I = vI * samples[i];
                Q = vQ * samples[i];

                // LPFs 
                I = lpf1.Filter(I);
                Q = lpf2.Filter(Q);

                // AGC, apparently throws the signal from side to side, not good!
                if (agcEnabled)
                {
                    agc.Apply(ref I, ref Q);
                }
                //wdtf1.WriteDouble(I);

                // VCO error
                error = I * Q;

                // loop filter
                error = lpf3.Filter(error);

                // Summing up all errors in this args sample set
                // As the carrier gets locked, the positive and the negative error are equally distributed and
                // their sum gets closer and closer to zero.
                err_summed += error;

                // RRC
                // It is more computer efficient to break up a FIR rather than having a fairly long one (Moe Wheatley, WinPSK)
                // We break the RRC into an 19 + 11 taps two part FIR.
                // We are trying to get to make every single pulse a sharp triangle to provide the input for the Gardner
                Irrc = rrcRe.Filter(I);
                Qrrc = rrcIm.Filter(Q);
                Irrc2 = rrcRe2.Filter(Irrc);
                //wdtf2.WriteDouble(Irrc2);
                Qrrc2 = rrcIm2.Filter(Qrrc);

                // Gardner
                Complex gardnerOutputSample;
                bool isOnPoint;
                if (!gardner.Step(Irrc2, Qrrc2, out gardnerOutputSample, out isOnPoint))
                {
                    continue;
                }

                // CMA
                if (cmaEnabled)
                {
                    if (!cma.Step(gardnerOutputSample, isOnPoint, out cmaOutputSample))
                    {
                        continue;
                    }
                }

                if (!isOnPoint)
                {
                    continue;
                }

                if (cmaEnabled)
                {
                    scatterPoint = cmaOutputSample;
                }
                else
                {
                    scatterPoint = gardnerOutputSample;
                }

#if SaveToFile
                gRe.WriteDouble(gardnerOutputSample.Real);
                gIm.WriteDouble(gardnerOutputSample.Imaginary);
                cRe.WriteDouble(cmaOutputSample.Real);
                cIm.WriteDouble(cmaOutputSample.Imaginary);
#endif
                //the output is the demodulated soft symbol
                byte hardBit = 0;
                if (scatterPoint.Real > 0)
                {
                    hardBit = 1;
                }

                Array.Copy(octetbuffer, 0, octetbuffer, 1, octetbuffer.Length - 1);
                octetbuffer[0] = hardBit;
                flagcounter++;

                if (flagcounter == DemodulatorConsts.BitsPerChunk)
                {
                    byte[] bits = new byte[DemodulatorConsts.BitsPerChunk];
                    Array.Copy(octetbuffer, bits, DemodulatorConsts.BitsPerChunk);

                    DemodulatedSymbolsArgs ca = new DemodulatedSymbolsArgs();
                    ca.Samples = sampleCount;
                    ca.Length = DemodulatorConsts.BitsPerChunk;
                    ca.Symbols = bits;
                    ca.IsHardDecision = true;
                    OnDemodulatedSymbols?.Invoke(this, ca);

                    flagcounter = 0;
                    Array.Clear(octetbuffer, 0, DemodulatorConsts.BitsPerChunk);
                }
            }
            isInSync = (Math.Abs(err_summed) < DemodulatorConsts.MaxAbsErrSum);
            //Debug.WriteLine("Error: {0}, MeanMagnitude: {1}, MaxSampleMagnitude {2}", err_summed, meanMagnitude, maxMagnitudeOfsamples);

            noSyncCounter += (isInSync) ? 0 : 1;
        }

        public void SetCenterFrequency(double centerFrequency)
        {
            omega = (2 * Math.PI * centerFrequency) / DemodulatorConsts.SampleRate;
        }

        public bool IsCMASupported()
        {
            return true;
        }

        public bool IsAGCSupported()
        {
            return true;
        }
    }

    internal class DemodulatorConsts
    {
        internal const int SymbolRate = 1200;
        internal const double SampleRate = 48000;
        internal const int BitsPerChunk = 5000;

        // As the carrier gets locked, the positive and the negative error are equally distributed and
        // their sum gets closer and closer to zero. This is the absolute number I came up with. 
        internal const double MaxAbsErrSum = 26;

        internal const double Alpha = 0.0065;

        // Signal attributes decimation factor
        internal const int SignalAttributesDecimationFactor = 2;
    }
}
