﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

using LiteDB;
using ScytaleC.Interfaces;
using ScytaleC.PacketDecoders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ScytaleC.QuickUI
{
    public static class PayloadType
    {
        public const int CNT_ZAP = 0;
        public const int CNT_IA5 = 1;
        public const int CNT_ITA2 = 2;
        public const int CNT_BINARY = 3;
    }
    public class Db
    {
        private const bool NOTEGC = false;
        private const bool IAMEGC = true;

        private MemoryStream mem;
        private string fullPathToDb;
        private LiteDatabase db;
        private BsonMapper mapper;

        //for all packets received
        private LiteCollection<PacketDecoder> colAll;

        //for unis
        private LiteCollection<Uni> colUni;
        private LiteCollection<UniDetail> colUniDetail;

        public event EventHandler<UniUpdatedArgs> OnUniUpdated;
        public event EventHandler<UniDeletedArgs> OnUniDeleted;

        public Db(string location)
        {
            fullPathToDb = string.Format(@"{0}\data.db", location);
            mem = readStream(fullPathToDb);
            db = new LiteDatabase(mem);

            mapper = BsonMapper.Global;

            //very important to set this to false, otherwise the data will be mangled
            mapper.TrimWhitespace = false;

            colAll = db.GetCollection<PacketDecoder>("All");
            colAll.EnsureIndex(x => x.StreamId);
            colAll.EnsureIndex(x => x.PacketDescriptor);
            colAll.EnsureIndex(x => x.FrameNumber);

            colUni = db.GetCollection<Uni>("Uni");
            colUniDetail = db.GetCollection<UniDetail>("UniDetail");
            colUniDetail.EnsureIndex(x => x.UniId);
            colUniDetail.EnsureIndex(x => x.MIdOrLCNo);
        }

        private static MemoryStream readStream(string path)
        {
            var ms = new MemoryStream();
            try
            {
                using (var temp = new MemoryStream(File.ReadAllBytes(path)))
                {
                    temp.CopyTo(ms);

                    return ms;
                }
            }
            catch (Exception ex)
            {
                Utils.Log.ConditionalDebug(ex);
                return ms;
            }
        }

        /// <summary>
        /// This method gets called for every packet that is part of the EGC.
        /// The packet is passed as the argument.
        /// 
        /// If the packet is not part of the EGC, the method gets called for every
        /// packet only if the checkbox "Use Local Db Store" is checked, in which
        /// case again, the packet is passed as argument.
        /// </summary>
        public void AddAnyPacket(PacketArgs e)
        {
            int packetDescriptor = e.PacketDescriptor;
            switch (packetDescriptor)
            {
                case 0xB1:
                    PacketDecoderB1 pdB1 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderB1>(e.SerializedData);
                    if (pdB1.IsCRC)
                    {
                        colAll.Insert(pdB1);
                        AddPacketToUni(IAMEGC, e, packetDescriptor);
                    }
                    break;

                case 0xB2:
                    PacketDecoderB2 pdB2 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderB2>(e.SerializedData);
                    if (pdB2.IsCRC)
                    {
                        colAll.Insert(pdB2);
                        AddPacketToUni(IAMEGC, e, packetDescriptor);
                    }
                    break;

                case 0xBD:
                    PacketDecoderBD pdBD = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderBD>(e.SerializedData);
                    if (pdBD.IsCRC)
                    {
                        colAll.Insert(pdBD);

                        //only the 0xB1 and 0xB2 go to EGC, the rest go into messages.
                        switch (pdBD.MultiframePacketDescriptor)
                        {
                            case 0xB1:
                                AddPacketToUni(IAMEGC, e, packetDescriptor, 0xB1);
                                break;

                            case 0xB2:
                                AddPacketToUni(IAMEGC, e, packetDescriptor, 0xB2);
                                break;

                            case 0xAA:
                                AddPacketToUni(NOTEGC, e, packetDescriptor, 0xAA);
                                break;
                        }
                    }

                    break;

                case 0xBE:
                    PacketDecoderBE pdBE = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderBE>(e.SerializedData);
                    if (pdBE.IsCRC)
                    {
                        colAll.Insert(pdBE);

                        //only the 0xB1 and 0xB2 go to EGC, the rest go into messages.
                        switch (pdBE.MultiframePacketDescriptor)
                        {
                            case 0xB1:
                                AddPacketToUni(IAMEGC, e, packetDescriptor, 0xB1);
                                break;

                            case 0xB2:
                                AddPacketToUni(IAMEGC, e, packetDescriptor, 0xB2);
                                break;

                            case 0xAA:
                                AddPacketToUni(NOTEGC, e, packetDescriptor, 0xAA);
                                break;
                        }
                    }

                    break;

                case 0xAA:
                    PacketDecoderAA pdAA = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderAA>(e.SerializedData);
                    if (pdAA.IsCRC)
                    {
                        colAll.Insert(pdAA);
                        AddPacketToUni(NOTEGC, e, packetDescriptor);
                    }
                    break;

                case 0x7D:
                    PacketDecoder7D pd7D = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder7D>(e.SerializedData);
                    if (pd7D.IsCRC)
                    {
                        colAll.Insert(pd7D);
                    }

                    break;

                case 0x92:
                    PacketDecoder92 pd92 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder92>(e.SerializedData);
                    if (pd92.IsCRC)
                    {
                        colAll.Insert(pd92);
                    }
                    break;

                case 0xAB:
                    PacketDecoderAB pdAB = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderAB>(e.SerializedData);
                    if (pdAB.IsCRC)
                    {
                        colAll.Insert(pdAB);
                    }
                    break;

                case 0x6C:
                    PacketDecoder6C pd6C = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder6C>(e.SerializedData);
                    if (pd6C.IsCRC)
                    {
                        colAll.Insert(pd6C);
                    }
                    break;

                case 0x83:
                    PacketDecoder83 pd83 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder83>(e.SerializedData);
                    if (pd83.IsCRC)
                    {
                        colAll.Insert(pd83);
                    }
                    break;

                case 0x27:
                    PacketDecoder27 pd27 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder27>(e.SerializedData);
                    if (pd27.IsCRC)
                    {
                        colAll.Insert(pd27);
                    }
                    break;

                case 0x2A:
                    PacketDecoder2A pd2A = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder2A>(e.SerializedData);
                    if (pd2A.IsCRC)
                    {
                        colAll.Insert(pd2A);
                    }
                    break;

                case 0x08:
                    PacketDecoder08 pd08 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder08>(e.SerializedData);
                    if (pd08.IsCRC)
                    {
                        colAll.Insert(pd08);
                    }
                    break;

                case 0x81:
                    PacketDecoder81 pd81 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder81>(e.SerializedData);
                    if (pd81.IsCRC)
                    {
                        colAll.Insert(pd81);
                    }
                    break;

                case 0xA3:
                    PacketDecoderA3 pdA3 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderA3>(e.SerializedData);
                    if (pdA3.IsCRC)
                    {
                        colAll.Insert(pdA3);
                    }
                    break;

                case 0xA8:
                    PacketDecoderA8 pdA8 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderA8>(e.SerializedData);
                    if (pdA8.IsCRC)
                    {
                        colAll.Insert(pdA8);
                    }
                    break;

                default:
                    PacketDecoder pd = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder>(e.SerializedData);
                    if (pd.IsCRC)
                    {
                        colAll.Insert(pd);
                    }
                    break;
            }
        }

        #region Uni

        /// <summary>
        /// This method gets called for every packet that is responsible for Messages or EGCs.
        /// Eevery time the information arrives, the calculations are made,
        /// the data is stored display-ready into the database and then we make a call
        /// through an even to the UI to update the particular record that got changed.
        /// 
        /// There is no need to check for CRC, a packet gets here only if it has a valid CRC.
        /// </summary>
        private void AddPacketToUni(bool isEGC, PacketArgs e, int packetDescriptor, int multiFrameDescriptor = 0)
        {
            Uni uni = null;
            int uniId = 0;
            PacketDecoder7D bb = null;
            List<UniDetail> unids = new List<UniDetail>();

            switch (packetDescriptor)
            {
                case 0xB1:
                    //this is a packet that belongs to an Egc
                    try
                    {
                        PacketDecoderB1 pdB1 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderB1>(e.SerializedData);

                        DateTime dtB1 = (pdB1.Time == DateTime.MinValue) ? DateTime.MinValue : pdB1.Time.AddHours(-10);
                        uni = colUni
                            .FindOne(x => x.StreamId == e.StreamId && x.MIdOrLCNo == pdB1.MessageId && x.Repetition == pdB1.Repetition && x.Time > dtB1);

                        if (uni == null)
                        {
                            uni = new Uni
                            {
                                StreamId = e.StreamId,
                                IsEGC = true,
                                Priority = pdB1.Priority,
                                MIdOrLCNo = pdB1.MessageId,
                                Repetition = pdB1.Repetition,
                                MessageType = pdB1.MessageType,
                                IsPartial = true,
                                TimeReceived = DateTime.UtcNow,
                                AddressHex = pdB1.AddressHex,
                                Area = PacketDecoderGeoUtils.ReturnArea(pdB1.MessageType, pdB1.AddressHex)
                            };
                            uniId = colUni.Insert(uni);
                        }
                        else
                        {
                            uniId = uni.UniId;
                        }

                        GetUniBulletinBoard(e.StreamId, pdB1.FrameNumber, out bb);
                        if (bb != null)
                        {
                            uni.Time = bb.Time;
                            uni.Sat = bb.Sat;
                            uni.SatName = bb.SatName;
                            uni.LesId = bb.LesId;
                            uni.LesName = bb.LesName;
                            uni.BulletinBoard = bb;
                        }
                        else
                        {
                            uni.Time = DateTime.MinValue;
                        }

                        colUniDetail.Insert(new UniDetail(uniId, IAMEGC, pdB1, bb));
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Error(ex);
                    }
                    break;

                case 0xB2:
                    //this is a packet that belongs to an Egc
                    try
                    {
                        PacketDecoderB2 pdB2 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderB2>(e.SerializedData);

                        DateTime dtB2 = (pdB2.Time == DateTime.MinValue) ? DateTime.MinValue : pdB2.Time.AddHours(-10);
                        uni = colUni
                            .FindOne(x => x.StreamId == e.StreamId && x.MIdOrLCNo == pdB2.MessageId && x.Repetition == pdB2.Repetition && x.Time > dtB2);

                        if (uni == null)
                        {
                            uni = new Uni
                            {
                                StreamId = e.StreamId,
                                IsEGC = true,
                                Priority = pdB2.Priority,
                                MIdOrLCNo = pdB2.MessageId,
                                Repetition = pdB2.Repetition,
                                MessageType = pdB2.MessageType,
                                IsPartial = true,
                                TimeReceived = DateTime.UtcNow,
                                AddressHex = pdB2.AddressHex,
                                Area = PacketDecoderGeoUtils.ReturnArea(pdB2.MessageType, pdB2.AddressHex)
                            };
                            uniId = colUni.Insert(uni);
                        }
                        else
                        {
                            uniId = uni.UniId;
                        }

                        GetUniBulletinBoard(e.StreamId, pdB2.FrameNumber, out bb);
                        if (bb != null)
                        {
                            uni.Time = bb.Time;
                            uni.Sat = bb.Sat;
                            uni.SatName = bb.SatName;
                            uni.LesId = bb.LesId;
                            uni.LesName = bb.LesName;
                            uni.BulletinBoard = bb;
                        }
                        else
                        {
                            uni.Time = DateTime.MinValue;
                        }

                        colUniDetail.Insert(new UniDetail(uniId, IAMEGC, pdB2, bb));
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Error(ex);
                    }
                    break;

                case 0xAA:
                    //this is a packet that belongs to a Msg
                    try
                    {
                        PacketDecoderAA pdAA = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderAA>(e.SerializedData);

                        DateTime dtAA = (pdAA.Time == DateTime.MinValue) ? DateTime.MinValue : pdAA.Time.AddMinutes(-15);
                        uni = colUni
                            .FindOne(x => x.StreamId == e.StreamId && x.MIdOrLCNo == pdAA.LogicalChannelNo && x.Time > dtAA);

                        if (uni == null)
                        {
                            uni = new Uni
                            {
                                StreamId = e.StreamId,
                                IsEGC = false,
                                MIdOrLCNo = pdAA.LogicalChannelNo,
                                IsPartial = true,
                                TimeReceived = DateTime.UtcNow
                            };
                            uniId = colUni.Insert(uni);
                        }
                        else
                        {
                            uniId = uni.UniId;
                        }

                        GetUniBulletinBoard(e.StreamId, pdAA.FrameNumber, out bb);
                        if (bb != null)
                        {
                            uni.Time = bb.Time;
                            uni.Sat = bb.Sat;
                            uni.SatName = bb.SatName;
                            uni.LesId = bb.LesId;
                            uni.LesName = bb.LesName;
                            uni.BulletinBoard = bb;
                        }
                        else
                        {
                            uni.Time = DateTime.MinValue;
                        }

                        colUniDetail.Insert(new UniDetail(uniId, NOTEGC, pdAA, bb));
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Error(ex);
                    }
                    break;

                case 0xBD:
                    try
                    {
                        PacketDecoderBD pdBD = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderBD>(e.SerializedData);

                        switch (multiFrameDescriptor)
                        {
                            case 0xB1:
                            case 0xB2:
                                //this is a packet that belongs to an Egc
                                //the packets have been already filter and the only received packets here are the ones
                                //containing either a continuation for B1 or a continuation for B2
                                try
                                {
                                    DateTime dtBDB1B2 = (pdBD.Time == DateTime.MinValue) ? DateTime.MinValue : pdBD.Time.AddHours(-10);
                                    uni = colUni
                                        .FindOne(x => x.StreamId == e.StreamId && x.MIdOrLCNo == pdBD.MessageId && x.Repetition == pdBD.Repetition && x.Time > dtBDB1B2);

                                    if (uni == null)
                                    {
                                        uni = new Uni
                                        {
                                            StreamId = e.StreamId,
                                            IsEGC = true,
                                            Priority = pdBD.Priority,
                                            MIdOrLCNo = pdBD.MessageId,
                                            Repetition = pdBD.Repetition,
                                            MessageType = pdBD.MessageType,
                                            IsPartial = true,
                                            TimeReceived = DateTime.UtcNow,
                                            AddressHex = pdBD.AddressHex,
                                            Area = PacketDecoderGeoUtils.ReturnArea(pdBD.MessageType, pdBD.AddressHex)
                                        };
                                        uniId = colUni.Insert(uni);
                                    }
                                    else
                                    {
                                        uniId = uni.UniId;
                                    }

                                    GetUniBulletinBoard(e.StreamId, pdBD.FrameNumber, out bb);
                                    if (bb != null)
                                    {
                                        uni.Time = bb.Time;
                                        uni.Sat = bb.Sat;
                                        uni.LesId = bb.LesId;
                                        uni.BulletinBoard = bb;
                                    }
                                    else
                                    {
                                        uni.Time = DateTime.MinValue;
                                    }

                                    colUniDetail.Insert(new UniDetail(uniId, IAMEGC, pdBD, bb));
                                }
                                catch (Exception ex)
                                {
                                    Utils.Log.Error(ex);
                                }
                                break;

                            case 0xAA:
                                //this is a packet that belongs to a Msg
                                try
                                {
                                    DateTime dtBDAA = (pdBD.Time == DateTime.MinValue) ? DateTime.MinValue : pdBD.Time.AddMinutes(-15);
                                    uni = colUni
                                        .FindOne(x => x.StreamId == e.StreamId && x.MIdOrLCNo == pdBD.LogicalChannelNo && x.Time > dtBDAA);

                                    if (uni == null)
                                    {
                                        uni = new Uni
                                        {
                                            StreamId = e.StreamId,
                                            IsEGC = false,
                                            MIdOrLCNo = pdBD.LogicalChannelNo,
                                            IsPartial = true,
                                            TimeReceived = DateTime.UtcNow
                                        };
                                        uniId = colUni.Insert(uni);
                                    }
                                    else
                                    {
                                        uniId = uni.UniId;
                                    }

                                    GetUniBulletinBoard(e.StreamId, pdBD.FrameNumber, out bb);
                                    if (bb != null)
                                    {
                                        uni.Time = bb.Time;
                                        uni.Sat = bb.Sat;
                                        uni.SatName = bb.SatName;
                                        uni.LesId = bb.LesId;
                                        uni.LesName = bb.LesName;
                                        uni.BulletinBoard = bb;
                                    }
                                    else
                                    {
                                        uni.Time = DateTime.MinValue;
                                    }

                                    colUniDetail.Insert(new UniDetail(uniId, NOTEGC, pdBD, bb));
                                }
                                catch (Exception ex)
                                {
                                    Utils.Log.Error(ex);
                                }
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Error(ex);
                    }
                    break;

                case 0xBE:
                    try
                    {
                        PacketDecoderBE pdBE = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderBE>(e.SerializedData);

                        switch (multiFrameDescriptor)
                        {
                            case 0xB1:
                            case 0xB2:
                                //this is a packet that belongs to an Egc
                                //the packets have been already filter and the only received packets here are the ones
                                //containing either a continuation for B1 or a continuation for B2
                                try
                                {
                                    DateTime dtBEB1B2 = (pdBE.Time == DateTime.MinValue) ? DateTime.MinValue : pdBE.Time.AddHours(-10);
                                    uni = colUni
                                        .FindOne(x => x.StreamId == e.StreamId && x.MIdOrLCNo == pdBE.MessageId && x.Repetition == pdBE.Repetition && x.Time > dtBEB1B2);

                                    if (uni == null)
                                    {
                                        uni = new Uni
                                        {
                                            StreamId = e.StreamId,
                                            IsEGC = true,
                                            Priority = pdBE.Priority,
                                            MIdOrLCNo = pdBE.MessageId,
                                            Repetition = pdBE.Repetition,
                                            MessageType = pdBE.MessageType,
                                            IsPartial = true,
                                            TimeReceived = DateTime.UtcNow,
                                            AddressHex = null,
                                            Area = null
                                        };
                                        uniId = colUni.Insert(uni);
                                    }
                                    else
                                    {
                                        uniId = uni.UniId;
                                    }

                                    GetUniBulletinBoard(e.StreamId, pdBE.FrameNumber, out bb);
                                    if (bb != null)
                                    {
                                        uni.Time = bb.Time;
                                        uni.Sat = bb.Sat;
                                        uni.LesId = bb.LesId;
                                        uni.BulletinBoard = bb;
                                    }
                                    else
                                    {
                                        uni.Time = DateTime.MinValue;
                                    }

                                    colUniDetail.Insert(new UniDetail(uniId, IAMEGC, pdBE, bb));
                                }
                                catch (Exception ex)
                                {
                                    Utils.Log.Error(ex);
                                }
                                break;

                            case 0xAA:
                                //this is a packet that belongs to a Msg
                                try
                                {
                                    DateTime dtBEAA = (pdBE.Time == DateTime.MinValue) ? DateTime.MinValue : pdBE.Time.AddMinutes(-15);
                                    uni = colUni
                                        .FindOne(x => x.StreamId == e.StreamId && x.MIdOrLCNo == pdBE.LogicalChannelNo && x.Time > dtBEAA);

                                    if (uni == null)
                                    {
                                        uni = new Uni
                                        {
                                            StreamId = e.StreamId,
                                            IsEGC = false,
                                            MIdOrLCNo = pdBE.LogicalChannelNo,
                                            IsPartial = true,
                                            TimeReceived = DateTime.UtcNow
                                        };
                                        uniId = colUni.Insert(uni);
                                    }
                                    else
                                    {
                                        uniId = uni.UniId;
                                    }

                                    GetUniBulletinBoard(e.StreamId, pdBE.FrameNumber, out bb);
                                    if (bb != null)
                                    {
                                        uni.Time = bb.Time;
                                        uni.Sat = bb.Sat;
                                        uni.SatName = bb.SatName;
                                        uni.LesId = bb.LesId;
                                        uni.LesName = bb.LesName;
                                        uni.BulletinBoard = bb;
                                    }
                                    else
                                    {
                                        uni.Time = DateTime.MinValue;
                                    }

                                    colUniDetail.Insert(new UniDetail(uniId, NOTEGC, pdBE, bb));
                                }
                                catch (Exception ex)
                                {
                                    Utils.Log.Error(ex);
                                }
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Error(ex);
                    }
                    break;
            }

            if (packetDescriptor == 0x7D || uni == null || bb == null)
            {
                return;
            }

            /// 
            /// Evaluate if packets are partial or fully received.
            /// Check for partial only for so many packets as there is a max limit of packets a message can span over.
            /// Also, if a partial message has received its final part, there is no need to refresh it again as there
            /// will no more new information to be received.
            /// 
            /// The result is sorted by default on UniDetaiId
            /// 

            unids.Clear();
            unids.AddRange(colUniDetail
                .Find(x => x.UniId == uni.UniId && x.MIdOrLCNo == uni.MIdOrLCNo));

            if (isEGC)
            {
                if (unids == null)
                {
                    return;
                }
                else
                {
                    //EGC is always IA5
                    uni.ContentType = PayloadType.CNT_IA5;
                }

                uni.PartsReceived = unids.Count;

                /// 
                /// A received EGC is complete when:
                ///     The first packet number is 1
                ///     The last B1/B2/BE packet has a Continuation of 0
                ///     The packet PacketNo are consecutive.
                ///

                bool complete = true;

                //is first packet number 1?
                if (unids[0].PacketNo != 1)
                {
                    complete = false;
                }

                if (complete)
                {
                    //does Continuation ends with 0?
                    //the BE continuation is irrelevant
                    UniDetail last = unids
                        .FindLast(x => x.PacketDescriptorHex == "B1" || x.PacketDescriptorHex == "B2" || x.PacketDescriptorHex == "BD");

                    if (last == null)
                    {
                        complete = false;
                    }
                    else if (last.Continuation != 0)
                    {
                        complete = false;
                    }
                }

                if (complete)
                {
                    //are the PacketNo consecutive?
                    //no need to sort and the PacketNo are always in ascending order
                    int[] pNos = unids
                        .Select(x => x.PacketNo)
                        .Distinct()
                        .ToArray();

                    int diff = 1;
                    for (int i = 0; i < pNos.Length - 1; i++)
                    {
                        diff = pNos[i + 1] - pNos[i];
                        if (diff > 1)
                        {
                            break;
                        }
                    }

                    if (diff > 1)
                    {
                        complete = false;
                    }
                }

                if (complete)
                {
                    //if it gets here, we have a full message
                    uni.IsPartial = false;
                }

                /// 
                /// Assemble the message
                ///

                StringBuilder sb = new StringBuilder();
                int prevPacketNo = 0;
                foreach (UniDetail detail in unids)
                {
                    // append a spacer if packets are missing
                    if (prevPacketNo < detail.PacketNo - 1)
                    {
                        sb.Append("\r\n<~ ~ ~>\r\n");
                    }

                    sb.Append(detail.Payload_.Ia5);
                    prevPacketNo = detail.PacketNo;
                }

                // append an extra spacer if the packet is null or partial and continuation does not end with zero
                if (uni.IsPartial)
                {
                    //does Continuation ends with 0?
                    UniDetail last = unids
                        .FindLast(x => x.PacketDescriptorHex == "B1" || x.PacketDescriptorHex == "B2" || x.PacketDescriptorHex == "BD");

                    if (last == null)
                    {
                        sb.Append("\r\n<~ ~ ~>\r\n");
                    }
                    else if (last.Continuation != 0)
                    {
                        sb.Append("\r\n<~ ~ ~>\r\n");
                    }
                }

                uni.Message = sb.ToString();

                // update database and notify UI
                if (colUni.Update(uni))
                {
                    UniUpdatedArgs args = new UniUpdatedArgs();
                    args.Uni_ = uni;

                    OnUniUpdated?.Invoke(this, args);
                }
            }
            else
            {
                if (unids == null)
                {
                    return;
                }
                else
                {
                    uni.ContentType = ReturnPayloadContentType(unids[0].Payload_);
                }

                uni.PartsReceived = unids.Count;

                //are the parts contiguous?
                //if they are contiguous we are able to decode the message if binary or ita2
                bool contiguous = true;

                //is first packet number 1?
                if (unids[0].PacketNo != 1)
                {
                    contiguous = false;
                }

                if (contiguous)
                {
                    //are the PacketNo consecutive?
                    //no need to sort and the PacketNo are always in ascending order
                    int[] pNos = unids
                        .Select(x => x.PacketNo)
                        .Distinct()
                        .ToArray();

                    int diff = 1;
                    for (int i = 0; i < pNos.Length - 1; i++)
                    {
                        diff = pNos[i + 1] - pNos[i];
                        if (diff > 1)
                        {
                            break;
                        }
                    }

                    if (diff > 1)
                    {
                        contiguous = false;
                    }
                }

                if (contiguous)
                {
                    //if it gets here, we have a full message
                    uni.IsPartial = false;
                }

                //IA5
                if (uni.ContentType == PayloadType.CNT_IA5)
                {
                    /// 
                    /// Assemble the message
                    ///

                    StringBuilder sb = new StringBuilder();
                    int prevPacketNo = 0;
                    foreach (UniDetail detail in unids)
                    {
                        // append a spacer if packets are missing
                        if (prevPacketNo < detail.PacketNo - 1)
                        {
                            sb.Append("\r\n<~ ~ ~>\r\n");
                        }

                        sb.Append(detail.Payload_.Ia5);
                        prevPacketNo = detail.PacketNo;
                    }

                    uni.Message = sb.ToString();

                    if (IsZappingMessage(uni.Message))
                    {
                        uni.ContentType = PayloadType.CNT_ZAP;
                    }

                    // update database and notify UI
                    if (colUni.Update(uni))
                    {
                        UniUpdatedArgs args = new UniUpdatedArgs
                        {
                            Uni_ = uni
                        };

                        OnUniUpdated?.Invoke(this, args);
                    }
                }
                else if (contiguous)
                {
                    //we know the message is binary and contiguous, we attempt to decode it as Ita2
                    byte[] payload = new byte[unids.Count * 640];
                    int pos = 0;

                    foreach (UniDetail detail in unids)
                    {
                        int length = detail.Payload_.Data8Bit.Length;
                        Array.Copy(detail.Payload_.Data8Bit, 0, payload, pos, length);
                        pos += length;
                    }
                    Array.Resize<byte>(ref payload, pos);

                    //attempt to decode as Ita2
                    Ita2Decoder ita2 = new Ita2Decoder();
                    string payloadIta2 = ita2.Decode(Utils.BytesToHexString(payload, 0, pos));

                    //verify if ita2
                    bool isIta2 = PacketDecoderUtils.IsBinary(payload, true); //as opposed to ASCII
                    if (isIta2)
                    {
                        //check for known string sequences that are less likely to be in  anormal text
                        //sequences that come from the Ita2 decoder
                        if (payloadIta2.Contains("{BELL}") || payloadIta2.Contains("{ENQ}"))
                        {
                            isIta2 = false;
                        }
                    }
                    uni.ContentType = isIta2 ? PayloadType.CNT_ITA2 : PayloadType.CNT_BINARY;

                    //ITA2
                    if (uni.ContentType == PayloadType.CNT_ITA2)
                    {
                        uni.Message = payloadIta2;
                    }
                    else
                    {
                        uni.Message = "BINARY, MULTI-ENCODED or INCOMPLETE content. TBD.";
                    }

                    // update database and notify UI
                    if (colUni.Update(uni))
                    {
                        UniUpdatedArgs args = new UniUpdatedArgs
                        {
                            Uni_ = uni
                        };

                        OnUniUpdated?.Invoke(this, args);
                    }
                }
                else
                {
                    //no point in trying to decode as only a part of this message will be decoded properly
                    //this is either partial message or multi encoding
                    uni.ContentType = PayloadType.CNT_BINARY;
                    uni.Message = "BINARY, MULTI-ENCODED or INCOMPLETE content. TBD.";

                    // update database and notify UI
                    if (colUni.Update(uni))
                    {
                        UniUpdatedArgs args = new UniUpdatedArgs
                        {
                            Uni_ = uni
                        };

                        OnUniUpdated?.Invoke(this, args);
                    }
                }
            }
        }

        private int ReturnPayloadContentType(Payload payload_)
        {
            if (payload_.Ia5 != null)
            {
                return PayloadType.CNT_IA5;
            }

            if (payload_.Ita2 != null)
            {
                return PayloadType.CNT_ITA2;
            }

            return PayloadType.CNT_BINARY;
        }

        private bool IsZappingMessage(string message)
        {
            return message.Contains("abcdefghijkl");
        }

        /// <summary>
        /// Sets the reference to the most recent bulletin board that contains the given frame number.
        /// Used to get the Uni Time and other data.
        /// </summary>
        private void GetUniBulletinBoard(long streamId, int frameNumber, out PacketDecoder7D bb)
        {
            //get the 7D packet time for the frame number
            bb = (PacketDecoder7D)colAll
                .Find(x => x.StreamId == streamId && x.PacketDescriptor == 0x7D && x.FrameNumber == frameNumber).LastOrDefault();
        }

        /// <summary>
        /// Retrives all Unis from the database
        /// </summary>
        public void LoadUnis(ref List<Uni> unis)
        {
            unis.AddRange(colUni.FindAll());
        }

        #endregion

        /// <summary>
        /// Flush db memory to file.
        /// </summary>
        public void Flush()
        {
            try
            {
                File.WriteAllBytes(fullPathToDb, mem.ToArray());
            }
            catch(Exception ex)
            {
                Utils.Log.Error(ex);
            }
        }

        /// <summary>
        /// Purge db of all records older than nMaxStoredHours.
        /// </summary>
        public void Purge(int nMaxStoredHours)
        {
            DateTime utcPurgeEndTime = DateTime.UtcNow.ToLocalTime().AddHours(-nMaxStoredHours);

            //purge all records but Unis
            //TODO

            //Purge Unis
            PurgeUnis(utcPurgeEndTime);
        }

        /// <summary>
        /// Purge db of all Unis received earlier than utcPurgeEndTime.
        /// </summary>
        private void PurgeUnis(DateTime utcPurgeEndTime)
        {
            List<Uni> unis = new List<Uni>();
            unis.AddRange(colUni.Find(
                x => x.TimeReceived < utcPurgeEndTime
                ));
            foreach (Uni uni in unis)
            {
                colUni.Delete(uni.UniId);

                UniDeletedArgs ea = new UniDeletedArgs
                {
                    UniId = uni.UniId
                };
                OnUniDeleted?.Invoke(this, ea);
            }
        }
    }
}
