﻿using LiteDB;
using ScytaleC.PacketDecoders;
using System;

namespace ScytaleC.QuickUI
{
    public class Uni
    {
        [BsonId]
        public int UniId { get; set; }

        public long StreamId { get; set; }
        public bool IsEGC { get; set; }
        public int Sat { get; set; }
        public string SatName { get; set; }
        public int LesId { get; set; }
        public string LesName { get; set; }

        // Time (Sat and LesId) is the Utc Time of the most recent BB packet attached
        // to any packet that constitutes the message (AA, BD or BE)
        public DateTime Time { get; set; }

        // TimeReceived is the Utc time the Msg was created.
        // It has no reference to the packet time.
        public DateTime TimeReceived { get; set; }

        public int MIdOrLCNo { get; set; }
        public int Repetition { get; set; }
        public int MessageType { get; set; }
        public int Priority { get; set; }
        public int ContentType { get; set; }
        public int PartsReceived { get; set; }
        public string Message { get; set; }
        public bool IsPartial { get; set; }
        public PacketDecoder7D BulletinBoard { get; set; }
        public string AddressHex { get; set; }
        public GeoArea Area { get; set; }
    }

    public class UniDeletedArgs : EventArgs
    {
        public int UniId { get; set; }
    }

    public class UniUpdatedArgs : EventArgs
    {
        public Uni Uni_ { set; get; }
    }

    public class UniDetail : PacketDecoder
    {
        [BsonId]
        public int UniDetailId { get; set; }

        public int UniId { get; set; }

        public int MIdOrLCNo { get; set; }
        public int PacketNo { get; set; }
        public int Continuation { get; set; }
        public PacketDecoder7D PacketDecoder7D_ { get; set; }


        public UniDetail() { }

        public UniDetail(int uniId, bool isEGC, PacketDecoder pd, PacketDecoder7D pd7D)
        {
            UniId = uniId;

            switch (pd.PacketDescriptor)
            {
                case 0xB1:
                    MIdOrLCNo = ((PacketDecoderB1)pd).MessageId;
                    Continuation = ((PacketDecoderB1)pd).Continuation;
                    Payload_ = ((PacketDecoderB1)pd).Payload_;
                    PacketNo = ((PacketDecoderB1)pd).PacketNo;
                    break;

                case 0xB2:
                    MIdOrLCNo = ((PacketDecoderB2)pd).MessageId;
                    Continuation = ((PacketDecoderB2)pd).Continuation;
                    Payload_ = ((PacketDecoderB2)pd).Payload_;
                    PacketNo = ((PacketDecoderB2)pd).PacketNo;
                    break;

                case 0xBD:
                    if (isEGC)
                    {
                        MIdOrLCNo = ((PacketDecoderBD)pd).MessageId;
                        Continuation = ((PacketDecoderBD)pd).Continuation;
                    }
                    else
                    {
                        MIdOrLCNo = ((PacketDecoderBD)pd).LogicalChannelNo;
                    }
                    Payload_ = ((PacketDecoderBD)pd).Payload_;
                    PacketNo = ((PacketDecoderBD)pd).PacketNo;
                    break;

                case 0xBE:
                    if (isEGC)
                    {
                        MIdOrLCNo = ((PacketDecoderBE)pd).MessageId;
                        Continuation = ((PacketDecoderBE)pd).Continuation;
                    }
                    else
                    {
                        MIdOrLCNo = ((PacketDecoderBE)pd).LogicalChannelNo;
                    }
                    Payload_ = ((PacketDecoderBE)pd).Payload_;
                    PacketNo = ((PacketDecoderBE)pd).PacketNo;
                    break;

                case 0xAA:
                    MIdOrLCNo = ((PacketDecoderAA)pd).LogicalChannelNo;
                    Payload_ = ((PacketDecoderAA)pd).Payload_;
                    PacketNo = ((PacketDecoderAA)pd).PacketNo;
                    break;
            }

            PacketDescriptor = pd.PacketDescriptor;
            PacketDescriptorHex = pd.PacketDescriptorHex;
            FrameNumber = pd.FrameNumber;
            PacketDecoder7D_ = pd7D;
        }
    }
}