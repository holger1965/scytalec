﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * https://stackoverflow.com/questions/9308673/how-to-draw-circle-on-the-map-using-gmap-net-in-c-sharp
 * http://www.independent-software.com/gmap-net-beginners-tutorial-maps-markers-polygons-routes-updated-for-visual-studio-2015-and-gmap-net-1-7/
 * https://stackoverflow.com/questions/19608850/gmap-dragging-using-left-mouse-button
 * 
 */

using GMap.NET;
using GMap.NET.WindowsForms;
using ScytaleC.PacketDecoders;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ScytaleC.QuickUI
{
    public class GMapper
    {
        private GMapControl gmap;
        private GMapOverlay polygons;

        public GMapper(GMapControl gmap)
        {
            this.gmap = gmap;
            polygons = new GMapOverlay("ImpactAreas");
        }

        public void Initialize()
        {
            try
            {
                gmap.MapProvider = GMap.NET.MapProviders.OpenStreetMapProvider.Instance;
                gmap.ShowCenter = false;
                gmap.DragButton = MouseButtons.Left;

                gmap.SetPositionByKeywords("Charlottetown, Canada");
                gmap.Zoom = 4;
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
            }
        }

        public void Disconnect()
        {
            gmap.Hide();
        }

        private List<PointLatLng> CreateCirclePoints(PointLatLng center, int radiusNM, int points)
        {
            List<PointLatLng> res = new List<PointLatLng>();
            GeoPoint gcenter = new GeoPoint(center.Lat, center.Lng); 

            //we compute clockwise, a fraction at a time
            double fraction = 2 * Math.PI / points;
            double angle;
            for (int i = 0; i < points; i++)
            {
                angle = fraction * i;
                GeoPoint gp = PacketDecoderGeoUtils.FindPointAtDistanceFrom(gcenter, angle, radiusNM * 1.852);
                res.Add(new PointLatLng(gp.Lat, gp.Lng));
            }

            return res;
        }

        public bool DrawRectangularArea(RectangularArea ra)
        {
            if (ra == null)
            {
                gmap.Overlays.Clear();
                polygons.Polygons.Clear();
                return false;
            }
            else
            {
                List<PointLatLng> points = new List<PointLatLng>
                {
                    new PointLatLng(ra.NWCorner.Lat, ra.NWCorner.Lng),
                    new PointLatLng(ra.NECorner.Lat, ra.NECorner.Lng),
                    new PointLatLng(ra.SECorner.Lat, ra.SECorner.Lng),
                    new PointLatLng(ra.SWCorner.Lat, ra.SWCorner.Lng)
                };

                GMapPolygon polygon = new GMapPolygon(points, "Rectangular Area")
                {
                    Fill = new SolidBrush(Color.FromArgb(50, Color.SteelBlue)),
                    Stroke = new Pen(Color.Navy, 1)
                };
                gmap.Overlays.Clear();
                polygons.Polygons.Clear();
                gmap.Overlays.Add(polygons);
                polygons.Polygons.Add(polygon);

                gmap.Position = new PointLatLng(ra.Center.Lat, ra.Center.Lng);
                return true;
            }
        }

        internal void Clear()
        {
            polygons.Clear();
        }

        public bool DrawCircularArea(CircularArea ca)
        {
            if (ca == null)
            {
                gmap.Overlays.Clear();
                polygons.Polygons.Clear();
                return false;
            }
            else
            {
                List<PointLatLng> points = CreateCirclePoints(new PointLatLng(ca.Center.Lat, ca.Center.Lng), ca.RadiusNM, 150);
                GMapPolygon polygon = new GMapPolygon(points, "Circular Area")
                {
                    Fill = new SolidBrush(Color.FromArgb(50, Color.SteelBlue)),
                    Stroke = new Pen(Color.Navy, 1)
                };
                gmap.Overlays.Clear();
                polygons.Polygons.Clear();
                gmap.Overlays.Add(polygons);
                polygons.Polygons.Add(polygon);

                gmap.Position = new PointLatLng(ca.Center.Lat, ca.Center.Lng);
                return true;
            }
        }

        public bool DrawNavArea(NavArea na)
        {
            if (na == null)
            {
                gmap.Overlays.Clear();
                polygons.Polygons.Clear();
                return false;
            }

            if (na.XmlArea == null)
            {
                gmap.Overlays.Clear();
                polygons.Polygons.Clear();
                return false;
            }

            if (na.XmlArea == null)
            {
                return false;
            }

            try
            {
                List<PointLatLng> points = new List<PointLatLng>();

                foreach (GeoPoint gp in na.XmlArea.Coordinates)
                {
                    points.Add(new PointLatLng(gp.Lat, gp.Lng));
                }

                GMapPolygon polygon = new GMapPolygon(points, "NavArea");
                polygon.Fill = new SolidBrush(Color.FromArgb(50, na.XmlArea.BkgColor));
                polygon.Stroke = new Pen(na.XmlArea.PrmColor, 1);
                gmap.Overlays.Clear();
                polygons.Polygons.Clear();
                gmap.Overlays.Add(polygons);
                polygons.Polygons.Add(polygon);

                return true;
            }
            catch(Exception ex)
            {
                Utils.Log.Error(ex);
                return false;
            }
        }

        public bool DrawCoastalArea(CoastalArea coa)
        {
            if (coa == null)
            {
                gmap.Overlays.Clear();
                polygons.Polygons.Clear();
                return false;
            }
            else
            {
                //List<PointLatLng> points = CreateCirclePoints(ca.Center, ca.RadiusNM, 150);
                //GMapPolygon polygon = new GMapPolygon(points, "Circular Area");
                //polygon.Fill = new SolidBrush(Color.FromArgb(50, Color.SteelBlue));
                //polygon.Stroke = new Pen(Color.Navy, 1);
                //gmap.Overlays.Clear();
                //polygons.Polygons.Clear();
                //gmap.Overlays.Add(polygons);
                //polygons.Polygons.Add(polygon);

                //gmap.Position = ca.Center;

                gmap.Overlays.Clear();
                polygons.Polygons.Clear();
                return true;
            }
        }
    }
}
