﻿namespace ScytaleC.QuickUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl6a = new System.Windows.Forms.Label();
            this.lbl3a = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl8a = new System.Windows.Forms.Label();
            this.lbl5a = new System.Windows.Forms.Label();
            this.lbl2a = new System.Windows.Forms.Label();
            this.lbl8 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl9a = new System.Windows.Forms.Label();
            this.lbl7a = new System.Windows.Forms.Label();
            this.lbl4a = new System.Windows.Forms.Label();
            this.lbl1a = new System.Windows.Forms.Label();
            this.lbl0a = new System.Windows.Forms.Label();
            this.lbl9 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl0 = new System.Windows.Forms.Label();
            this.btnCredits = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnMenu = new System.Windows.Forms.Button();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbUseLocalDbStore = new System.Windows.Forms.CheckBox();
            this.nMaxDisplayedDebugs = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.nMaxDisplayedPackets = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.gbFrames = new System.Windows.Forms.GroupBox();
            this.cbA8 = new System.Windows.Forms.CheckBox();
            this.cbA3 = new System.Windows.Forms.CheckBox();
            this.cb81 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.cb08 = new System.Windows.Forms.CheckBox();
            this.cb2A = new System.Windows.Forms.CheckBox();
            this.cb27 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.cb83 = new System.Windows.Forms.CheckBox();
            this.cbBE = new System.Windows.Forms.CheckBox();
            this.cbBD = new System.Windows.Forms.CheckBox();
            this.cbB2 = new System.Windows.Forms.CheckBox();
            this.cbB1 = new System.Windows.Forms.CheckBox();
            this.cbAB = new System.Windows.Forms.CheckBox();
            this.cbAA = new System.Windows.Forms.CheckBox();
            this.cb7D = new System.Windows.Forms.CheckBox();
            this.cb6C = new System.Windows.Forms.CheckBox();
            this.cb92 = new System.Windows.Forms.CheckBox();
            this.gbRepeater = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRawFrameUDPPort = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.label5 = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.udpConsumerTimer = new System.Windows.Forms.Timer(this.components);
            this.dbFlushToDiskTimer = new System.Windows.Forms.Timer(this.components);
            this.tabUni = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.pnlUniMap = new System.Windows.Forms.Panel();
            this.uniGMap = new GMap.NET.WindowsForms.GMapControl();
            this.label19 = new System.Windows.Forms.Label();
            this.splitterUniMap = new System.Windows.Forms.Splitter();
            this.uniMsgBox = new System.Windows.Forms.RichTextBox();
            this.pnlUniPriority = new System.Windows.Forms.Panel();
            this.lblUniServiceAddress = new System.Windows.Forms.Label();
            this.lblUniPriority = new System.Windows.Forms.Label();
            this.uniGridSplitter = new System.Windows.Forms.Splitter();
            this.panel14 = new System.Windows.Forms.Panel();
            this.dgvUni = new System.Windows.Forms.DataGridView();
            this.UniPriority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniEncoding = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mIdOrLCNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniRepetition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniPartsReceived = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniIsPartial = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.UniStation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniSatName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniSat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniStreamId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uniIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.streamIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniIsEGC = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.satDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.satNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lesIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lesNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeReceivedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.repetitionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.messageTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priorityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contentTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.partsReceivedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.messageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isPartialDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.bulletinBoardDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressHexDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.areaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unisBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.btnUniAutoScroll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnUniAutoLastSelect = new System.Windows.Forms.ToolStripButton();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.lblUniChannelTypeAndSat = new System.Windows.Forms.Label();
            this.lblUniStatus = new System.Windows.Forms.Label();
            this.lblUniServices = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.tabDebug = new System.Windows.Forms.TabPage();
            this.rtbDebug = new System.Windows.Forms.RichTextBox();
            this.tsBlocks = new System.Windows.Forms.ToolStrip();
            this.btnDebugClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDebugCopy = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDebugAutoScroll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnJson = new System.Windows.Forms.ToolStripButton();
            this.tabPackets = new System.Windows.Forms.TabPage();
            this.rtbPackets = new System.Windows.Forms.RichTextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnPacketsClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPacketsAutoScroll = new System.Windows.Forms.ToolStripButton();
            this.tC = new System.Windows.Forms.TabControl();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nMaxStoredHours = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelMenu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxDisplayedDebugs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxDisplayedPackets)).BeginInit();
            this.gbFrames.SuspendLayout();
            this.gbRepeater.SuspendLayout();
            this.tabUni.SuspendLayout();
            this.panel9.SuspendLayout();
            this.pnlUniMap.SuspendLayout();
            this.pnlUniPriority.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unisBindingSource)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.panel15.SuspendLayout();
            this.tabDebug.SuspendLayout();
            this.tsBlocks.SuspendLayout();
            this.tabPackets.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxStoredHours)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btnCredits);
            this.panel1.Controls.Add(this.btnStart);
            this.panel1.Controls.Add(this.btnMenu);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1143, 77);
            this.panel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.LightCyan;
            this.panel2.Controls.Add(this.lbl6a);
            this.panel2.Controls.Add(this.lbl3a);
            this.panel2.Controls.Add(this.lbl6);
            this.panel2.Controls.Add(this.lbl3);
            this.panel2.Controls.Add(this.lbl8a);
            this.panel2.Controls.Add(this.lbl5a);
            this.panel2.Controls.Add(this.lbl2a);
            this.panel2.Controls.Add(this.lbl8);
            this.panel2.Controls.Add(this.lbl5);
            this.panel2.Controls.Add(this.lbl2);
            this.panel2.Controls.Add(this.lbl9a);
            this.panel2.Controls.Add(this.lbl7a);
            this.panel2.Controls.Add(this.lbl4a);
            this.panel2.Controls.Add(this.lbl1a);
            this.panel2.Controls.Add(this.lbl0a);
            this.panel2.Controls.Add(this.lbl9);
            this.panel2.Controls.Add(this.lbl7);
            this.panel2.Controls.Add(this.lbl4);
            this.panel2.Controls.Add(this.lbl1);
            this.panel2.Controls.Add(this.lbl0);
            this.panel2.Location = new System.Drawing.Point(158, 3);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(1);
            this.panel2.Size = new System.Drawing.Size(827, 71);
            this.panel2.TabIndex = 12;
            // 
            // lbl6a
            // 
            this.lbl6a.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl6a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl6a.ForeColor = System.Drawing.Color.Navy;
            this.lbl6a.Location = new System.Drawing.Point(278, 54);
            this.lbl6a.Name = "lbl6a";
            this.lbl6a.Size = new System.Drawing.Size(91, 13);
            this.lbl6a.TabIndex = 19;
            this.lbl6a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl3a
            // 
            this.lbl3a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl3a.ForeColor = System.Drawing.Color.Navy;
            this.lbl3a.Location = new System.Drawing.Point(4, 54);
            this.lbl3a.Name = "lbl3a";
            this.lbl3a.Size = new System.Drawing.Size(91, 13);
            this.lbl3a.TabIndex = 18;
            this.lbl3a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl6
            // 
            this.lbl6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl6.AutoEllipsis = true;
            this.lbl6.BackColor = System.Drawing.SystemColors.Control;
            this.lbl6.Location = new System.Drawing.Point(371, 54);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(179, 13);
            this.lbl6.TabIndex = 17;
            this.lbl6.Tag = "0";
            // 
            // lbl3
            // 
            this.lbl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl3.AutoEllipsis = true;
            this.lbl3.BackColor = System.Drawing.SystemColors.Control;
            this.lbl3.Location = new System.Drawing.Point(97, 54);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(179, 13);
            this.lbl3.TabIndex = 16;
            this.lbl3.Tag = "0";
            // 
            // lbl8a
            // 
            this.lbl8a.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl8a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl8a.ForeColor = System.Drawing.Color.Navy;
            this.lbl8a.Location = new System.Drawing.Point(552, 37);
            this.lbl8a.Name = "lbl8a";
            this.lbl8a.Size = new System.Drawing.Size(91, 13);
            this.lbl8a.TabIndex = 15;
            this.lbl8a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl5a
            // 
            this.lbl5a.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl5a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl5a.ForeColor = System.Drawing.Color.Navy;
            this.lbl5a.Location = new System.Drawing.Point(278, 37);
            this.lbl5a.Name = "lbl5a";
            this.lbl5a.Size = new System.Drawing.Size(91, 13);
            this.lbl5a.TabIndex = 14;
            this.lbl5a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl2a
            // 
            this.lbl2a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl2a.ForeColor = System.Drawing.Color.Navy;
            this.lbl2a.Location = new System.Drawing.Point(4, 37);
            this.lbl2a.Name = "lbl2a";
            this.lbl2a.Size = new System.Drawing.Size(91, 13);
            this.lbl2a.TabIndex = 13;
            this.lbl2a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl8
            // 
            this.lbl8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl8.AutoEllipsis = true;
            this.lbl8.BackColor = System.Drawing.SystemColors.Control;
            this.lbl8.Location = new System.Drawing.Point(645, 37);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(178, 13);
            this.lbl8.TabIndex = 12;
            this.lbl8.Tag = "0";
            // 
            // lbl5
            // 
            this.lbl5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl5.AutoEllipsis = true;
            this.lbl5.BackColor = System.Drawing.SystemColors.Control;
            this.lbl5.Location = new System.Drawing.Point(371, 37);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(179, 13);
            this.lbl5.TabIndex = 11;
            this.lbl5.Tag = "0";
            // 
            // lbl2
            // 
            this.lbl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl2.AutoEllipsis = true;
            this.lbl2.BackColor = System.Drawing.SystemColors.Control;
            this.lbl2.Location = new System.Drawing.Point(97, 37);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(179, 13);
            this.lbl2.TabIndex = 10;
            this.lbl2.Tag = "0";
            // 
            // lbl9a
            // 
            this.lbl9a.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl9a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl9a.ForeColor = System.Drawing.Color.Navy;
            this.lbl9a.Location = new System.Drawing.Point(552, 54);
            this.lbl9a.Name = "lbl9a";
            this.lbl9a.Size = new System.Drawing.Size(91, 13);
            this.lbl9a.TabIndex = 9;
            this.lbl9a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl7a
            // 
            this.lbl7a.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl7a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl7a.ForeColor = System.Drawing.Color.Navy;
            this.lbl7a.Location = new System.Drawing.Point(552, 20);
            this.lbl7a.Name = "lbl7a";
            this.lbl7a.Size = new System.Drawing.Size(91, 13);
            this.lbl7a.TabIndex = 8;
            this.lbl7a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl4a
            // 
            this.lbl4a.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl4a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl4a.ForeColor = System.Drawing.Color.Navy;
            this.lbl4a.Location = new System.Drawing.Point(278, 20);
            this.lbl4a.Name = "lbl4a";
            this.lbl4a.Size = new System.Drawing.Size(91, 13);
            this.lbl4a.TabIndex = 7;
            this.lbl4a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl1a
            // 
            this.lbl1a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl1a.ForeColor = System.Drawing.Color.Navy;
            this.lbl1a.Location = new System.Drawing.Point(4, 20);
            this.lbl1a.Name = "lbl1a";
            this.lbl1a.Size = new System.Drawing.Size(91, 13);
            this.lbl1a.TabIndex = 6;
            this.lbl1a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl0a
            // 
            this.lbl0a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl0a.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lbl0a.ForeColor = System.Drawing.Color.Navy;
            this.lbl0a.Location = new System.Drawing.Point(4, 3);
            this.lbl0a.Name = "lbl0a";
            this.lbl0a.Size = new System.Drawing.Size(91, 13);
            this.lbl0a.TabIndex = 5;
            this.lbl0a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl9
            // 
            this.lbl9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl9.AutoEllipsis = true;
            this.lbl9.BackColor = System.Drawing.SystemColors.Control;
            this.lbl9.Location = new System.Drawing.Point(645, 54);
            this.lbl9.Name = "lbl9";
            this.lbl9.Size = new System.Drawing.Size(178, 13);
            this.lbl9.TabIndex = 4;
            this.lbl9.Tag = "0";
            // 
            // lbl7
            // 
            this.lbl7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl7.AutoEllipsis = true;
            this.lbl7.BackColor = System.Drawing.SystemColors.Control;
            this.lbl7.Location = new System.Drawing.Point(645, 20);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(178, 13);
            this.lbl7.TabIndex = 3;
            this.lbl7.Tag = "0";
            // 
            // lbl4
            // 
            this.lbl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl4.AutoEllipsis = true;
            this.lbl4.BackColor = System.Drawing.SystemColors.Control;
            this.lbl4.Location = new System.Drawing.Point(371, 20);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(179, 13);
            this.lbl4.TabIndex = 2;
            this.lbl4.Tag = "0";
            // 
            // lbl1
            // 
            this.lbl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl1.AutoEllipsis = true;
            this.lbl1.BackColor = System.Drawing.SystemColors.Control;
            this.lbl1.Location = new System.Drawing.Point(97, 20);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(179, 13);
            this.lbl1.TabIndex = 1;
            this.lbl1.Tag = "0";
            // 
            // lbl0
            // 
            this.lbl0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl0.AutoEllipsis = true;
            this.lbl0.BackColor = System.Drawing.SystemColors.Control;
            this.lbl0.Location = new System.Drawing.Point(97, 3);
            this.lbl0.Name = "lbl0";
            this.lbl0.Size = new System.Drawing.Size(726, 13);
            this.lbl0.TabIndex = 0;
            this.lbl0.Tag = "0";
            this.lbl0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCredits
            // 
            this.btnCredits.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCredits.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCredits.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCredits.FlatAppearance.BorderSize = 0;
            this.btnCredits.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCredits.Image = global::ScytaleC.QuickUI.Properties.Resources.if_note;
            this.btnCredits.Location = new System.Drawing.Point(1070, 12);
            this.btnCredits.Name = "btnCredits";
            this.btnCredits.Size = new System.Drawing.Size(61, 50);
            this.btnCredits.TabIndex = 11;
            this.btnCredits.TabStop = false;
            this.btnCredits.Tag = "";
            this.btnCredits.UseVisualStyleBackColor = false;
            this.btnCredits.Click += new System.EventHandler(this.btnCredits_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnStart.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.btnStart.FlatAppearance.BorderSize = 0;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Image = global::ScytaleC.QuickUI.Properties.Resources.button_blue_play;
            this.btnStart.Location = new System.Drawing.Point(68, 12);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(61, 50);
            this.btnStart.TabIndex = 10;
            this.btnStart.TabStop = false;
            this.btnStart.Tag = "start";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnMenu
            // 
            this.btnMenu.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnMenu.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.btnMenu.FlatAppearance.BorderSize = 0;
            this.btnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu.Image = global::ScytaleC.QuickUI.Properties.Resources.exchange_back;
            this.btnMenu.Location = new System.Drawing.Point(9, 12);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(61, 50);
            this.btnMenu.TabIndex = 0;
            this.btnMenu.TabStop = false;
            this.btnMenu.Tag = "menu-close";
            this.btnMenu.UseVisualStyleBackColor = false;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // panelMenu
            // 
            this.panelMenu.AutoScroll = true;
            this.panelMenu.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panelMenu.Controls.Add(this.groupBox1);
            this.panelMenu.Controls.Add(this.gbFrames);
            this.panelMenu.Controls.Add(this.gbRepeater);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 77);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Padding = new System.Windows.Forms.Padding(2);
            this.panelMenu.Size = new System.Drawing.Size(215, 557);
            this.panelMenu.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nMaxStoredHours);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbUseLocalDbStore);
            this.groupBox1.Controls.Add(this.nMaxDisplayedDebugs);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.nMaxDisplayedPackets);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(2, 594);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(194, 116);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // cbUseLocalDbStore
            // 
            this.cbUseLocalDbStore.AutoSize = true;
            this.cbUseLocalDbStore.Location = new System.Drawing.Point(15, 93);
            this.cbUseLocalDbStore.Name = "cbUseLocalDbStore";
            this.cbUseLocalDbStore.Size = new System.Drawing.Size(131, 17);
            this.cbUseLocalDbStore.TabIndex = 111;
            this.cbUseLocalDbStore.Text = "Use Local Db Storage";
            this.cbUseLocalDbStore.UseVisualStyleBackColor = true;
            // 
            // nMaxDisplayedDebugs
            // 
            this.nMaxDisplayedDebugs.Increment = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nMaxDisplayedDebugs.Location = new System.Drawing.Point(91, 42);
            this.nMaxDisplayedDebugs.Maximum = new decimal(new int[] {
            15000,
            0,
            0,
            0});
            this.nMaxDisplayedDebugs.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nMaxDisplayedDebugs.Name = "nMaxDisplayedDebugs";
            this.nMaxDisplayedDebugs.Size = new System.Drawing.Size(99, 20);
            this.nMaxDisplayedDebugs.TabIndex = 94;
            this.nMaxDisplayedDebugs.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 93;
            this.label3.Text = "Debug Lines:";
            // 
            // nMaxDisplayedPackets
            // 
            this.nMaxDisplayedPackets.Increment = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nMaxDisplayedPackets.Location = new System.Drawing.Point(91, 18);
            this.nMaxDisplayedPackets.Maximum = new decimal(new int[] {
            15000,
            0,
            0,
            0});
            this.nMaxDisplayedPackets.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nMaxDisplayedPackets.Name = "nMaxDisplayedPackets";
            this.nMaxDisplayedPackets.Size = new System.Drawing.Size(99, 20);
            this.nMaxDisplayedPackets.TabIndex = 92;
            this.nMaxDisplayedPackets.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 13);
            this.label9.TabIndex = 91;
            this.label9.Text = "Packet Lines:";
            // 
            // gbFrames
            // 
            this.gbFrames.Controls.Add(this.cbA8);
            this.gbFrames.Controls.Add(this.cbA3);
            this.gbFrames.Controls.Add(this.cb81);
            this.gbFrames.Controls.Add(this.checkBox9);
            this.gbFrames.Controls.Add(this.checkBox8);
            this.gbFrames.Controls.Add(this.checkBox7);
            this.gbFrames.Controls.Add(this.checkBox6);
            this.gbFrames.Controls.Add(this.cb08);
            this.gbFrames.Controls.Add(this.cb2A);
            this.gbFrames.Controls.Add(this.cb27);
            this.gbFrames.Controls.Add(this.checkBox2);
            this.gbFrames.Controls.Add(this.cb83);
            this.gbFrames.Controls.Add(this.cbBE);
            this.gbFrames.Controls.Add(this.cbBD);
            this.gbFrames.Controls.Add(this.cbB2);
            this.gbFrames.Controls.Add(this.cbB1);
            this.gbFrames.Controls.Add(this.cbAB);
            this.gbFrames.Controls.Add(this.cbAA);
            this.gbFrames.Controls.Add(this.cb7D);
            this.gbFrames.Controls.Add(this.cb6C);
            this.gbFrames.Controls.Add(this.cb92);
            this.gbFrames.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbFrames.Location = new System.Drawing.Point(2, 86);
            this.gbFrames.Name = "gbFrames";
            this.gbFrames.Size = new System.Drawing.Size(194, 508);
            this.gbFrames.TabIndex = 17;
            this.gbFrames.TabStop = false;
            this.gbFrames.Text = "Packet Decoding";
            // 
            // cbA8
            // 
            this.cbA8.AutoSize = true;
            this.cbA8.Checked = true;
            this.cbA8.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbA8.Location = new System.Drawing.Point(15, 484);
            this.cbA8.Name = "cbA8";
            this.cbA8.Size = new System.Drawing.Size(106, 17);
            this.cbA8.TabIndex = 110;
            this.cbA8.Text = "A8 - Confirmation";
            this.cbA8.UseVisualStyleBackColor = true;
            // 
            // cbA3
            // 
            this.cbA3.AutoSize = true;
            this.cbA3.Checked = true;
            this.cbA3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbA3.Location = new System.Drawing.Point(15, 461);
            this.cbA3.Name = "cbA3";
            this.cbA3.Size = new System.Drawing.Size(113, 17);
            this.cbA3.TabIndex = 109;
            this.cbA3.Text = "A3 - Individual Poll";
            this.cbA3.UseVisualStyleBackColor = true;
            // 
            // cb81
            // 
            this.cb81.AutoSize = true;
            this.cb81.Checked = true;
            this.cb81.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb81.Location = new System.Drawing.Point(15, 438);
            this.cb81.Name = "cb81";
            this.cb81.Size = new System.Drawing.Size(119, 17);
            this.cb81.TabIndex = 108;
            this.cb81.Text = "81 - Announcement";
            this.cb81.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Enabled = false;
            this.checkBox9.Location = new System.Drawing.Point(15, 415);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(149, 17);
            this.checkBox9.TabIndex = 107;
            this.checkBox9.Text = "AD - Distress Test Results";
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Enabled = false;
            this.checkBox8.Location = new System.Drawing.Point(15, 392);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(133, 17);
            this.checkBox8.TabIndex = 106;
            this.checkBox8.Text = "91 - Distress Alert Ack.";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Enabled = false;
            this.checkBox7.Location = new System.Drawing.Point(15, 369);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(152, 17);
            this.checkBox7.TabIndex = 105;
            this.checkBox7.Text = "A0 - Distress Test Request";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Enabled = false;
            this.checkBox6.Location = new System.Drawing.Point(15, 346);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(122, 17);
            this.checkBox6.TabIndex = 104;
            this.checkBox6.Text = "AC - Request Status";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // cb08
            // 
            this.cb08.AutoSize = true;
            this.cb08.Checked = true;
            this.cb08.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb08.Location = new System.Drawing.Point(15, 323);
            this.cb08.Name = "cb08";
            this.cb08.Size = new System.Drawing.Size(112, 17);
            this.cb08.TabIndex = 103;
            this.cb08.Text = "08 - Ack. Request";
            this.cb08.UseVisualStyleBackColor = true;
            // 
            // cb2A
            // 
            this.cb2A.AutoSize = true;
            this.cb2A.Checked = true;
            this.cb2A.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb2A.Location = new System.Drawing.Point(15, 300);
            this.cb2A.Name = "cb2A";
            this.cb2A.Size = new System.Drawing.Size(158, 17);
            this.cb2A.TabIndex = 102;
            this.cb2A.Text = "2A - Inbound Message Ack.";
            this.cb2A.UseVisualStyleBackColor = true;
            // 
            // cb27
            // 
            this.cb27.AutoSize = true;
            this.cb27.Checked = true;
            this.cb27.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb27.Location = new System.Drawing.Point(15, 277);
            this.cb27.Name = "cb27";
            this.cb27.Size = new System.Drawing.Size(150, 17);
            this.cb27.TabIndex = 101;
            this.cb27.Text = "27 - Logical Channel Clear";
            this.cb27.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Enabled = false;
            this.checkBox2.Location = new System.Drawing.Point(15, 254);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(183, 17);
            this.checkBox2.TabIndex = 100;
            this.checkBox2.Text = "9A - Enhanced Data Report Ack.";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // cb83
            // 
            this.cb83.AutoSize = true;
            this.cb83.Checked = true;
            this.cb83.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb83.Location = new System.Drawing.Point(15, 231);
            this.cb83.Name = "cb83";
            this.cb83.Size = new System.Drawing.Size(180, 17);
            this.cb83.TabIndex = 99;
            this.cb83.Text = "83 - Logical Channel Assignment";
            this.cb83.UseVisualStyleBackColor = true;
            // 
            // cbBE
            // 
            this.cbBE.AutoSize = true;
            this.cbBE.Checked = true;
            this.cbBE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbBE.Location = new System.Drawing.Point(15, 208);
            this.cbBE.Name = "cbBE";
            this.cbBE.Size = new System.Drawing.Size(171, 17);
            this.cbBE.TabIndex = 98;
            this.cbBE.Text = "BE - Multiframe Message Cntd.";
            this.cbBE.UseVisualStyleBackColor = true;
            // 
            // cbBD
            // 
            this.cbBD.AutoSize = true;
            this.cbBD.Checked = true;
            this.cbBD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbBD.Location = new System.Drawing.Point(15, 185);
            this.cbBD.Name = "cbBD";
            this.cbBD.Size = new System.Drawing.Size(144, 17);
            this.cbBD.TabIndex = 97;
            this.cbBD.Text = "BD - Multiframe Message";
            this.cbBD.UseVisualStyleBackColor = true;
            // 
            // cbB2
            // 
            this.cbB2.AutoSize = true;
            this.cbB2.Checked = true;
            this.cbB2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbB2.Location = new System.Drawing.Point(15, 162);
            this.cbB2.Name = "cbB2";
            this.cbB2.Size = new System.Drawing.Size(70, 17);
            this.cbB2.TabIndex = 96;
            this.cbB2.Text = "B2 - EGC";
            this.cbB2.UseVisualStyleBackColor = true;
            // 
            // cbB1
            // 
            this.cbB1.AutoSize = true;
            this.cbB1.Checked = true;
            this.cbB1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbB1.Location = new System.Drawing.Point(15, 139);
            this.cbB1.Name = "cbB1";
            this.cbB1.Size = new System.Drawing.Size(70, 17);
            this.cbB1.TabIndex = 95;
            this.cbB1.Text = "B1 - EGC";
            this.cbB1.UseVisualStyleBackColor = true;
            // 
            // cbAB
            // 
            this.cbAB.AutoSize = true;
            this.cbAB.Checked = true;
            this.cbAB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAB.Location = new System.Drawing.Point(15, 116);
            this.cbAB.Name = "cbAB";
            this.cbAB.Size = new System.Drawing.Size(88, 17);
            this.cbAB.TabIndex = 94;
            this.cbAB.Text = "AB - LES List";
            this.cbAB.UseVisualStyleBackColor = true;
            // 
            // cbAA
            // 
            this.cbAA.AutoSize = true;
            this.cbAA.Checked = true;
            this.cbAA.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAA.Location = new System.Drawing.Point(15, 93);
            this.cbAA.Name = "cbAA";
            this.cbAA.Size = new System.Drawing.Size(92, 17);
            this.cbAA.TabIndex = 93;
            this.cbAA.Text = "AA - Message";
            this.cbAA.UseVisualStyleBackColor = true;
            // 
            // cb7D
            // 
            this.cb7D.AutoSize = true;
            this.cb7D.Checked = true;
            this.cb7D.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb7D.Location = new System.Drawing.Point(15, 47);
            this.cb7D.Name = "cb7D";
            this.cb7D.Size = new System.Drawing.Size(114, 17);
            this.cb7D.TabIndex = 92;
            this.cb7D.Text = "7D - Bulletin Board";
            this.cb7D.UseVisualStyleBackColor = true;
            // 
            // cb6C
            // 
            this.cb6C.AutoSize = true;
            this.cb6C.Checked = true;
            this.cb6C.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb6C.Location = new System.Drawing.Point(15, 24);
            this.cb6C.Name = "cb6C";
            this.cb6C.Size = new System.Drawing.Size(135, 17);
            this.cb6C.TabIndex = 91;
            this.cb6C.Text = "6C - Signalling Channel";
            this.cb6C.UseVisualStyleBackColor = true;
            // 
            // cb92
            // 
            this.cb92.AutoSize = true;
            this.cb92.Checked = true;
            this.cb92.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb92.Location = new System.Drawing.Point(15, 70);
            this.cb92.Name = "cb92";
            this.cb92.Size = new System.Drawing.Size(164, 17);
            this.cb92.TabIndex = 90;
            this.cb92.Text = "92 - Login Acknowledgement";
            this.cb92.UseVisualStyleBackColor = true;
            // 
            // gbRepeater
            // 
            this.gbRepeater.Controls.Add(this.label1);
            this.gbRepeater.Controls.Add(this.txtRawFrameUDPPort);
            this.gbRepeater.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbRepeater.Location = new System.Drawing.Point(2, 2);
            this.gbRepeater.Name = "gbRepeater";
            this.gbRepeater.Size = new System.Drawing.Size(194, 84);
            this.gbRepeater.TabIndex = 1;
            this.gbRepeater.TabStop = false;
            this.gbRepeater.Text = "Sources";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 13);
            this.label1.TabIndex = 93;
            this.label1.Text = "UDP Ports (comma delimited):";
            // 
            // txtRawFrameUDPPort
            // 
            this.txtRawFrameUDPPort.Location = new System.Drawing.Point(15, 43);
            this.txtRawFrameUDPPort.Multiline = true;
            this.txtRawFrameUDPPort.Name = "txtRawFrameUDPPort";
            this.txtRawFrameUDPPort.Size = new System.Drawing.Size(171, 33);
            this.txtRawFrameUDPPort.TabIndex = 91;
            this.txtRawFrameUDPPort.Text = "15003, 15004";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 634);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1143, 22);
            this.statusStrip1.TabIndex = 68;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label5.Location = new System.Drawing.Point(968, 638);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(163, 13);
            this.label5.TabIndex = 69;
            this.label5.Text = "Open source Inmarsat-C decoder";
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.ForeColor = System.Drawing.Color.DeepPink;
            this.lblVersion.Location = new System.Drawing.Point(722, 638);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(240, 13);
            this.lblVersion.TabIndex = 70;
            this.lblVersion.Text = "BETA";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // udpConsumerTimer
            // 
            this.udpConsumerTimer.Enabled = true;
            this.udpConsumerTimer.Tick += new System.EventHandler(this.udpConsumerTimer_Tick);
            // 
            // dbFlushToDiskTimer
            // 
            this.dbFlushToDiskTimer.Enabled = true;
            this.dbFlushToDiskTimer.Interval = 600000;
            this.dbFlushToDiskTimer.Tick += new System.EventHandler(this.dbFlushToDiskTimer_Tick);
            // 
            // tabUni
            // 
            this.tabUni.Controls.Add(this.panel9);
            this.tabUni.Controls.Add(this.uniGridSplitter);
            this.tabUni.Controls.Add(this.panel14);
            this.tabUni.Controls.Add(this.panel15);
            this.tabUni.Location = new System.Drawing.Point(4, 22);
            this.tabUni.Name = "tabUni";
            this.tabUni.Size = new System.Drawing.Size(920, 531);
            this.tabUni.TabIndex = 21;
            this.tabUni.Text = "EGCs and Messages";
            this.tabUni.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.pnlUniMap);
            this.panel9.Controls.Add(this.splitterUniMap);
            this.panel9.Controls.Add(this.uniMsgBox);
            this.panel9.Controls.Add(this.pnlUniPriority);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(632, 67);
            this.panel9.Name = "panel9";
            this.panel9.Padding = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.panel9.Size = new System.Drawing.Size(288, 464);
            this.panel9.TabIndex = 3;
            // 
            // pnlUniMap
            // 
            this.pnlUniMap.BackColor = System.Drawing.Color.White;
            this.pnlUniMap.Controls.Add(this.uniGMap);
            this.pnlUniMap.Controls.Add(this.label19);
            this.pnlUniMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUniMap.Location = new System.Drawing.Point(1, 283);
            this.pnlUniMap.Name = "pnlUniMap";
            this.pnlUniMap.Padding = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.pnlUniMap.Size = new System.Drawing.Size(286, 181);
            this.pnlUniMap.TabIndex = 71;
            this.pnlUniMap.Visible = false;
            // 
            // uniGMap
            // 
            this.uniGMap.BackColor = System.Drawing.SystemColors.Control;
            this.uniGMap.Bearing = 0F;
            this.uniGMap.CanDragMap = true;
            this.uniGMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uniGMap.EmptyTileColor = System.Drawing.Color.Navy;
            this.uniGMap.GrayScaleMode = false;
            this.uniGMap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.uniGMap.LevelsKeepInMemmory = 5;
            this.uniGMap.Location = new System.Drawing.Point(0, 1);
            this.uniGMap.MarkersEnabled = true;
            this.uniGMap.MaxZoom = 18;
            this.uniGMap.MinZoom = 2;
            this.uniGMap.MouseWheelZoomEnabled = true;
            this.uniGMap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.uniGMap.Name = "uniGMap";
            this.uniGMap.NegativeMode = false;
            this.uniGMap.PolygonsEnabled = true;
            this.uniGMap.RetryLoadTile = 0;
            this.uniGMap.RoutesEnabled = true;
            this.uniGMap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.uniGMap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.uniGMap.ShowTileGridLines = false;
            this.uniGMap.Size = new System.Drawing.Size(286, 180);
            this.uniGMap.TabIndex = 1;
            this.uniGMap.Zoom = 8D;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 14);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Chart";
            // 
            // splitterUniMap
            // 
            this.splitterUniMap.BackColor = System.Drawing.SystemColors.Control;
            this.splitterUniMap.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.splitterUniMap.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterUniMap.Location = new System.Drawing.Point(1, 278);
            this.splitterUniMap.Name = "splitterUniMap";
            this.splitterUniMap.Size = new System.Drawing.Size(286, 5);
            this.splitterUniMap.TabIndex = 69;
            this.splitterUniMap.TabStop = false;
            // 
            // uniMsgBox
            // 
            this.uniMsgBox.BackColor = System.Drawing.SystemColors.Window;
            this.uniMsgBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.uniMsgBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.uniMsgBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uniMsgBox.ForeColor = System.Drawing.Color.Navy;
            this.uniMsgBox.Location = new System.Drawing.Point(1, 47);
            this.uniMsgBox.Name = "uniMsgBox";
            this.uniMsgBox.ReadOnly = true;
            this.uniMsgBox.Size = new System.Drawing.Size(286, 231);
            this.uniMsgBox.TabIndex = 68;
            this.uniMsgBox.Text = "";
            // 
            // pnlUniPriority
            // 
            this.pnlUniPriority.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pnlUniPriority.Controls.Add(this.lblUniServiceAddress);
            this.pnlUniPriority.Controls.Add(this.lblUniPriority);
            this.pnlUniPriority.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlUniPriority.Location = new System.Drawing.Point(1, 1);
            this.pnlUniPriority.Name = "pnlUniPriority";
            this.pnlUniPriority.Size = new System.Drawing.Size(286, 46);
            this.pnlUniPriority.TabIndex = 70;
            // 
            // lblUniServiceAddress
            // 
            this.lblUniServiceAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUniServiceAddress.AutoEllipsis = true;
            this.lblUniServiceAddress.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lblUniServiceAddress.ForeColor = System.Drawing.Color.Navy;
            this.lblUniServiceAddress.Location = new System.Drawing.Point(55, 10);
            this.lblUniServiceAddress.Name = "lblUniServiceAddress";
            this.lblUniServiceAddress.Size = new System.Drawing.Size(224, 27);
            this.lblUniServiceAddress.TabIndex = 13;
            this.lblUniServiceAddress.Text = "Service and address";
            // 
            // lblUniPriority
            // 
            this.lblUniPriority.Location = new System.Drawing.Point(3, 10);
            this.lblUniPriority.Name = "lblUniPriority";
            this.lblUniPriority.Size = new System.Drawing.Size(56, 13);
            this.lblUniPriority.TabIndex = 14;
            this.lblUniPriority.Text = "Priority:";
            this.lblUniPriority.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // uniGridSplitter
            // 
            this.uniGridSplitter.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.uniGridSplitter.Location = new System.Drawing.Point(627, 67);
            this.uniGridSplitter.Name = "uniGridSplitter";
            this.uniGridSplitter.Size = new System.Drawing.Size(5, 464);
            this.uniGridSplitter.TabIndex = 2;
            this.uniGridSplitter.TabStop = false;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel14.Controls.Add(this.dgvUni);
            this.panel14.Controls.Add(this.toolStrip2);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 67);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(627, 464);
            this.panel14.TabIndex = 0;
            // 
            // dgvUni
            // 
            this.dgvUni.AllowUserToAddRows = false;
            this.dgvUni.AllowUserToDeleteRows = false;
            this.dgvUni.AllowUserToOrderColumns = true;
            this.dgvUni.AllowUserToResizeRows = false;
            this.dgvUni.AutoGenerateColumns = false;
            this.dgvUni.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvUni.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvUni.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvUni.ColumnHeadersHeight = 30;
            this.dgvUni.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvUni.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UniPriority,
            this.UniEncoding,
            this.UniTime,
            this.mIdOrLCNoDataGridViewTextBoxColumn,
            this.UniRepetition,
            this.UniPartsReceived,
            this.UniIsPartial,
            this.UniStation,
            this.UniSatName,
            this.UniSat,
            this.UniStreamId,
            this.uniIdDataGridViewTextBoxColumn,
            this.streamIdDataGridViewTextBoxColumn,
            this.UniIsEGC,
            this.satDataGridViewTextBoxColumn,
            this.satNameDataGridViewTextBoxColumn,
            this.lesIdDataGridViewTextBoxColumn,
            this.lesNameDataGridViewTextBoxColumn,
            this.timeDataGridViewTextBoxColumn,
            this.timeReceivedDataGridViewTextBoxColumn,
            this.repetitionDataGridViewTextBoxColumn,
            this.messageTypeDataGridViewTextBoxColumn,
            this.priorityDataGridViewTextBoxColumn,
            this.contentTypeDataGridViewTextBoxColumn,
            this.partsReceivedDataGridViewTextBoxColumn,
            this.messageDataGridViewTextBoxColumn,
            this.isPartialDataGridViewCheckBoxColumn,
            this.bulletinBoardDataGridViewTextBoxColumn,
            this.addressHexDataGridViewTextBoxColumn,
            this.areaDataGridViewTextBoxColumn});
            this.dgvUni.DataSource = this.unisBindingSource;
            this.dgvUni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvUni.Location = new System.Drawing.Point(0, 0);
            this.dgvUni.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.dgvUni.MultiSelect = false;
            this.dgvUni.Name = "dgvUni";
            this.dgvUni.ReadOnly = true;
            this.dgvUni.RowHeadersVisible = false;
            this.dgvUni.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvUni.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUni.Size = new System.Drawing.Size(627, 438);
            this.dgvUni.TabIndex = 0;
            this.dgvUni.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUni_CellClick);
            this.dgvUni.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvUni_CellFormatting);
            this.dgvUni.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvUni_CellPainting);
            this.dgvUni.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUni_RowEnter);
            this.dgvUni.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvUni_KeyDown);
            // 
            // UniPriority
            // 
            this.UniPriority.DataPropertyName = "Priority";
            this.UniPriority.HeaderText = "";
            this.UniPriority.Name = "UniPriority";
            this.UniPriority.ReadOnly = true;
            this.UniPriority.ToolTipText = "Priority";
            this.UniPriority.Width = 13;
            // 
            // UniEncoding
            // 
            this.UniEncoding.DataPropertyName = "ContentType";
            this.UniEncoding.HeaderText = "";
            this.UniEncoding.Name = "UniEncoding";
            this.UniEncoding.ReadOnly = true;
            this.UniEncoding.ToolTipText = "Encoding";
            this.UniEncoding.Width = 13;
            // 
            // UniTime
            // 
            this.UniTime.DataPropertyName = "Time";
            this.UniTime.HeaderText = "Local Time";
            this.UniTime.Name = "UniTime";
            this.UniTime.ReadOnly = true;
            this.UniTime.Width = 120;
            // 
            // mIdOrLCNoDataGridViewTextBoxColumn
            // 
            this.mIdOrLCNoDataGridViewTextBoxColumn.DataPropertyName = "MIdOrLCNo";
            this.mIdOrLCNoDataGridViewTextBoxColumn.HeaderText = "EGC/LCN";
            this.mIdOrLCNoDataGridViewTextBoxColumn.Name = "mIdOrLCNoDataGridViewTextBoxColumn";
            this.mIdOrLCNoDataGridViewTextBoxColumn.ReadOnly = true;
            this.mIdOrLCNoDataGridViewTextBoxColumn.ToolTipText = "EGC / Logical Channel Number";
            this.mIdOrLCNoDataGridViewTextBoxColumn.Width = 60;
            // 
            // UniRepetition
            // 
            this.UniRepetition.DataPropertyName = "Repetition";
            this.UniRepetition.HeaderText = "REP";
            this.UniRepetition.Name = "UniRepetition";
            this.UniRepetition.ReadOnly = true;
            this.UniRepetition.ToolTipText = "Repetition";
            this.UniRepetition.Width = 40;
            // 
            // UniPartsReceived
            // 
            this.UniPartsReceived.DataPropertyName = "PartsReceived";
            this.UniPartsReceived.HeaderText = "PTS";
            this.UniPartsReceived.Name = "UniPartsReceived";
            this.UniPartsReceived.ReadOnly = true;
            this.UniPartsReceived.ToolTipText = "Parts";
            this.UniPartsReceived.Width = 40;
            // 
            // UniIsPartial
            // 
            this.UniIsPartial.DataPropertyName = "IsPartial";
            this.UniIsPartial.FalseValue = "";
            this.UniIsPartial.HeaderText = "Partial";
            this.UniIsPartial.IndeterminateValue = "";
            this.UniIsPartial.Name = "UniIsPartial";
            this.UniIsPartial.ReadOnly = true;
            this.UniIsPartial.ToolTipText = "If checked, this message is either incomplete or still being received";
            this.UniIsPartial.TrueValue = "";
            this.UniIsPartial.Width = 42;
            // 
            // UniStation
            // 
            this.UniStation.DataPropertyName = "LesName";
            this.UniStation.HeaderText = "Station";
            this.UniStation.Name = "UniStation";
            this.UniStation.ReadOnly = true;
            this.UniStation.Width = 110;
            // 
            // UniSatName
            // 
            this.UniSatName.DataPropertyName = "SatName";
            this.UniSatName.HeaderText = "Sat";
            this.UniSatName.Name = "UniSatName";
            this.UniSatName.ReadOnly = true;
            this.UniSatName.Width = 105;
            // 
            // UniSat
            // 
            this.UniSat.DataPropertyName = "Sat";
            this.UniSat.HeaderText = "Sat";
            this.UniSat.Name = "UniSat";
            this.UniSat.ReadOnly = true;
            this.UniSat.Visible = false;
            // 
            // UniStreamId
            // 
            this.UniStreamId.DataPropertyName = "StreamId";
            this.UniStreamId.HeaderText = "SId";
            this.UniStreamId.Name = "UniStreamId";
            this.UniStreamId.ReadOnly = true;
            this.UniStreamId.Width = 60;
            // 
            // uniIdDataGridViewTextBoxColumn
            // 
            this.uniIdDataGridViewTextBoxColumn.DataPropertyName = "UniId";
            this.uniIdDataGridViewTextBoxColumn.HeaderText = "UniId";
            this.uniIdDataGridViewTextBoxColumn.Name = "uniIdDataGridViewTextBoxColumn";
            this.uniIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.uniIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // streamIdDataGridViewTextBoxColumn
            // 
            this.streamIdDataGridViewTextBoxColumn.DataPropertyName = "StreamId";
            this.streamIdDataGridViewTextBoxColumn.HeaderText = "StreamId";
            this.streamIdDataGridViewTextBoxColumn.Name = "streamIdDataGridViewTextBoxColumn";
            this.streamIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.streamIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // UniIsEGC
            // 
            this.UniIsEGC.DataPropertyName = "IsEGC";
            this.UniIsEGC.HeaderText = "IsEGC";
            this.UniIsEGC.Name = "UniIsEGC";
            this.UniIsEGC.ReadOnly = true;
            this.UniIsEGC.Visible = false;
            // 
            // satDataGridViewTextBoxColumn
            // 
            this.satDataGridViewTextBoxColumn.DataPropertyName = "Sat";
            this.satDataGridViewTextBoxColumn.HeaderText = "Sat";
            this.satDataGridViewTextBoxColumn.Name = "satDataGridViewTextBoxColumn";
            this.satDataGridViewTextBoxColumn.ReadOnly = true;
            this.satDataGridViewTextBoxColumn.Visible = false;
            // 
            // satNameDataGridViewTextBoxColumn
            // 
            this.satNameDataGridViewTextBoxColumn.DataPropertyName = "SatName";
            this.satNameDataGridViewTextBoxColumn.HeaderText = "SatName";
            this.satNameDataGridViewTextBoxColumn.Name = "satNameDataGridViewTextBoxColumn";
            this.satNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.satNameDataGridViewTextBoxColumn.Visible = false;
            // 
            // lesIdDataGridViewTextBoxColumn
            // 
            this.lesIdDataGridViewTextBoxColumn.DataPropertyName = "LesId";
            this.lesIdDataGridViewTextBoxColumn.HeaderText = "LesId";
            this.lesIdDataGridViewTextBoxColumn.Name = "lesIdDataGridViewTextBoxColumn";
            this.lesIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.lesIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // lesNameDataGridViewTextBoxColumn
            // 
            this.lesNameDataGridViewTextBoxColumn.DataPropertyName = "LesName";
            this.lesNameDataGridViewTextBoxColumn.HeaderText = "LesName";
            this.lesNameDataGridViewTextBoxColumn.Name = "lesNameDataGridViewTextBoxColumn";
            this.lesNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.lesNameDataGridViewTextBoxColumn.Visible = false;
            // 
            // timeDataGridViewTextBoxColumn
            // 
            this.timeDataGridViewTextBoxColumn.DataPropertyName = "Time";
            this.timeDataGridViewTextBoxColumn.HeaderText = "Time";
            this.timeDataGridViewTextBoxColumn.Name = "timeDataGridViewTextBoxColumn";
            this.timeDataGridViewTextBoxColumn.ReadOnly = true;
            this.timeDataGridViewTextBoxColumn.Visible = false;
            // 
            // timeReceivedDataGridViewTextBoxColumn
            // 
            this.timeReceivedDataGridViewTextBoxColumn.DataPropertyName = "TimeReceived";
            this.timeReceivedDataGridViewTextBoxColumn.HeaderText = "TimeReceived";
            this.timeReceivedDataGridViewTextBoxColumn.Name = "timeReceivedDataGridViewTextBoxColumn";
            this.timeReceivedDataGridViewTextBoxColumn.ReadOnly = true;
            this.timeReceivedDataGridViewTextBoxColumn.Visible = false;
            // 
            // repetitionDataGridViewTextBoxColumn
            // 
            this.repetitionDataGridViewTextBoxColumn.DataPropertyName = "Repetition";
            this.repetitionDataGridViewTextBoxColumn.HeaderText = "Repetition";
            this.repetitionDataGridViewTextBoxColumn.Name = "repetitionDataGridViewTextBoxColumn";
            this.repetitionDataGridViewTextBoxColumn.ReadOnly = true;
            this.repetitionDataGridViewTextBoxColumn.Visible = false;
            // 
            // messageTypeDataGridViewTextBoxColumn
            // 
            this.messageTypeDataGridViewTextBoxColumn.DataPropertyName = "MessageType";
            this.messageTypeDataGridViewTextBoxColumn.HeaderText = "MessageType";
            this.messageTypeDataGridViewTextBoxColumn.Name = "messageTypeDataGridViewTextBoxColumn";
            this.messageTypeDataGridViewTextBoxColumn.ReadOnly = true;
            this.messageTypeDataGridViewTextBoxColumn.Visible = false;
            // 
            // priorityDataGridViewTextBoxColumn
            // 
            this.priorityDataGridViewTextBoxColumn.DataPropertyName = "Priority";
            this.priorityDataGridViewTextBoxColumn.HeaderText = "Priority";
            this.priorityDataGridViewTextBoxColumn.Name = "priorityDataGridViewTextBoxColumn";
            this.priorityDataGridViewTextBoxColumn.ReadOnly = true;
            this.priorityDataGridViewTextBoxColumn.Visible = false;
            // 
            // contentTypeDataGridViewTextBoxColumn
            // 
            this.contentTypeDataGridViewTextBoxColumn.DataPropertyName = "ContentType";
            this.contentTypeDataGridViewTextBoxColumn.HeaderText = "ContentType";
            this.contentTypeDataGridViewTextBoxColumn.Name = "contentTypeDataGridViewTextBoxColumn";
            this.contentTypeDataGridViewTextBoxColumn.ReadOnly = true;
            this.contentTypeDataGridViewTextBoxColumn.Visible = false;
            // 
            // partsReceivedDataGridViewTextBoxColumn
            // 
            this.partsReceivedDataGridViewTextBoxColumn.DataPropertyName = "PartsReceived";
            this.partsReceivedDataGridViewTextBoxColumn.HeaderText = "PartsReceived";
            this.partsReceivedDataGridViewTextBoxColumn.Name = "partsReceivedDataGridViewTextBoxColumn";
            this.partsReceivedDataGridViewTextBoxColumn.ReadOnly = true;
            this.partsReceivedDataGridViewTextBoxColumn.Visible = false;
            // 
            // messageDataGridViewTextBoxColumn
            // 
            this.messageDataGridViewTextBoxColumn.DataPropertyName = "Message";
            this.messageDataGridViewTextBoxColumn.HeaderText = "Message";
            this.messageDataGridViewTextBoxColumn.Name = "messageDataGridViewTextBoxColumn";
            this.messageDataGridViewTextBoxColumn.ReadOnly = true;
            this.messageDataGridViewTextBoxColumn.Visible = false;
            // 
            // isPartialDataGridViewCheckBoxColumn
            // 
            this.isPartialDataGridViewCheckBoxColumn.DataPropertyName = "IsPartial";
            this.isPartialDataGridViewCheckBoxColumn.HeaderText = "IsPartial";
            this.isPartialDataGridViewCheckBoxColumn.Name = "isPartialDataGridViewCheckBoxColumn";
            this.isPartialDataGridViewCheckBoxColumn.ReadOnly = true;
            this.isPartialDataGridViewCheckBoxColumn.Visible = false;
            // 
            // bulletinBoardDataGridViewTextBoxColumn
            // 
            this.bulletinBoardDataGridViewTextBoxColumn.DataPropertyName = "BulletinBoard";
            this.bulletinBoardDataGridViewTextBoxColumn.HeaderText = "BulletinBoard";
            this.bulletinBoardDataGridViewTextBoxColumn.Name = "bulletinBoardDataGridViewTextBoxColumn";
            this.bulletinBoardDataGridViewTextBoxColumn.ReadOnly = true;
            this.bulletinBoardDataGridViewTextBoxColumn.Visible = false;
            // 
            // addressHexDataGridViewTextBoxColumn
            // 
            this.addressHexDataGridViewTextBoxColumn.DataPropertyName = "AddressHex";
            this.addressHexDataGridViewTextBoxColumn.HeaderText = "AddressHex";
            this.addressHexDataGridViewTextBoxColumn.Name = "addressHexDataGridViewTextBoxColumn";
            this.addressHexDataGridViewTextBoxColumn.ReadOnly = true;
            this.addressHexDataGridViewTextBoxColumn.Visible = false;
            // 
            // areaDataGridViewTextBoxColumn
            // 
            this.areaDataGridViewTextBoxColumn.DataPropertyName = "Area";
            this.areaDataGridViewTextBoxColumn.HeaderText = "Area";
            this.areaDataGridViewTextBoxColumn.Name = "areaDataGridViewTextBoxColumn";
            this.areaDataGridViewTextBoxColumn.ReadOnly = true;
            this.areaDataGridViewTextBoxColumn.Visible = false;
            // 
            // unisBindingSource
            // 
            this.unisBindingSource.DataSource = typeof(ScytaleC.QuickUI.Uni);
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip2.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnUniAutoScroll,
            this.toolStripSeparator1,
            this.btnUniAutoLastSelect});
            this.toolStrip2.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip2.Location = new System.Drawing.Point(0, 438);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Padding = new System.Windows.Forms.Padding(0, 2, 0, 1);
            this.toolStrip2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolStrip2.Size = new System.Drawing.Size(627, 26);
            this.toolStrip2.TabIndex = 75;
            this.toolStrip2.Text = "toolStrip1";
            // 
            // btnUniAutoScroll
            // 
            this.btnUniAutoScroll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnUniAutoScroll.Image = ((System.Drawing.Image)(resources.GetObject("btnUniAutoScroll.Image")));
            this.btnUniAutoScroll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUniAutoScroll.Margin = new System.Windows.Forms.Padding(0, 1, 50, 2);
            this.btnUniAutoScroll.Name = "btnUniAutoScroll";
            this.btnUniAutoScroll.Size = new System.Drawing.Size(51, 20);
            this.btnUniAutoScroll.Text = "Auto Scroll";
            this.btnUniAutoScroll.Click += new System.EventHandler(this.btnUniAutoScroll_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 23);
            // 
            // btnUniAutoLastSelect
            // 
            this.btnUniAutoLastSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnUniAutoLastSelect.Enabled = false;
            this.btnUniAutoLastSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnUniAutoLastSelect.Image")));
            this.btnUniAutoLastSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUniAutoLastSelect.Name = "btnUniAutoLastSelect";
            this.btnUniAutoLastSelect.Size = new System.Drawing.Size(72, 20);
            this.btnUniAutoLastSelect.Text = "Auto Last Select";
            this.btnUniAutoLastSelect.Click += new System.EventHandler(this.toolStripButton_Click);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel15.Controls.Add(this.label44);
            this.panel15.Controls.Add(this.label43);
            this.panel15.Controls.Add(this.label45);
            this.panel15.Controls.Add(this.label46);
            this.panel15.Controls.Add(this.label47);
            this.panel15.Controls.Add(this.label48);
            this.panel15.Controls.Add(this.label49);
            this.panel15.Controls.Add(this.label50);
            this.panel15.Controls.Add(this.label35);
            this.panel15.Controls.Add(this.label36);
            this.panel15.Controls.Add(this.label37);
            this.panel15.Controls.Add(this.label38);
            this.panel15.Controls.Add(this.label39);
            this.panel15.Controls.Add(this.label40);
            this.panel15.Controls.Add(this.label41);
            this.panel15.Controls.Add(this.label42);
            this.panel15.Controls.Add(this.label34);
            this.panel15.Controls.Add(this.lblUniChannelTypeAndSat);
            this.panel15.Controls.Add(this.lblUniStatus);
            this.panel15.Controls.Add(this.lblUniServices);
            this.panel15.Controls.Add(this.label26);
            this.panel15.Controls.Add(this.label32);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(920, 67);
            this.panel15.TabIndex = 0;
            // 
            // label44
            // 
            this.label44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label44.BackColor = System.Drawing.Color.Plum;
            this.label44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label44.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label44.Location = new System.Drawing.Point(853, 20);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(13, 13);
            this.label44.TabIndex = 45;
            // 
            // label43
            // 
            this.label43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(868, 20);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(23, 13);
            this.label43.TabIndex = 46;
            this.label43.Text = "IA5";
            // 
            // label45
            // 
            this.label45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(868, 6);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(36, 13);
            this.label45.TabIndex = 40;
            this.label45.Text = "Binary";
            // 
            // label46
            // 
            this.label46.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(868, 48);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(26, 13);
            this.label46.TabIndex = 44;
            this.label46.Text = "Zap";
            // 
            // label47
            // 
            this.label47.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label47.BackColor = System.Drawing.Color.LightSalmon;
            this.label47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label47.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label47.Location = new System.Drawing.Point(853, 6);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(13, 13);
            this.label47.TabIndex = 39;
            // 
            // label48
            // 
            this.label48.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label48.BackColor = System.Drawing.Color.LightGray;
            this.label48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label48.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label48.Location = new System.Drawing.Point(853, 34);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(13, 13);
            this.label48.TabIndex = 41;
            // 
            // label49
            // 
            this.label49.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label49.BackColor = System.Drawing.Color.White;
            this.label49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label49.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label49.Location = new System.Drawing.Point(853, 48);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(13, 13);
            this.label49.TabIndex = 43;
            // 
            // label50
            // 
            this.label50.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(868, 34);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(30, 13);
            this.label50.TabIndex = 42;
            this.label50.Text = "ITA2";
            // 
            // label35
            // 
            this.label35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(799, 6);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(44, 13);
            this.label35.TabIndex = 1;
            this.label35.Text = "Distress";
            // 
            // label36
            // 
            this.label36.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(799, 48);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(44, 13);
            this.label36.TabIndex = 7;
            this.label36.Text = "Routine";
            // 
            // label37
            // 
            this.label37.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label37.BackColor = System.Drawing.Color.Red;
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label37.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label37.Location = new System.Drawing.Point(784, 6);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(13, 13);
            this.label37.TabIndex = 0;
            // 
            // label38
            // 
            this.label38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label38.BackColor = System.Drawing.Color.LightGreen;
            this.label38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label38.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label38.Location = new System.Drawing.Point(784, 34);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(13, 13);
            this.label38.TabIndex = 4;
            // 
            // label39
            // 
            this.label39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label39.BackColor = System.Drawing.Color.PaleTurquoise;
            this.label39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label39.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label39.Location = new System.Drawing.Point(784, 48);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(13, 13);
            this.label39.TabIndex = 6;
            // 
            // label40
            // 
            this.label40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(799, 20);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(47, 13);
            this.label40.TabIndex = 3;
            this.label40.Text = "Urgency";
            // 
            // label41
            // 
            this.label41.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label41.BackColor = System.Drawing.Color.Gold;
            this.label41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label41.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label41.Location = new System.Drawing.Point(784, 20);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(13, 13);
            this.label41.TabIndex = 2;
            // 
            // label42
            // 
            this.label42.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(799, 34);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(37, 13);
            this.label42.TabIndex = 5;
            this.label42.Text = "Safety";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(11, 6);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(49, 13);
            this.label34.TabIndex = 8;
            this.label34.Text = "Channel:";
            // 
            // lblUniChannelTypeAndSat
            // 
            this.lblUniChannelTypeAndSat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUniChannelTypeAndSat.AutoEllipsis = true;
            this.lblUniChannelTypeAndSat.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblUniChannelTypeAndSat.ForeColor = System.Drawing.Color.Navy;
            this.lblUniChannelTypeAndSat.Location = new System.Drawing.Point(63, 7);
            this.lblUniChannelTypeAndSat.Name = "lblUniChannelTypeAndSat";
            this.lblUniChannelTypeAndSat.Size = new System.Drawing.Size(703, 13);
            this.lblUniChannelTypeAndSat.TabIndex = 12;
            // 
            // lblUniStatus
            // 
            this.lblUniStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUniStatus.AutoEllipsis = true;
            this.lblUniStatus.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblUniStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUniStatus.ForeColor = System.Drawing.Color.Navy;
            this.lblUniStatus.Location = new System.Drawing.Point(63, 44);
            this.lblUniStatus.Name = "lblUniStatus";
            this.lblUniStatus.Size = new System.Drawing.Size(703, 14);
            this.lblUniStatus.TabIndex = 36;
            // 
            // lblUniServices
            // 
            this.lblUniServices.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUniServices.AutoEllipsis = true;
            this.lblUniServices.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblUniServices.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUniServices.ForeColor = System.Drawing.Color.Navy;
            this.lblUniServices.Location = new System.Drawing.Point(63, 25);
            this.lblUniServices.Name = "lblUniServices";
            this.lblUniServices.Size = new System.Drawing.Size(703, 14);
            this.lblUniServices.TabIndex = 35;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(17, 45);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(40, 13);
            this.label26.TabIndex = 29;
            this.label26.Text = "Status:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(11, 25);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(46, 13);
            this.label32.TabIndex = 10;
            this.label32.Text = "Service:";
            // 
            // tabDebug
            // 
            this.tabDebug.Controls.Add(this.rtbDebug);
            this.tabDebug.Controls.Add(this.tsBlocks);
            this.tabDebug.Location = new System.Drawing.Point(4, 22);
            this.tabDebug.Name = "tabDebug";
            this.tabDebug.Padding = new System.Windows.Forms.Padding(1);
            this.tabDebug.Size = new System.Drawing.Size(920, 531);
            this.tabDebug.TabIndex = 4;
            this.tabDebug.Text = "Debug";
            this.tabDebug.UseVisualStyleBackColor = true;
            // 
            // rtbDebug
            // 
            this.rtbDebug.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbDebug.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbDebug.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbDebug.Location = new System.Drawing.Point(1, 26);
            this.rtbDebug.Name = "rtbDebug";
            this.rtbDebug.Size = new System.Drawing.Size(918, 504);
            this.rtbDebug.TabIndex = 67;
            this.rtbDebug.Text = "";
            this.rtbDebug.WordWrap = false;
            // 
            // tsBlocks
            // 
            this.tsBlocks.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsBlocks.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDebugClear,
            this.toolStripSeparator4,
            this.btnDebugCopy,
            this.toolStripSeparator17,
            this.btnDebugAutoScroll,
            this.toolStripSeparator2,
            this.btnJson});
            this.tsBlocks.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsBlocks.Location = new System.Drawing.Point(1, 1);
            this.tsBlocks.Name = "tsBlocks";
            this.tsBlocks.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tsBlocks.Size = new System.Drawing.Size(918, 25);
            this.tsBlocks.TabIndex = 74;
            this.tsBlocks.Text = "toolStrip1";
            // 
            // btnDebugClear
            // 
            this.btnDebugClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDebugClear.Image = ((System.Drawing.Image)(resources.GetObject("btnDebugClear.Image")));
            this.btnDebugClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDebugClear.Name = "btnDebugClear";
            this.btnDebugClear.Size = new System.Drawing.Size(30, 22);
            this.btnDebugClear.Text = "Clear";
            this.btnDebugClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // btnDebugCopy
            // 
            this.btnDebugCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDebugCopy.Image = ((System.Drawing.Image)(resources.GetObject("btnDebugCopy.Image")));
            this.btnDebugCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDebugCopy.Name = "btnDebugCopy";
            this.btnDebugCopy.Size = new System.Drawing.Size(29, 22);
            this.btnDebugCopy.Text = "Copy";
            this.btnDebugCopy.Click += new System.EventHandler(this.btnDebugCopy_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(6, 25);
            // 
            // btnDebugAutoScroll
            // 
            this.btnDebugAutoScroll.Checked = true;
            this.btnDebugAutoScroll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnDebugAutoScroll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDebugAutoScroll.Image = ((System.Drawing.Image)(resources.GetObject("btnDebugAutoScroll.Image")));
            this.btnDebugAutoScroll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDebugAutoScroll.Name = "btnDebugAutoScroll";
            this.btnDebugAutoScroll.Size = new System.Drawing.Size(51, 22);
            this.btnDebugAutoScroll.Text = "Auto Scroll";
            this.btnDebugAutoScroll.Click += new System.EventHandler(this.toolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnJson
            // 
            this.btnJson.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnJson.Image = ((System.Drawing.Image)(resources.GetObject("btnJson.Image")));
            this.btnJson.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnJson.Name = "btnJson";
            this.btnJson.Size = new System.Drawing.Size(26, 22);
            this.btnJson.Text = "Json";
            this.btnJson.Click += new System.EventHandler(this.toolStripButton_Click);
            // 
            // tabPackets
            // 
            this.tabPackets.BackColor = System.Drawing.SystemColors.Window;
            this.tabPackets.Controls.Add(this.rtbPackets);
            this.tabPackets.Controls.Add(this.toolStrip1);
            this.tabPackets.Location = new System.Drawing.Point(4, 22);
            this.tabPackets.Name = "tabPackets";
            this.tabPackets.Padding = new System.Windows.Forms.Padding(1);
            this.tabPackets.Size = new System.Drawing.Size(920, 531);
            this.tabPackets.TabIndex = 2;
            this.tabPackets.Tag = "";
            this.tabPackets.Text = "Packets";
            // 
            // rtbPackets
            // 
            this.rtbPackets.BackColor = System.Drawing.SystemColors.Window;
            this.rtbPackets.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbPackets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbPackets.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbPackets.Location = new System.Drawing.Point(1, 26);
            this.rtbPackets.MaxLength = 500;
            this.rtbPackets.Name = "rtbPackets";
            this.rtbPackets.Size = new System.Drawing.Size(918, 504);
            this.rtbPackets.TabIndex = 66;
            this.rtbPackets.Text = "";
            this.rtbPackets.WordWrap = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPacketsClear,
            this.toolStripSeparator14,
            this.toolStripButton4,
            this.toolStripSeparator18,
            this.btnPacketsAutoScroll});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(1, 1);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolStrip1.Size = new System.Drawing.Size(918, 25);
            this.toolStrip1.TabIndex = 77;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnPacketsClear
            // 
            this.btnPacketsClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnPacketsClear.Image = ((System.Drawing.Image)(resources.GetObject("btnPacketsClear.Image")));
            this.btnPacketsClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPacketsClear.Name = "btnPacketsClear";
            this.btnPacketsClear.Size = new System.Drawing.Size(30, 22);
            this.btnPacketsClear.Text = "Clear";
            this.btnPacketsClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(29, 22);
            this.toolStripButton4.Text = "Copy";
            this.toolStripButton4.Click += new System.EventHandler(this.btnPacketsCopy_Click);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPacketsAutoScroll
            // 
            this.btnPacketsAutoScroll.Checked = true;
            this.btnPacketsAutoScroll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnPacketsAutoScroll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnPacketsAutoScroll.Image = ((System.Drawing.Image)(resources.GetObject("btnPacketsAutoScroll.Image")));
            this.btnPacketsAutoScroll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPacketsAutoScroll.Name = "btnPacketsAutoScroll";
            this.btnPacketsAutoScroll.Size = new System.Drawing.Size(51, 22);
            this.btnPacketsAutoScroll.Text = "Auto Scroll";
            this.btnPacketsAutoScroll.Click += new System.EventHandler(this.toolStripButton_Click);
            // 
            // tC
            // 
            this.tC.Controls.Add(this.tabPackets);
            this.tC.Controls.Add(this.tabDebug);
            this.tC.Controls.Add(this.tabUni);
            this.tC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tC.HotTrack = true;
            this.tC.Location = new System.Drawing.Point(215, 77);
            this.tC.Multiline = true;
            this.tC.Name = "tC";
            this.tC.SelectedIndex = 0;
            this.tC.Size = new System.Drawing.Size(928, 557);
            this.tC.TabIndex = 67;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "BulletinBoard";
            this.dataGridViewTextBoxColumn16.HeaderText = "BulletinBoard";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "Area";
            this.dataGridViewTextBoxColumn17.HeaderText = "Area";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // nMaxStoredHours
            // 
            this.nMaxStoredHours.Increment = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.nMaxStoredHours.Location = new System.Drawing.Point(91, 66);
            this.nMaxStoredHours.Maximum = new decimal(new int[] {
            168,
            0,
            0,
            0});
            this.nMaxStoredHours.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.nMaxStoredHours.Name = "nMaxStoredHours";
            this.nMaxStoredHours.Size = new System.Drawing.Size(99, 20);
            this.nMaxStoredHours.TabIndex = 115;
            this.nMaxStoredHours.Value = new decimal(new int[] {
            36,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 114;
            this.label2.Text = "Db Hours:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1143, 656);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tC);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Quick UI for Scytale-C";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panelMenu.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxDisplayedDebugs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxDisplayedPackets)).EndInit();
            this.gbFrames.ResumeLayout(false);
            this.gbFrames.PerformLayout();
            this.gbRepeater.ResumeLayout(false);
            this.gbRepeater.PerformLayout();
            this.tabUni.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.pnlUniMap.ResumeLayout(false);
            this.pnlUniMap.PerformLayout();
            this.pnlUniPriority.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unisBindingSource)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.tabDebug.ResumeLayout(false);
            this.tabDebug.PerformLayout();
            this.tsBlocks.ResumeLayout(false);
            this.tsBlocks.PerformLayout();
            this.tabPackets.ResumeLayout(false);
            this.tabPackets.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nMaxStoredHours)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCredits;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnMenu;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.GroupBox gbFrames;
        private System.Windows.Forms.CheckBox cb92;
        private System.Windows.Forms.GroupBox gbRepeater;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtRawFrameUDPPort;
        private System.Windows.Forms.CheckBox cbBE;
        private System.Windows.Forms.CheckBox cbBD;
        private System.Windows.Forms.CheckBox cbB2;
        private System.Windows.Forms.CheckBox cbB1;
        private System.Windows.Forms.CheckBox cbAB;
        private System.Windows.Forms.CheckBox cbAA;
        private System.Windows.Forms.CheckBox cb7D;
        private System.Windows.Forms.CheckBox cb6C;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox cb08;
        private System.Windows.Forms.CheckBox cb2A;
        private System.Windows.Forms.CheckBox cb27;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox cb83;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Timer udpConsumerTimer;
        private System.Windows.Forms.Label lbl9;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl0;
        private System.Windows.Forms.Label lbl0a;
        private System.Windows.Forms.Label lbl9a;
        private System.Windows.Forms.Label lbl7a;
        private System.Windows.Forms.Label lbl4a;
        private System.Windows.Forms.Label lbl1a;
        private System.Windows.Forms.CheckBox cb81;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown nMaxDisplayedDebugs;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nMaxDisplayedPackets;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox cbA8;
        private System.Windows.Forms.CheckBox cbA3;
        private System.Windows.Forms.CheckBox cbUseLocalDbStore;
        private System.Windows.Forms.Timer dbFlushToDiskTimer;
        private System.Windows.Forms.BindingSource unisBindingSource;
        private System.Windows.Forms.TabPage tabUni;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel pnlUniMap;
        private GMap.NET.WindowsForms.GMapControl uniGMap;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Splitter splitterUniMap;
        internal System.Windows.Forms.RichTextBox uniMsgBox;
        private System.Windows.Forms.Panel pnlUniPriority;
        private System.Windows.Forms.Label lblUniServiceAddress;
        private System.Windows.Forms.Splitter uniGridSplitter;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.DataGridView dgvUni;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label lblUniStatus;
        private System.Windows.Forms.Label lblUniServices;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label lblUniChannelTypeAndSat;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TabPage tabDebug;
        internal System.Windows.Forms.RichTextBox rtbDebug;
        private System.Windows.Forms.TabPage tabPackets;
        internal System.Windows.Forms.RichTextBox rtbPackets;
        internal System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnPacketsClear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripButton btnPacketsAutoScroll;
        private System.Windows.Forms.TabControl tC;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.Label lblUniPriority;
        private System.Windows.Forms.Label lbl6a;
        private System.Windows.Forms.Label lbl3a;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl8a;
        private System.Windows.Forms.Label lbl5a;
        private System.Windows.Forms.Label lbl2a;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lbl2;
        internal System.Windows.Forms.ToolStrip tsBlocks;
        private System.Windows.Forms.ToolStripButton btnDebugClear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton btnDebugCopy;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripButton btnDebugAutoScroll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton btnJson;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniPriority;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniEncoding;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn mIdOrLCNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniRepetition;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniPartsReceived;
        private System.Windows.Forms.DataGridViewCheckBoxColumn UniIsPartial;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniStation;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniSatName;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniSat;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniStreamId;
        private System.Windows.Forms.DataGridViewTextBoxColumn uniIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn streamIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn UniIsEGC;
        private System.Windows.Forms.DataGridViewTextBoxColumn satDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn satNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lesIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lesNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeReceivedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn repetitionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn messageTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priorityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contentTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn partsReceivedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn messageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isPartialDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bulletinBoardDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressHexDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn areaDataGridViewTextBoxColumn;
        internal System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btnUniAutoScroll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnUniAutoLastSelect;
        private System.Windows.Forms.NumericUpDown nMaxStoredHours;
        private System.Windows.Forms.Label label2;
    }
}

