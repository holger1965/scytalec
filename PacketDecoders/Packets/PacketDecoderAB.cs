﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 *   
 * 
 * AB28BF0601F8F7F9FFFF02F8FF792E2E03F8FF792E6204F8FFF92E320CF8FFF92E3A15F8FFF92E2278
 * 
 * AB   - LES List
 * 28   - length
 * BF   - ?
 * 06   - number of stations in list
 * 01F8F7F9FFFF
 * 02F8FF792E2E
 * 03F8FF792E62
 * 04F8FFF92E32
 * 0CF8FFF92E3A
 * 15F8FFF92E22
 * 78   - end of info?
 * 
 * 2279 - AB: LES List
 *        001 [6OICO ASCT-DCFPLA2D--L] ----.--- Telenor Satellite Services Inc
 *        002 [6OICO ASCTHDCF-LA2D--L] 1539.555 Stratos Burum-2 Netherlands
 *        003 [6OICO ASCTHDCF-LA2D--L] 1539.685 Yamaguchi Japan
 *        004 [6OICO ASCTHDCFPLA2D--L] 1539.565 Southbury USA
 *        012 [6OICO ASCTHDCFPLA2D--L] 1539.585 Station 12 Netherlands
 *        021 [6OICO ASCTHDCFPLA2D--L] 1539.525 France Telecom MSC
 * 
 * 
 */

using ScytaleC.Interfaces;
using System;
using System.Collections.Generic;

namespace ScytaleC.PacketDecoders
{
    /// <summary>
    /// AB - LES List
    /// </summary>
    public class PacketDecoderAB : PacketDecoder
    {
        public int LESListLength { get; set; }
        public string StationStartHex { get; set; }
        public int StationCount { get; set; }
        public List<PacketDecoderUtils.Station> Stations { get; set; }

        public override void Decode(DescrambledFrameArgs args, ref int pos)
        {
            try
            {
                base.Decode(args, ref pos);
                if (!IsCRC)
                {
                    return;
                }

                DecodingStage_ = DecodingStage.Complete;

                LESListLength = args.DescrambledFrame[pos + 1];

                StationStartHex = Utils.BytesToHexString(args.DescrambledFrame, pos + 2, 1);

                //stations
                StationCount = args.DescrambledFrame[pos + 3];
                Stations = new List<PacketDecoderUtils.Station>();

                int j = pos + 4;
                for (int i = 0; i < StationCount; i++)
                {
                    PacketDecoderUtils.Station s = PacketDecoderUtils.ReturnStation(args.DescrambledFrame, j);
                    Stations.Add(s);

                    j += 6;
                }
            }
            catch (Exception ex)
            {
                CrashReport = string.Format("{0} [{1}]", ex.Message, ex.StackTrace);
            }
        }
    }
}