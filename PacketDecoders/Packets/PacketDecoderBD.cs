﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 *   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
 *   http://docslide.us/documents/tt3026-software-reference-manual.html
 * 
 *  
 * BDF9B2FC31AE154201002801BB37C74CC1CEC42C2045C1D35420434FC1D354AE20204C796E6E2057E96E64E661F26DAE20436861F2F420C2C12031B038AE208A46EFECECEFF7E96E67204C796E6E2070E5F2E96DE5F4E5F2206275EF796167E52070E5F26D616EE56EF4EC792064E973E3EF6EF4E96E75E564BA208ACE206275EF7920B5B3ADB0B9AE31CE20B0B0B0AD3237AEB5452C208AD3206275EF7920B5B3ADB037AE32CE20B0B0B0AD3238AEB9452C208AD357206275EF7920B5B3ADB037AE32CE20B0B0B0AD32B5AE32452C208A57206275EF7920B5B3ADB038AE32CE20B0B0B0AD32B5AEB34520616E6420CE57206275EF7920B5B30E
 * 
 * BD 
 * F9   - After this we have a quasi-normal B2 (see BlockDecoderB2)
 * B2FC31AE154201002801BB37C74CC1CEC42C2045C1D35420434FC1D354AE20204C796E6E2057E96E64E661F26DAE20436861F2F420C2C12031B038AE208A46EFECECEFF7E96E67204C796E6E2070E5F2E96DE5F4E5F2206275EF796167E52070E5F26D616EE56EF4EC792064E973E3EF6EF4E96E75E564BA208ACE206275EF7920B5B3ADB0B9AE31CE20B0B0B0AD3237AEB5452C208AD3206275EF7920B5B3ADB037AE32CE20B0B0B0AD3238AEB9452C208AD357206275EF7920B5B3ADB037AE32CE20B0B0B0AD32B5AE32452C208A57206275EF7920B5B3ADB038AE32CE20B0B0B0AD32B5AEB34520616E6420CE57206275EF7920B5B30E
 *
 * 
 * BD39AA8515600545C1C446524549C7C85420434CC149CDAE0D8A0D8A57494C4C20D345CEC4204FD55220D64FD92049CED35458CE2057C845CE51
 *
 * BD
 * 39   - After this we have a quasi-normal AA (see BlockDecoderAA)
 * AA8515600545C1C446524549C7C85420434CC149CDAE0D8A0D8A57494C4C20D345CEC4204FD55220D64FD92049CED35458CE2057C845CE51
 * 
 *  TODO completeness, areas
 * 
 */

using ScytaleC.Interfaces;
using System;

namespace ScytaleC.PacketDecoders

{
    /// <summary>
    /// BD - Multiframe Message Start
    /// </summary>
    public class PacketDecoderBD : PacketDecoder
    {
        public int MultiframePacketDescriptor { get; set; }
        public string MultiframePacketDescriptorHex { get; set; }
        public int MessageType { get; set; }
        public string ServiceCodeAndAddressName { get; set; }
        public int Continuation { get; set; }
        public int Priority { get; set; }
        public string PriorityText { get; set; }
        public bool IsDistress { get; set; }
        public int Repetition { get; set; }
        public int LogicalChannelNo { get; set; }
        public int MessageId { get; set; }
        public int PacketNo { get; set; }
        public string AddressHex { get; set; }
        public int Sat { get; set; }
        public string SatName { get; set; }
        public int LesId { get; set; }
        public string LesName { get; set; }

        //default constructor needed for dynamic casting
        public PacketDecoderBD() { }

        public PacketDecoderBD(MultiFrameArgs mfa) : base()
        {
            if (mfa != null)
            {
                MessageType = mfa.MessageType;
                Continuation = mfa.Continuation;
                Priority = mfa.Priority;
                IsDistress = mfa.IsDistress;
                Repetition = mfa.Repetition;
                LogicalChannelNo = mfa.LogicalChannelNo;
                MessageId = mfa.MessageId;
                PacketNo = mfa.PacketNo;
                Payload_.Presentation = mfa.Presentation;
            }
        }
        public void Decode(DescrambledFrameArgs args, ref int pos, out MultiFrameArgs mfa)
        {
            mfa = new MultiFrameArgs
            {
                Priority = Priority,
                Repetition = Repetition,
                LogicalChannelNo = LogicalChannelNo,
                MessageId = MessageId,
                PacketNo = PacketNo,
                Presentation = Payload_.Presentation
            };

            try
            {
                base.Decode(args, ref pos);
                if (!IsCRC)
                {
                    return;
                }

                DecodingStage_ = DecodingStage.Partial;

                MultiframePacketDescriptor = args.DescrambledFrame[pos + 2] & 0xFF;
                MultiframePacketDescriptorHex = MultiframePacketDescriptor.ToString("X");

                switch (MultiframePacketDescriptor)
                {
                    case 0xB1:
                        // Decode the rest of this block almost as a B1
                        MessageType = args.DescrambledFrame[pos + 4];
                        ServiceCodeAndAddressName = PacketDecoderUtils.ReturnServiceCodeAndAddressName(MessageType);

                        Continuation = (args.DescrambledFrame[pos + 5] & 0x80) >> 7;

                        Priority = (args.DescrambledFrame[pos + 5] & 0x60) >> 5;
                        PriorityText = PacketDecoderUtils.ReturnPriority(Priority);
                        IsDistress = Priority == PacketDecoderUtils.Distress;

                        Repetition = args.DescrambledFrame[pos + 5] & 0x1F;
                        MessageId = args.DescrambledFrame[pos + 6] << 8 | args.DescrambledFrame[pos + 7];
                        PacketNo = args.DescrambledFrame[pos + 8];

                        //bool isNewPayload = PacketNo == 1;
                        Payload_.Presentation = args.DescrambledFrame[pos + 9];

                        mfa.MultiframePacketDescriptor = 0xB1;
                        mfa.Priority = Priority;
                        mfa.Repetition = Repetition;
                        mfa.Continuation = Continuation;
                        mfa.MessageId = MessageId;
                        mfa.LogicalChannelNo = MessageId;
                        mfa.PacketNo = PacketNo;
                        mfa.Presentation = Payload_.Presentation;

                        //NAV/MET coordinator... area... TODO
                        int addressLengthB1 = PacketDecoderUtils.ReturnAddressLenght(MessageType);

                        if (pos + 10 + addressLengthB1 >= args.DescrambledFrame.Length)
                        {
                            return;
                        }

                        byte[] addressB1 = new byte[addressLengthB1];
                        Array.Copy(args.DescrambledFrame, pos + 10, addressB1, 0, addressLengthB1);

                        //The address actually starts at byte 9 and it is for now addressLength-1 long
                        //We think for now that byte 8 is a LesId
                        AddressHex = Utils.BytesToHexString(addressB1, 1, addressLengthB1 - 1);

                        //payload in bytes, presentation agnostic
                        int payloadLengthB1 = PacketLength - 2 - 10 - addressLengthB1;
                        if (payloadLengthB1 <= 0)
                        {
                            IsHeaderOnly = true;
                            break;
                        }
                        byte[] payloadB1 = new byte[payloadLengthB1];

                        int kB1 = pos + 10 + addressLengthB1;
                        int actualLengthB1 = 0;
                        for (int i = 0; kB1 < pos + 10 + addressLengthB1 + payloadLengthB1; i++)
                        {
                            payloadB1[i] = args.DescrambledFrame[kB1];
                            kB1++;
                            actualLengthB1++;
                        }
                        Payload_.Data8Bit = new byte[actualLengthB1];
                        Array.Copy(payloadB1, Payload_.Data8Bit, actualLengthB1);

                        break;

                    case 0xB2:
                        // Decode the rest of this block almost as a B2

                        MessageType = args.DescrambledFrame[pos + 4];
                        ServiceCodeAndAddressName = PacketDecoderUtils.ReturnServiceCodeAndAddressName(MessageType);

                        Continuation = (args.DescrambledFrame[pos + 5] & 0x80) >> 7;

                        Priority = (args.DescrambledFrame[pos + 5] & 0x60) >> 5;
                        PriorityText = PacketDecoderUtils.ReturnPriority(Priority);
                        IsDistress = Priority == PacketDecoderUtils.Distress;

                        Repetition = args.DescrambledFrame[pos + 5] & 0x1F;
                        MessageId = args.DescrambledFrame[pos + 6] << 8 | args.DescrambledFrame[pos + 7];
                        PacketNo = args.DescrambledFrame[pos + 8];

                        Payload_.Presentation = args.DescrambledFrame[pos + 9];

                        mfa.MultiframePacketDescriptor = 0xB2;
                        mfa.Priority = Priority;
                        mfa.Repetition = Repetition;
                        mfa.Continuation = Continuation;
                        mfa.MessageId = MessageId;
                        mfa.LogicalChannelNo = MessageId;
                        mfa.PacketNo = PacketNo;
                        mfa.Presentation = Payload_.Presentation;

                        //NAV/MET coordinator... TODO
                        //Address
                        int addressLength = PacketDecoderUtils.ReturnAddressLenght(MessageType);

                        if (pos + 10 + addressLength >= args.DescrambledFrame.Length)
                        {
                            return;
                        }

                        byte[] address = new byte[addressLength];
                        Array.Copy(args.DescrambledFrame, pos + 10, address, 0, addressLength);

                        //The address actually starts at byte 9 and it is for now addressLength-1 long
                        //We think for now that byte 8 is a LesId
                        AddressHex = Utils.BytesToHexString(address, 1, addressLength - 1);

                        int payloadLength = PacketLength - 2 - 10 - addressLength;
                        if (payloadLength <= 0)
                        {
                            IsHeaderOnly = true;
                            break;
                        }
                        byte[] payload = new byte[payloadLength];

                        int k = pos + 10 + addressLength;
                        int actualLengthB2 = 0;
                        for (int i = 0; k < pos + 10 + addressLength + payloadLength; i++)
                        {
                            payload[i] = args.DescrambledFrame[k];
                            k++;
                            actualLengthB2++;
                        }
                        Payload_.Data8Bit = new byte[actualLengthB2];
                        Array.Copy(payload, Payload_.Data8Bit, actualLengthB2);

                        break;

                    case 0xAA:
                        // Decode the rest of this block almost as an AA

                        base.Decode(args, ref pos);
                        DecodingStage_ = DecodingStage.Complete;
                        Sat = PacketDecoderUtils.ReturnSat(args.DescrambledFrame[pos + 4]);
                        SatName = PacketDecoderUtils.ReturnSatName(Sat);

                        LesId = PacketDecoderUtils.ReturnLesId(args.DescrambledFrame[pos + 4]);
                        LesName = PacketDecoderUtils.ReturnLesName(Sat, LesId);

                        LogicalChannelNo = args.DescrambledFrame[pos + 5];

                        PacketNo = args.DescrambledFrame[pos + 6];

                        byte[] payloadAA = new byte[PacketLength - 2];

                        int m = pos + 7;
                        int actualLengthAA = 0;
                        for (int i = 0; m < pos + PacketLength - 2; i++)
                        {
                            payloadAA[i] = args.DescrambledFrame[m];
                            m++;
                            actualLengthAA++;
                        }
                        Payload_.Data8Bit = new byte[actualLengthAA];
                        Array.Copy(payloadAA, Payload_.Data8Bit, actualLengthAA);

                        mfa.MultiframePacketDescriptor = 0xAA;
                        mfa.LogicalChannelNo = LogicalChannelNo;
                        mfa.PacketNo = PacketNo;
                        mfa.Presentation = Payload_.Presentation;

                        break;
                }
            }
            catch (Exception ex)
            {
                CrashReport = string.Format("{0} [{1}]", ex.Message, ex.StackTrace);
            }
        }
    }
}