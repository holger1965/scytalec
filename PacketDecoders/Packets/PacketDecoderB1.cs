﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 *   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
 *   http://docslide.us/documents/tt3026-software-reference-manual.html
 * 
 *  TODO completeness, areas
 *  
 */

using ScytaleC.Interfaces;
using System;

namespace ScytaleC.PacketDecoders
{
    /// <summary>
    /// B1 - EGC
    /// </summary>
    public class PacketDecoderB1 : PacketDecoder
    {
        public int MessageType { get; set; }
        public string ServiceCodeAndAddressName { get; set; }
        public int Continuation { get; set; }
        public int Priority { get; set; }
        public string PriorityText { get; set; }
        public bool IsDistress { get; set; }
        public int Repetition { get; set; }
        public int MessageId { get; set; }
        public int PacketNo { get; set; }
        public string AddressHex { get; set; }

        public void Decode(DescrambledFrameArgs args, ref int pos, out MultiFrameArgs mfa)
        {
            mfa = new MultiFrameArgs();
            try
            {
                base.Decode(args, ref pos);
                if (!IsCRC)
                {
                    return;
                }

                DecodingStage_ = DecodingStage.Partial;

                //byte 2
                MessageType = args.DescrambledFrame[pos + 2];
                ServiceCodeAndAddressName = PacketDecoderUtils.ReturnServiceCodeAndAddressName(MessageType);

                ///byte 3, MSB 1 bit
                Continuation = (args.DescrambledFrame[pos + 3] & 0x80) >> 7;

                //byte 3, next 2 bits
                Priority = (args.DescrambledFrame[pos + 3] & 0x60) >> 5;
                PriorityText = PacketDecoderUtils.ReturnPriority(Priority);

                IsDistress = Priority == PacketDecoderUtils.Distress;

                //byte 3, LSB 5 bits
                Repetition = args.DescrambledFrame[pos + 3] & 0x1F;

                //byte 4 and byte 5
                MessageId = args.DescrambledFrame[pos + 4] << 8 | args.DescrambledFrame[pos + 5];

                //byte 6
                PacketNo = args.DescrambledFrame[pos + 6];
                bool isNewPayload = PacketNo == 1;

                //byte 7
                Payload_.Presentation = args.DescrambledFrame[pos + 7];

                mfa.MessageType = MessageType;
                mfa.Continuation = Continuation;
                mfa.Priority = Priority;
                mfa.IsDistress = IsDistress;
                mfa.Repetition = Repetition;
                mfa.MessageId = MessageId;
                mfa.LogicalChannelNo = MessageId;
                mfa.PacketNo = PacketNo;
                mfa.Presentation = Payload_.Presentation;

                //NAV/MET coordinator... area... TODO
                int addressLength = PacketDecoderUtils.ReturnAddressLenght(MessageType);

                if (pos + 8 + addressLength >= args.DescrambledFrame.Length)
                {
                    return;
                }

                //byte 8
                byte[] address = new byte[addressLength];
                Array.Copy(args.DescrambledFrame, pos + 8, address, 0, addressLength);

                //The address actually starts at byte 9 and it is for now addressLength-1 long
                //We think for now that byte 8 is a LesId
                AddressHex = Utils.BytesToHexString(address, 1, addressLength - 1);

                //payload in bytes, presentation agnostic
                int payloadLength = PacketLength - 2 - 8 - addressLength;
                byte[] payload = new byte[payloadLength];

                int k = pos + 8 + addressLength;
                int actualLength = 0;
                for (int i = 0; k < pos + 8 + addressLength + payloadLength; i++)
                {
                    payload[i] = args.DescrambledFrame[k];
                    k++;
                    actualLength++;
                }
                Payload_.Data8Bit = new byte[actualLength];
                Array.Copy(payload, Payload_.Data8Bit, actualLength);
            }
            catch (Exception ex)
            {
                CrashReport = string.Format("{0} [{1}]", ex.Message, ex.StackTrace);
            }
        }
    }
}