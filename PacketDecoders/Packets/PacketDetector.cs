﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 *   
 * 
 * The NCS common channel and LES TDM channels share a common overall structure. The
 * TDM channels are based on fixed-length transmitted at 1200 symbols/s giving a
 * frame time of 8.64S. 10,000 frames are transmitted every day (8.64s x 10000 = 24 hours).
 * 
 * Each frame carries a 639 byte information field, followed by a flush byte (all 0s).
 * The first packet in the information field is always the Bulletin Board (BB) packet.
 * The bulletin board contains information on the network configuration and the correct
 * frame number.
 * 
 * Note:
 * It seems that for the message packets we need to keep track or the logical channel number
 * and the type of the message.
 * It is plausible at this point that the logic below is erroneous and instead of keeping
 * track of the previously decoded packet, we should keep tract of the previously decoded message.
 * This I think makes more and more sense.
 * An exhaustive and statistical analysis on data will reveal this aspect. It seems to be more
 * prevalent on the AA packets.
 * This is Fun!
 *
 */

using ScytaleC.Interfaces;
using System;

namespace ScytaleC.PacketDecoders
{
    public class PacketDetector : IPacketDetector
    {
        private PacketDecoder pd;
        private MessageDecoder md;
        private MultiStreamFrameElements msfe;

        public event EventHandler<PacketArgs> OnPacket;

        public PacketDetector()
        {
            msfe = new MultiStreamFrameElements();
        }

        public void Process(DescrambledFrameArgs args, long streamId = 0)
        {
            int pos = 0;
            do
            {
                Detect(args, streamId, ref pos);
            } while (pos > 0);
        }

        private void Detect(DescrambledFrameArgs args, long streamId, ref int pos)
        {
            if (pos >= 640)
            {
                pos = 0;
                return;
            }

            switch (args.DescrambledFrame[pos])
            {
                //No more data
                case 0x00:
                    pos = 0;
                    return;

                //27 - Logical Channel Clear
                case 0x27:
                    pd = new PacketDecoder27();
                    pd.Decode(args, ref pos);
                    break;

                //2A - Inbound Message Ack.
                case 0x2A:
                    pd = new PacketDecoder2A();
                    pd.Decode(args, ref pos);
                    break;

                //08 - Acknowledgement Request
                case 0x08:
                    pd = new PacketDecoder08();
                    pd.Decode(args, ref pos);
                    break;

                //6C - Signalling Channel
                case 0x6C:
                    pd = new PacketDecoder6C();
                    pd.Decode(args, ref pos);
                    break;

                //7D - Bulletin Board
                case 0x7D:
                    pd = new PacketDecoder7D();
                    pd.Decode(args, ref pos);
                    break;

                //81 - Announcement
                case 0x81:
                    pd = new PacketDecoder81();
                    pd.Decode(args, ref pos);
                    break;

                //83 - Logical Channel Assignment
                case 0x83:
                    pd = new PacketDecoder83();
                    pd.Decode(args, ref pos);
                    break;

                //91 - Distress Alert Ack.
                //case 0x91:
                //    pd = new PacketDecoder91();
                //    pd.Decode(args, ref pos);
                //    break;

                //92 - Login Ack.
                case 0x92:
                    pd = new PacketDecoder92();
                    pd.Decode(args, ref pos);
                    break;

                //9A - Enhanced Data Report Ack.
                //case 0x9A:
                //    pd = new PacketDecoder9A();
                //    pd.Decode(args, ref pos);
                //    break;

                //A0 - Distress Test Request
                //case 0xA0:
                //    pd = new PacketDecoderA0();
                //    pd.Decode(args, ref pos);
                //    break;

                //A3 - Individual Poll
                case 0xA3:
                    pd = new PacketDecoderA3();
                    pd.Decode(args, ref pos);
                    break;

                //A8 - Confirmation
                case 0xA8:
                    pd = new PacketDecoderA8();
                    pd.Decode(args, ref pos);
                    break;

                //AA - Message
                case 0xAA:
                    pd = new PacketDecoderAA();
                    ((PacketDecoderAA)pd).Decode(args, ref pos, out MultiFrameArgs mfaAA);
                    msfe[streamId].Mfa = mfaAA;
                    md = new MessageDecoder((PacketDecoderAA)pd);
                    md.DecodeMessage();
                    break;

                //AB - Les List
                case 0xAB:
                    pd = new PacketDecoderAB();
                    pd.Decode(args, ref pos);
                    break;

                //AC - Request Status
                //case 0xAC:
                //    pd = new PacketDecoderAC();
                //    pd.Decode(args, ref pos);
                //    break;

                //AD - Test Result
                //case 0xAD:
                //    pd = new PacketDecoderAD();
                //    pd.Decode(args, ref pos);
                //    break;

                //B1 - EGC
                case 0xB1:
                    pd = new PacketDecoderB1();
                    ((PacketDecoderB1)pd).Decode(args, ref pos, out MultiFrameArgs mfaB1);
                    msfe[streamId].Mfa = mfaB1;
                    md = new MessageDecoder((PacketDecoderB1)pd);
                    md.DecodeMessage();
                    break;

                //B2 - EGC
                case 0xB2:
                    pd = new PacketDecoderB2();
                    ((PacketDecoderB2)pd).Decode(args, ref pos, out MultiFrameArgs mfaB2);
                    msfe[streamId].Mfa = mfaB2;
                    md = new MessageDecoder((PacketDecoderB2)pd);
                    md.DecodeMessage();
                    break;

                //BD - Multiframe Message
                case 0xBD:
                    pd = new PacketDecoderBD(msfe[streamId].Mfa);
                    ((PacketDecoderBD)pd).Decode(args, ref pos, out MultiFrameArgs mfaBD);
                    msfe[streamId].Mfa = mfaBD;
                    md = new MessageDecoder((PacketDecoderBD)pd);
                    md.DecodeMessage();
                    break;

                //BE - Multiframe Message Continue
                case 0xBE:
                    // This frame gets the same parameters as the Multiframe Message
                    pd = new PacketDecoderBE(msfe[streamId].Mfa);
                    pd.Decode(args, ref pos);
                    md = new MessageDecoder((PacketDecoderBE)pd);
                    md.DecodeMessage();
                    break;

                default:
                    pd = new PacketDecoder();
                    pd.Decode(args, ref pos);
                    break;
            }

            PacketArgs pa = new PacketArgs
            {
                OriginalFrameArgs = args
            };

            // set the StreamId
            pd.StreamId = streamId;

            // if this is the bulletin board, add the bb packet number to the originating frame
            // if the CRC for the bulletin board is not okay, mark the frame as not containing a valid bb
            if (args.DescrambledFrame[pos] == 0x7D)
            {
                if (pd.IsCRC)
                {
                    pa.OriginalFrameArgs.IsBadBulletinBoard = false;
                    //the typecasting is not needed since we introduced the frameNumber in the base packetDetector class
                    //however we keep it as is just to emphasize that this is wher the frameNumber comes from.
                    //all the other packets in this frame receive it as well.
                    msfe[streamId].FrameNumber = ((PacketDecoder7D)pd).FrameNumber;
                    pa.OriginalFrameArgs.FrameNumber = ((PacketDecoder7D)pd).FrameNumber;
                    //compute the full UTC time of the frame
                    pd.Time = PacketDecoderUtils.ReturnUtcTime(pd.StreamId, ((PacketDecoder7D)pd).FrameTimeUTC_);
                    pa.OriginalFrameArgs.Time = pd.Time;
                }
                else
                {
                    ((PacketDecoder7D)pd).FrameNumber = -999;
                    msfe[streamId].FrameNumber = -999;
                    pa.OriginalFrameArgs.FrameNumber = -999;
                    pa.OriginalFrameArgs.Time = DateTime.MinValue;
                }
            }
            else
            {
                pd.FrameNumber = pa.OriginalFrameArgs.FrameNumber;
                pd.Time = pa.OriginalFrameArgs.Time;
            }

            pa.PacketDescriptor = pd.PacketDescriptor;
            pa.HexSequence = pd.HexSequence;
            pa.StreamId = streamId;

            pa.SerializedData = pd.Serialize();
            OnPacket?.Invoke(this, pa);

            pos += pd.PacketLength;
        }
    }
}