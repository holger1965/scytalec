﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using ScytaleC.Interfaces;
using System.Collections.Generic;

namespace ScytaleC.PacketDecoders
{
    public class MultiStreamFrameElements
    {
        private Dictionary<long, MultiStreamFrameElement> elements;

        public MultiStreamFrameElements()
        {
            elements = new Dictionary<long, MultiStreamFrameElement>();
        }

        public MultiStreamFrameElement this[long key]
        {
            get
            {
                MultiStreamFrameElement msfe;

                //return the element if exists
                //create the element if it does not exist and return the newly created element
                if (!elements.ContainsKey(key))
                {
                    msfe = new MultiStreamFrameElement();
                    elements.Add(key, msfe);
                }

                elements.TryGetValue(key, out msfe);

                return msfe;
            }
        }
    }

    public class MultiStreamFrameElement
    {
        public int FrameNumber { get; set; }
        public MultiFrameArgs Mfa { get; set; }
    }
}