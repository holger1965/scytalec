﻿/*
 * microp11, holger 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 *   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
 *   http://docslide.us/documents/tt3026-software-reference-manual.html
 *   
 * 
 * 08 -Acknowledgement Request
 * 
 * 08 55 DE 2E24 6886 7F06
 *               |
 *               unknown
 * 
 * 08 - AcknowledgementRequest LES_Id: 21  LCnr: 222  Uplink: 1640,03 MHz
 * 
 * 
 * */

using ScytaleC.Interfaces;
using System;

namespace ScytaleC.PacketDecoders
{
    public class PacketDecoder08 : PacketDecoder
    {
        public int Sat { get; set; }
        public string SatName { get; set; }
        public int LesId { get; set; }
        public string LesName { get; set; }
        public int LogicalChannelNo { get; set; }
        public double UplinkChannelMHz { get; set; }

        public override void Decode(DescrambledFrameArgs args, ref int pos)
        {
            try
            {
                base.Decode(args, ref pos);
                if (!IsCRC)
                {
                    return;
                }

                DecodingStage_ = DecodingStage.Complete;

                Sat = PacketDecoderUtils.ReturnSat(args.DescrambledFrame[pos + 1]);
                SatName = PacketDecoderUtils.ReturnSatName(Sat);

                LesId = PacketDecoderUtils.ReturnLesId(args.DescrambledFrame[pos + 1]);
                LesName = PacketDecoderUtils.ReturnLesName(Sat, LesId);

                LogicalChannelNo = args.DescrambledFrame[pos + 2];

                UplinkChannelMHz = PacketDecoderUtils.ReturnUplinkChannelMHz(args.DescrambledFrame, pos + 3);
            }
            catch (Exception ex)
            {
                CrashReport = string.Format("{0} [{1}]", ex.Message, ex.StackTrace);
            }
        }

    }
}

