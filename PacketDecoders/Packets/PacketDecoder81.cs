﻿/*
 * microp11, holger 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 *   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
 *   http://docslide.us/documents/tt3026-software-reference-manual.html
 *   
 * 
 *  81 - Announcement
 *  81 11 EB1DB7 44 2E3E 00 87 017F0800 07 032E 3187
 *  81 11 DDCF43 55 2E5E 00 52 05DBB900 06 0110 1785
 *  81 11 9FC48B 55 2E5E 00 64 0AD2D600 06 011B 5D0A
 *  81 11 AF6654 55 2E5E 00 65 0E378700 06 0119 D300
 *  81 11 3D9180 55 2E5E 00 66 06AA5800 00 0111 5A65
 *     |  |      |  |    |  |  |        |  |    |
 *     |  |      |  |    |  |  |        |  |    CRC
 *     |  |      |  |    |  |  |        |  unknown
 *     |  |      |  |    |  |  |        Presentation
 *     |  |      |  |    |  |  unknown
 *     |  |      |  |    |  LogicalChannel
 *     |  |      |  |    unknown
 *     |  |      |  LESTDMDownlinkFrequency
 *     |  |      LES_ID
 *     |  MES_ID
 *     Length
 * 
 * */

using ScytaleC.Interfaces;
using System;

namespace ScytaleC.PacketDecoders

{
    public class PacketDecoder81 : PacketDecoder
    {
        public int MesId { get; set; }
        public string MesIdHex { get; set; }
        public int Sat { get; set; }
        public string SatName { get; set; }
        public int LesId { get; set; }
        public string LesName { get; set; }
        public double DownlinkChannelMHz { get; set; }
        public string Unknown1Hex { get; set; }
        public int LogicalChannelNo { get; set; }
        public string Unknown2Hex { get; set; }
        public int Presentation { get; set; }
        public string Unknown3Hex { get; set; }

        public override void Decode(DescrambledFrameArgs args, ref int pos)
        {
            try
            {
                base.Decode(args, ref pos);
                if (!IsCRC)
                {
                    return;
                }

                DecodingStage_ = DecodingStage.Partial;

                MesId = PacketDecoderUtils.ReturnMesId(args.DescrambledFrame, pos + 2);
                MesIdHex = Utils.BytesToHexString(args.DescrambledFrame, pos + 2, 3);

                Sat = PacketDecoderUtils.ReturnSat(args.DescrambledFrame[pos + 5]);
                SatName = PacketDecoderUtils.ReturnSatName(Sat);

                LesId = PacketDecoderUtils.ReturnLesId(args.DescrambledFrame[pos + 5]);
                LesName = PacketDecoderUtils.ReturnLesName(Sat, LesId);

                DownlinkChannelMHz = PacketDecoderUtils.ReturnDownlinkChannelMHz(args.DescrambledFrame, pos + 6);

                Unknown1Hex = Utils.BytesToHexString(args.DescrambledFrame, pos + 8, 1);

                LogicalChannelNo = args.DescrambledFrame[pos + 9];

                Unknown2Hex = Utils.BytesToHexString(args.DescrambledFrame, pos + 10, 4);

                Presentation = args.DescrambledFrame[pos + 14];

                Unknown3Hex = Utils.BytesToHexString(args.DescrambledFrame, pos + 15, 2);
            }
            catch (Exception ex)
            {
                CrashReport = string.Format("{0} [{1}]", ex.Message, ex.StackTrace);
            }
        }

    }
}
