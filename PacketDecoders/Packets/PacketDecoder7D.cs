﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 *   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
 *   http://docslide.us/documents/tt3026-software-reference-manual.html
 *   
 *   
 */

using ScytaleC.Interfaces;
using System;

namespace ScytaleC.PacketDecoders

{
    /// <summary>
    /// 7D Bulletin Board
    /// </summary>
    public class PacketDecoder7D : PacketDecoder
    {
        public int NetworkVersion { get; set; }
        public FrameTimeUTC FrameTimeUTC_ { get; set; }
        public int SignallingChannel { get; set; }
        public int Count { get; set; }
        public int ChannelType { get; set; }
        public string ChannelTypeName { get; set; }
        public int Local { get; set; }
        public int Sat { get; set; }
        public string SatName { get; set; }
        public int LesId { get; set; }
        public string LesName { get; set; }
        public PacketDecoderUtils.Status Status_ { get; set; }
        public PacketDecoderUtils.Services Services_ { get; set; }
        public int RandomInterval { get; set; }
        
        public override void Decode(DescrambledFrameArgs args, ref int pos)
        {
            try
            {
                base.Decode(args, ref pos);

                if (!IsCRC)
                {
                    return;
                }

                DecodingStage_ = DecodingStage.Complete;

                NetworkVersion = args.DescrambledFrame[pos + 1];

                FrameNumber = args.DescrambledFrame[pos + 2] << 8 | args.DescrambledFrame[pos + 3];
                FrameTimeUTC_ = PacketDecoderUtils.ReturnTime(FrameNumber);

                SignallingChannel = args.DescrambledFrame[pos + 4] >> 2;

                Count = (args.DescrambledFrame[pos + 5] >> 4 & 0x0F) * 0x02;

                ChannelType = args.DescrambledFrame[pos + 6] >> 0x05;
                ChannelTypeName = PacketDecoderUtils.ReturnChannelTypeName(ChannelType);
                Local = args.DescrambledFrame[pos + 6] >> 2 & 0x07;

                Sat = PacketDecoderUtils.ReturnSat(args.DescrambledFrame[pos + 7]);
                SatName = PacketDecoderUtils.ReturnSatName(Sat);
                LesId = PacketDecoderUtils.ReturnLesId(args.DescrambledFrame[pos + 7]);
                LesName = PacketDecoderUtils.ReturnLesName(Sat, LesId);

                Status_ = PacketDecoderUtils.ReturnStatus(args.DescrambledFrame[pos + 8]);

                int ss = args.DescrambledFrame[pos + 9] << 8 | args.DescrambledFrame[pos + 10];
                Services_ = PacketDecoderUtils.ReturnServices(ss);

                RandomInterval = args.DescrambledFrame[pos + 11];
            }
            catch (Exception ex)
            {
                CrashReport = string.Format("{0} [{1}]", ex.Message, ex.StackTrace);
            }
        }
    }
}