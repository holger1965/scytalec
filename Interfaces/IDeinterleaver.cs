﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See implementation for documentation and references.
 * 
 */

using System;

namespace ScytaleC.Interfaces
{
    /// <summary>
    /// Frame deinterleaver
    /// Receives a frame as it comes out of the Depermuter
    /// 
    /// At the end of processing, the output will contain 10240 data symbols representing the received
    /// output of the transmitter's convolutional encoder
    /// </summary>
    public interface IDeinterleaver
    {
        /// <summary>
        /// See implementation for details.
        /// </summary>
        /// <param name="args"></param>
        void Process(DepermutedFrameArgs args);

        event EventHandler<DeinterleavedFrameArgs> OnDeinterleavedFrame;
    }
}
