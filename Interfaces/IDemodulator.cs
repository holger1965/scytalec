﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See implementation for documentation and references.
 * 
 */

using System;

namespace ScytaleC.Interfaces
{
    /// <summary>
    ///  BPSK demodulator
    /// </summary>
    public interface IDemodulator
    {
        /// <summary>
        /// This is the text that gets displayed in the UI Demodulators combo box.
        /// </summary>
        string Name { get; }
        bool IsCMASupported();
        bool IsAGCSupported();
        bool CmaEnabled { get; set; }
        bool AgcEnabled { get; set; }
        int LoFreq { get; set; }
        int HiFreq { get; set; }

        /// <summary>
        /// Demodulator main function.
        /// Takes an amount of audio samples and extracts the demodulated symbols.
        /// As it processes the samples, it calls back with signal imformation and demodulated symbols.
        /// </summary>
        /// <param name="args.Samples">Audio samples PCM 16-bit, big endian, 1 channel, 48000Hz.</param>
        /// <param name="args.Length">The length of the audio sample array.</param>
        void Process(AudioSampleArgs args);

        /// <summary>
        /// Sometimes it is necesary to train the demodulator and bring it back into its demodulating range.
        /// Sets the CF.
        /// </summary>
        /// <param name="centerFrequency">Center Frequency</param>
        void SetCenterFrequency(double centerFrequency);

        /// <summary>
        /// Even handler for signal information.
        /// </summary>
        event EventHandler<SignalAttributesArgs> OnSignalAttributes;

        /// <summary>
        /// Event handler for demodulated symbols.
        /// Each symbol is one byte.
        /// This allows for a soft Viterbi decoder implementation down the decoding chain.
        /// </summary>
        event EventHandler<DemodulatedSymbolsArgs> OnDemodulatedSymbols;
    }
}
